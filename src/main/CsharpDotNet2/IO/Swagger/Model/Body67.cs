using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body67 {
    /// <summary>
    /// The identifier of the Login resource that owns this Permission.
    /// </summary>
    /// <value>The identifier of the Login resource that owns this Permission.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// If you are delegating Permissions from a Login that you own, then this field stores the identifier of the Login resource whose access you want to delegate.
    /// </summary>
    /// <value>If you are delegating Permissions from a Login that you own, then this field stores the identifier of the Login resource whose access you want to delegate.</value>
    [DataMember(Name="fromlogin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fromlogin")]
    public string Fromlogin { get; set; }

    /// <summary>
    /// If you are delegating Permissions to a Login, then this field stores the identifier of the Login resource that should be granted the Permission.
    /// </summary>
    /// <value>If you are delegating Permissions to a Login, then this field stores the identifier of the Login resource that should be granted the Permission.</value>
    [DataMember(Name="tologin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tologin")]
    public string Tologin { get; set; }

    /// <summary>
    /// If you are delegating Permissions from an Org that you own, then this field stores the identifier of the Org resource whose access you want to delegate.
    /// </summary>
    /// <value>If you are delegating Permissions from an Org that you own, then this field stores the identifier of the Org resource whose access you want to delegate.</value>
    [DataMember(Name="fromorg", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fromorg")]
    public string Fromorg { get; set; }

    /// <summary>
    /// If you are delegating Permissions to an Org, then this field stores the identifier of the Org resource that should be granted the Permission.
    /// </summary>
    /// <value>If you are delegating Permissions to an Org, then this field stores the identifier of the Org resource that should be granted the Permission.</value>
    [DataMember(Name="toorg", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "toorg")]
    public string Toorg { get; set; }

    /// <summary>
    /// Gets or Sets Resource
    /// </summary>
    [DataMember(Name="resource", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "resource")]
    public int? Resource { get; set; }

    /// <summary>
    /// The level of access to delegate to the target Login or Org to view resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.
    /// </summary>
    /// <value>The level of access to delegate to the target Login or Org to view resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.</value>
    [DataMember(Name="view", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "view")]
    public int? View { get; set; }

    /// <summary>
    /// The level of access to delegate to the target Login or Org to add resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.
    /// </summary>
    /// <value>The level of access to delegate to the target Login or Org to add resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.</value>
    [DataMember(Name="add", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "add")]
    public int? Add { get; set; }

    /// <summary>
    /// The level of access to delegate to the target Login or Org to edit resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.
    /// </summary>
    /// <value>The level of access to delegate to the target Login or Org to edit resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.</value>
    [DataMember(Name="edit", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "edit")]
    public int? Edit { get; set; }

    /// <summary>
    /// The level of access to delegate to the target Login or Org to delete resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.
    /// </summary>
    /// <value>The level of access to delegate to the target Login or Org to delete resources of this type.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.</value>
    [DataMember(Name="destroy", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "destroy")]
    public int? Destroy { get; set; }

    /// <summary>
    /// The level of access to delegate to the target Login or Org to reference resources of this type in other resources.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.
    /// </summary>
    /// <value>The level of access to delegate to the target Login or Org to reference resources of this type in other resources.  Valid values are:  '0': No delegation. Do not modify the privileges of the target Entity or Org.  '1': Allow. Grant this level of access.   '2': Deny. Prevent this level of access.</value>
    [DataMember(Name="reference", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reference")]
    public int? Reference { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body67 {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Fromlogin: ").Append(Fromlogin).Append("\n");
      sb.Append("  Tologin: ").Append(Tologin).Append("\n");
      sb.Append("  Fromorg: ").Append(Fromorg).Append("\n");
      sb.Append("  Toorg: ").Append(Toorg).Append("\n");
      sb.Append("  Resource: ").Append(Resource).Append("\n");
      sb.Append("  View: ").Append(View).Append("\n");
      sb.Append("  Add: ").Append(Add).Append("\n");
      sb.Append("  Edit: ").Append(Edit).Append("\n");
      sb.Append("  Destroy: ").Append(Destroy).Append("\n");
      sb.Append("  Reference: ").Append(Reference).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
