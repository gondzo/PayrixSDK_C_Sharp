using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class AlertActionsQueryResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The medium to use to deliver this Alert.  Valid values are:  'email': Deliver the Alert to an email address.  'web': Deliver the Alert through a web site notification.  'app': Deliver the Alert through a mobile application notification.  'sms': Deliver the Alert through an SMS message to a mobile device.
    /// </summary>
    /// <value>The medium to use to deliver this Alert.  Valid values are:  'email': Deliver the Alert to an email address.  'web': Deliver the Alert through a web site notification.  'app': Deliver the Alert through a mobile application notification.  'sms': Deliver the Alert through an SMS message to a mobile device.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public string Type { get; set; }

    /// <summary>
    /// When the 'type' field of this resource is set to 'web', this field determines the format that the Alert data should be sent in.   Valid values are:  'JSON': JSON document serialization.  'XML': XML document serialization.  'SOAP': SOAP XML document serialization.  'FORM':HTML form data serialization.
    /// </summary>
    /// <value>When the 'type' field of this resource is set to 'web', this field determines the format that the Alert data should be sent in.   Valid values are:  'JSON': JSON document serialization.  'XML': XML document serialization.  'SOAP': SOAP XML document serialization.  'FORM':HTML form data serialization.</value>
    [DataMember(Name="options", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "options")]
    public string Options { get; set; }

    /// <summary>
    /// Gets or Sets Value
    /// </summary>
    [DataMember(Name="value", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "value")]
    public string Value { get; set; }

    /// <summary>
    /// The identifier of the Alert resource that defines this alertAction.
    /// </summary>
    /// <value>The identifier of the Alert resource that defines this alertAction.</value>
    [DataMember(Name="alert", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "alert")]
    public string Alert { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AlertActionsQueryResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Options: ").Append(Options).Append("\n");
      sb.Append("  Value: ").Append(Value).Append("\n");
      sb.Append("  Alert: ").Append(Alert).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
