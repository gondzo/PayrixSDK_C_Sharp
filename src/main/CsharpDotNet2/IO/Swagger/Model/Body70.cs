using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body70 {
    /// <summary>
    /// The identifier of the Entries resource that is being refunded.
    /// </summary>
    /// <value>The identifier of the Entries resource that is being refunded.</value>
    [DataMember(Name="entry", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entry")]
    public string Entry { get; set; }

    /// <summary>
    /// A description of this Refund.   This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>A description of this Refund.   This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The amount of this Refund.  This field is specified as an integer in cents.  This field is optional. If it is not set, then the API uses the amount that is specified in the related Entry resource.
    /// </summary>
    /// <value>The amount of this Refund.  This field is specified as an integer in cents.  This field is optional. If it is not set, then the API uses the amount that is specified in the related Entry resource.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public int? Amount { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body70 {\n");
      sb.Append("  Entry: ").Append(Entry).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
