using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// An object representing details of the Account, including the type of Account (method), Account number and routing code.
  /// </summary>
  [DataContract]
  public class AccountsaccountRequestFields {
    /// <summary>
    /// The type of the Account.  This field is specified as an integer.  Valid values are:  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account
    /// </summary>
    /// <value>The type of the Account.  This field is specified as an integer.  Valid values are:  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account</value>
    [DataMember(Name="method", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "method")]
    public int? Method { get; set; }

    /// <summary>
    /// The number of the bank account.
    /// </summary>
    /// <value>The number of the bank account.</value>
    [DataMember(Name="number", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "number")]
    public string Number { get; set; }

    /// <summary>
    /// The routing number of the bank account.
    /// </summary>
    /// <value>The routing number of the bank account.</value>
    [DataMember(Name="routing", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "routing")]
    public string Routing { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccountsaccountRequestFields {\n");
      sb.Append("  Method: ").Append(Method).Append("\n");
      sb.Append("  Number: ").Append(Number).Append("\n");
      sb.Append("  Routing: ").Append(Routing).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
