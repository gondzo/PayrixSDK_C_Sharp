using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class OrgUsersUpdateFields {
    /// <summary>
    /// The identifier of the Login resource that should be marked as part of the Org identified in the 'org' field.
    /// </summary>
    /// <value>The identifier of the Login resource that should be marked as part of the Org identified in the 'org' field.</value>
    [DataMember(Name="forlogin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "forlogin")]
    public string Forlogin { get; set; }

    /// <summary>
    /// The identifier of the Org resource that the Login identified in the 'forlogin' field should be marked as part of.
    /// </summary>
    /// <value>The identifier of the Org resource that the Login identified in the 'forlogin' field should be marked as part of.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class OrgUsersUpdateFields {\n");
      sb.Append("  Forlogin: ").Append(Forlogin).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
