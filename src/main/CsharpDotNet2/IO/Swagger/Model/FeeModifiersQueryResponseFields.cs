using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class FeeModifiersQueryResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Fee that this Fee Modifier applies.
    /// </summary>
    /// <value>The identifier of the Fee that this Fee Modifier applies.</value>
    [DataMember(Name="fee", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fee")]
    public string Fee { get; set; }

    /// <summary>
    /// The identifier of the Entity that this Fee Modifier applies for.
    /// </summary>
    /// <value>The identifier of the Entity that this Fee Modifier applies for.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The identifier of the Org this Fee Modifiers should apply for on behalf of the Entity identified in the value of the 'entity' field.  This field is optional. If it is set, then the Fee Modifier is applied to this Org instead.
    /// </summary>
    /// <value>The identifier of the Org this Fee Modifiers should apply for on behalf of the Entity identified in the value of the 'entity' field.  This field is optional. If it is set, then the Fee Modifier is applied to this Org instead.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// The identifier of the Entity who should pay this Fee on behalf of the Entity identified in the value of the 'entity' or 'org' field.  This field is optional. If it is set, then the Fee is charged to this Entity instead.
    /// </summary>
    /// <value>The identifier of the Entity who should pay this Fee on behalf of the Entity identified in the value of the 'entity' or 'org' field.  This field is optional. If it is set, then the Fee is charged to this Entity instead.</value>
    [DataMember(Name="fromentity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fromentity")]
    public string Fromentity { get; set; }

    /// <summary>
    /// The unit of measure for the markup amount for the Fee.  Valid values are:  '2': The markup is a fixed amount, specified in the 'markupAmount' field as an integer in cents.  '1': The markup is a percentage of the fee amount, specified in the 'markupAmount' field in basis points.
    /// </summary>
    /// <value>The unit of measure for the markup amount for the Fee.  Valid values are:  '2': The markup is a fixed amount, specified in the 'markupAmount' field as an integer in cents.  '1': The markup is a percentage of the fee amount, specified in the 'markupAmount' field in basis points.</value>
    [DataMember(Name="markupUm", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "markupUm")]
    public int? MarkupUm { get; set; }

    /// <summary>
    /// The total amount of the markup value for this Fee.  This field is specified as an integer.  The units used in this field are determined by the value of the 'markupUm' field on the Fee. If the 'markupUm' field is set to 'percentage', then this field specifies the Fee percentage to levy in basis points. If the 'markupUm' field is set to 'actual', then this field specifies the markup amount in cents.
    /// </summary>
    /// <value>The total amount of the markup value for this Fee.  This field is specified as an integer.  The units used in this field are determined by the value of the 'markupUm' field on the Fee. If the 'markupUm' field is set to 'percentage', then this field specifies the Fee percentage to levy in basis points. If the 'markupUm' field is set to 'actual', then this field specifies the markup amount in cents.</value>
    [DataMember(Name="markupAmount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "markupAmount")]
    public string MarkupAmount { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class FeeModifiersQueryResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Fee: ").Append(Fee).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Fromentity: ").Append(Fromentity).Append("\n");
      sb.Append("  MarkupUm: ").Append(MarkupUm).Append("\n");
      sb.Append("  MarkupAmount: ").Append(MarkupAmount).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
