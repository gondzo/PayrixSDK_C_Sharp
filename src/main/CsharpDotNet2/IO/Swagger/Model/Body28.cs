using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body28 {
    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the Fund that this entityReserves resource relates to.
    /// </summary>
    /// <value>The identifier of the Fund that this entityReserves resource relates to.</value>
    [DataMember(Name="fund", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fund")]
    public string Fund { get; set; }

    /// <summary>
    /// The amount held in this entityReserve.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount held in this entityReserve.  This field is specified as an integer in cents.</value>
    [DataMember(Name="total", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "total")]
    public int? Total { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body28 {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Fund: ").Append(Fund).Append("\n");
      sb.Append("  Total: ").Append(Total).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
