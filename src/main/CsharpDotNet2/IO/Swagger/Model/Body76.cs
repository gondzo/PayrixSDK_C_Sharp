using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body76 {
    /// <summary>
    /// The identifier of the Plan that this Subscription is associated with.  The Plan determines the frequency and amount of each payment.
    /// </summary>
    /// <value>The identifier of the Plan that this Subscription is associated with.  The Plan determines the frequency and amount of each payment.</value>
    [DataMember(Name="plan", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "plan")]
    public string Plan { get; set; }

    /// <summary>
    /// The date on which the Subscription should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future.
    /// </summary>
    /// <value>The date on which the Subscription should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future.</value>
    [DataMember(Name="start", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "start")]
    public int? Start { get; set; }

    /// <summary>
    /// The date on which the Subscription should finish.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future.
    /// </summary>
    /// <value>The date on which the Subscription should finish.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future.</value>
    [DataMember(Name="finish", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "finish")]
    public int? Finish { get; set; }

    /// <summary>
    /// The amount of the total sum of this Subscription that is made up of tax.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount of the total sum of this Subscription that is made up of tax.  This field is specified as an integer in cents.</value>
    [DataMember(Name="tax", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tax")]
    public int? Tax { get; set; }

    /// <summary>
    /// The descriptor used in this Subscription.   This field is stored as a text string and must be between 1 and 50 characters long. If a value is not set, an attempt is made to set a default value from the merchant information.
    /// </summary>
    /// <value>The descriptor used in this Subscription.   This field is stored as a text string and must be between 1 and 50 characters long. If a value is not set, an attempt is made to set a default value from the merchant information.</value>
    [DataMember(Name="descriptor", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "descriptor")]
    public string Descriptor { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body76 {\n");
      sb.Append("  Plan: ").Append(Plan).Append("\n");
      sb.Append("  Start: ").Append(Start).Append("\n");
      sb.Append("  Finish: ").Append(Finish).Append("\n");
      sb.Append("  Tax: ").Append(Tax).Append("\n");
      sb.Append("  Descriptor: ").Append(Descriptor).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
