using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body7 {
    /// <summary>
    /// The identifier of the Alert resource that you want to invoke with this trigger.
    /// </summary>
    /// <value>The identifier of the Alert resource that you want to invoke with this trigger.</value>
    [DataMember(Name="alert", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "alert")]
    public string Alert { get; set; }

    /// <summary>
    /// The event type that triggers the associated Alert.  Valid values are:  'create': Triggers when the associated  resource is created.  'update': Triggers when the associated resource is updated.  'delete': Triggers when the associated  resource is created.  'ownership': Triggers when the ownership of the associated resource changes.  'board': Triggers when a Merchant is boarded.  'txnhold': Triggers when a transaction is held for review.  'batch': Triggers when Transactions are captured in a batch.  'account': Triggers when the Account associated with a Merchant is updated.  'payout': Triggers when a Payout occurs.  'fee': Triggers when an Entity is charged a Fee. 
    /// </summary>
    /// <value>The event type that triggers the associated Alert.  Valid values are:  'create': Triggers when the associated  resource is created.  'update': Triggers when the associated resource is updated.  'delete': Triggers when the associated  resource is created.  'ownership': Triggers when the ownership of the associated resource changes.  'board': Triggers when a Merchant is boarded.  'txnhold': Triggers when a transaction is held for review.  'batch': Triggers when Transactions are captured in a batch.  'account': Triggers when the Account associated with a Merchant is updated.  'payout': Triggers when a Payout occurs.  'fee': Triggers when an Entity is charged a Fee. </value>
    [DataMember(Name="event", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "event")]
    public string _Event { get; set; }

    /// <summary>
    /// Gets or Sets Resource
    /// </summary>
    [DataMember(Name="resource", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "resource")]
    public int? Resource { get; set; }

    /// <summary>
    /// The name of this alertTrigger.  This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>The name of this alertTrigger.  This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// Gets or Sets Description
    /// </summary>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body7 {\n");
      sb.Append("  Alert: ").Append(Alert).Append("\n");
      sb.Append("  _Event: ").Append(_Event).Append("\n");
      sb.Append("  Resource: ").Append(Resource).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
