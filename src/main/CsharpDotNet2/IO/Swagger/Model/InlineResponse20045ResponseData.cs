using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InlineResponse20045ResponseData {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The Login that will own the Payout resource. When set to null, the Payout resource will be owned by the triggerring Entity.
    /// </summary>
    /// <value>The Login that will own the Payout resource. When set to null, the Payout resource will be owned by the triggerring Entity.</value>
    [DataMember(Name="payoutLogin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "payoutLogin")]
    public string PayoutLogin { get; set; }

    /// <summary>
    /// The identifier of the Org that this payoutFlows resource applies to.  If you set this field, then the payoutFlow applies to all Entities in the Org.
    /// </summary>
    /// <value>The identifier of the Org that this payoutFlows resource applies to.  If you set this field, then the payoutFlow applies to all Entities in the Org.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// The identifier of the Entity that this payoutFlow applies to.
    /// </summary>
    /// <value>The identifier of the Entity that this payoutFlow applies to.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The event on the Org or Entity that should trigger the creation of an associated Payout resource.  Valid values are:  '1': Trigger the creation of the Payout when the primary bank account is associated.  '2': Trigger the creation of the Payout when the Merchant is boarded.
    /// </summary>
    /// <value>The event on the Org or Entity that should trigger the creation of an associated Payout resource.  Valid values are:  '1': Trigger the creation of the Payout when the primary bank account is associated.  '2': Trigger the creation of the Payout when the Merchant is boarded.</value>
    [DataMember(Name="trigger", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "trigger")]
    public int? Trigger { get; set; }

    /// <summary>
    /// The schedule that determines when the Payout resource that is created should be triggered to be paid.  Valid values are:  '1': Daily - the Payout is paid every day.  '2': Weekly - the Payout is paid every week.  '3': Monthly - the Payout is paid every month.  '4': Annually - the Payout is paid every year.  '5': Single - the Payout is a one-off payment.
    /// </summary>
    /// <value>The schedule that determines when the Payout resource that is created should be triggered to be paid.  Valid values are:  '1': Daily - the Payout is paid every day.  '2': Weekly - the Payout is paid every week.  '3': Monthly - the Payout is paid every month.  '4': Annually - the Payout is paid every year.  '5': Single - the Payout is a one-off payment.</value>
    [DataMember(Name="schedule", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "schedule")]
    public int? Schedule { get; set; }

    /// <summary>
    /// A multiplier that you can use to adjust the schedule set in the 'schedule' field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This affects the Payout resource that is created by this payoutFlow.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if 'schedule' is set to '1' (meaning 'daily'), then a 'scheduleFactor' value of '2' would cause the Payout to trigger every two days.
    /// </summary>
    /// <value>A multiplier that you can use to adjust the schedule set in the 'schedule' field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This affects the Payout resource that is created by this payoutFlow.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if 'schedule' is set to '1' (meaning 'daily'), then a 'scheduleFactor' value of '2' would cause the Payout to trigger every two days.</value>
    [DataMember(Name="scheduleFactor", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "scheduleFactor")]
    public int? ScheduleFactor { get; set; }

    /// <summary>
    /// The unit of measure for the Payout resource that is created.  Valid values are:  '1': Percentage - the Payout is a percentage of the current available funds for this Entity that should be paid to their Account, specified in the 'amount' field in basis points. '2': Actual - the Payout is a fixed amount, specified in the 'amount' field as an integer in cents.  '3': Negative percentage - the Payout is a percentage of the balance, specified in the 'amount' field as a negative integer in basis points. The direction of the Payout payment is reversed. For example, if the Entity has a negative balance of $10 and the amount is set to 10000 (100%), then $10 will be drawn from their account to fully replenish the balance to $0.
    /// </summary>
    /// <value>The unit of measure for the Payout resource that is created.  Valid values are:  '1': Percentage - the Payout is a percentage of the current available funds for this Entity that should be paid to their Account, specified in the 'amount' field in basis points. '2': Actual - the Payout is a fixed amount, specified in the 'amount' field as an integer in cents.  '3': Negative percentage - the Payout is a percentage of the balance, specified in the 'amount' field as a negative integer in basis points. The direction of the Payout payment is reversed. For example, if the Entity has a negative balance of $10 and the amount is set to 10000 (100%), then $10 will be drawn from their account to fully replenish the balance to $0.</value>
    [DataMember(Name="um", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "um")]
    public int? Um { get; set; }

    /// <summary>
    /// Gets or Sets Amount
    /// </summary>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public int? Amount { get; set; }

    /// <summary>
    /// Gets or Sets Minimum
    /// </summary>
    [DataMember(Name="minimum", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "minimum")]
    public int? Minimum { get; set; }

    /// <summary>
    /// Whether the Payout resource will be marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether the Payout resource will be marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="payoutInactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "payoutInactive")]
    public int? PayoutInactive { get; set; }

    /// <summary>
    /// Whether the Payout resource will be marked to skip the creation of disbursements on holidays and weekends. A value of '1' means skip and a value of '0' means do not skip.
    /// </summary>
    /// <value>Whether the Payout resource will be marked to skip the creation of disbursements on holidays and weekends. A value of '1' means skip and a value of '0' means do not skip.</value>
    [DataMember(Name="skipOffDays", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "skipOffDays")]
    public int? SkipOffDays { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InlineResponse20045ResponseData {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  PayoutLogin: ").Append(PayoutLogin).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Trigger: ").Append(Trigger).Append("\n");
      sb.Append("  Schedule: ").Append(Schedule).Append("\n");
      sb.Append("  ScheduleFactor: ").Append(ScheduleFactor).Append("\n");
      sb.Append("  Um: ").Append(Um).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("  Minimum: ").Append(Minimum).Append("\n");
      sb.Append("  PayoutInactive: ").Append(PayoutInactive).Append("\n");
      sb.Append("  SkipOffDays: ").Append(SkipOffDays).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
