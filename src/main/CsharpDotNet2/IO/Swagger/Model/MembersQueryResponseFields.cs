using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class MembersQueryResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Merchant associated with this Member.
    /// </summary>
    /// <value>The identifier of the Merchant associated with this Member.</value>
    [DataMember(Name="merchant", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "merchant")]
    public string Merchant { get; set; }

    /// <summary>
    /// The title that this Member holds in relation to the associated Merchant.  For example, 'CEO', 'Owner' or 'Director of Finance'.
    /// </summary>
    /// <value>The title that this Member holds in relation to the associated Merchant.  For example, 'CEO', 'Owner' or 'Director of Finance'.</value>
    [DataMember(Name="title", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "title")]
    public string Title { get; set; }

    /// <summary>
    /// The first name associated with this Member.
    /// </summary>
    /// <value>The first name associated with this Member.</value>
    [DataMember(Name="first", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "first")]
    public string First { get; set; }

    /// <summary>
    /// The middle name associated with this Member.
    /// </summary>
    /// <value>The middle name associated with this Member.</value>
    [DataMember(Name="middle", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "middle")]
    public string Middle { get; set; }

    /// <summary>
    /// The last name associated with this Member.
    /// </summary>
    /// <value>The last name associated with this Member.</value>
    [DataMember(Name="last", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "last")]
    public string Last { get; set; }

    /// <summary>
    /// The social security number of this Member. This field is required if the Merchant associated with the Member is a sole trader.
    /// </summary>
    /// <value>The social security number of this Member. This field is required if the Merchant associated with the Member is a sole trader.</value>
    [DataMember(Name="ssn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ssn")]
    public string Ssn { get; set; }

    /// <summary>
    /// The date of birth of this Member.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.
    /// </summary>
    /// <value>The date of birth of this Member.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.</value>
    [DataMember(Name="dob", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "dob")]
    public int? Dob { get; set; }

    /// <summary>
    /// The driver's license number of this Member.
    /// </summary>
    /// <value>The driver's license number of this Member.</value>
    [DataMember(Name="dl", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "dl")]
    public string Dl { get; set; }

    /// <summary>
    /// The U.S. state where the driver's license of this Member was issued.  Valid values are any U.S. state's 2 character postal abbreviation.
    /// </summary>
    /// <value>The U.S. state where the driver's license of this Member was issued.  Valid values are any U.S. state's 2 character postal abbreviation.</value>
    [DataMember(Name="dlstate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "dlstate")]
    public string Dlstate { get; set; }

    /// <summary>
    /// The share of the Member's ownership of the associated Merchant, expressed in basis points.  For example, 25.3% is expressed as '2530'.
    /// </summary>
    /// <value>The share of the Member's ownership of the associated Merchant, expressed in basis points.  For example, 25.3% is expressed as '2530'.</value>
    [DataMember(Name="ownership", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ownership")]
    public int? Ownership { get; set; }

    /// <summary>
    /// The email address of this Member.
    /// </summary>
    /// <value>The email address of this Member.</value>
    [DataMember(Name="email", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "email")]
    public string Email { get; set; }

    /// <summary>
    /// The fax number associated with this Member.  This field is stored as a text string and must be between 10 and 15 characters long.
    /// </summary>
    /// <value>The fax number associated with this Member.  This field is stored as a text string and must be between 10 and 15 characters long.</value>
    [DataMember(Name="fax", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fax")]
    public string Fax { get; set; }

    /// <summary>
    /// The phone number associated with this Member.  This field is stored as a text string and must be between 10 and 15 characters long.
    /// </summary>
    /// <value>The phone number associated with this Member.  This field is stored as a text string and must be between 10 and 15 characters long.</value>
    [DataMember(Name="phone", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "phone")]
    public string Phone { get; set; }

    /// <summary>
    /// The country in the address associated with the Member. Currently, this field only accepts the value 'USA'.
    /// </summary>
    /// <value>The country in the address associated with the Member. Currently, this field only accepts the value 'USA'.</value>
    [DataMember(Name="country", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "country")]
    public string Country { get; set; }

    /// <summary>
    /// The ZIP code in the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The ZIP code in the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="zip", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "zip")]
    public string Zip { get; set; }

    /// <summary>
    /// The U.S. state associated with this Member.  Valid values are any U.S. state's 2 character postal abbreviation.
    /// </summary>
    /// <value>The U.S. state associated with this Member.  Valid values are any U.S. state's 2 character postal abbreviation.</value>
    [DataMember(Name="state", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "state")]
    public string State { get; set; }

    /// <summary>
    /// The name of the city in the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The name of the city in the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="city", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "city")]
    public string City { get; set; }

    /// <summary>
    /// The second line of the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The second line of the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="address2", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address2")]
    public string Address2 { get; set; }

    /// <summary>
    /// The first line of the address associated with this Member.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The first line of the address associated with this Member.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="address1", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address1")]
    public string Address1 { get; set; }

    /// <summary>
    /// Indicates whether the Member is the 'primary' contact for the associated Merchant. Only one Member associated with each Merchant can be the 'primary' Member.  A value of '1' means primary and a value of '0' means not primary.
    /// </summary>
    /// <value>Indicates whether the Member is the 'primary' contact for the associated Merchant. Only one Member associated with each Merchant can be the 'primary' Member.  A value of '1' means primary and a value of '0' means not primary.</value>
    [DataMember(Name="primary", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "primary")]
    public int? Primary { get; set; }

    /// <summary>
    /// Whether this Member is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this Member is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this Member should be marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this Member should be marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class MembersQueryResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Merchant: ").Append(Merchant).Append("\n");
      sb.Append("  Title: ").Append(Title).Append("\n");
      sb.Append("  First: ").Append(First).Append("\n");
      sb.Append("  Middle: ").Append(Middle).Append("\n");
      sb.Append("  Last: ").Append(Last).Append("\n");
      sb.Append("  Ssn: ").Append(Ssn).Append("\n");
      sb.Append("  Dob: ").Append(Dob).Append("\n");
      sb.Append("  Dl: ").Append(Dl).Append("\n");
      sb.Append("  Dlstate: ").Append(Dlstate).Append("\n");
      sb.Append("  Ownership: ").Append(Ownership).Append("\n");
      sb.Append("  Email: ").Append(Email).Append("\n");
      sb.Append("  Fax: ").Append(Fax).Append("\n");
      sb.Append("  Phone: ").Append(Phone).Append("\n");
      sb.Append("  Country: ").Append(Country).Append("\n");
      sb.Append("  Zip: ").Append(Zip).Append("\n");
      sb.Append("  State: ").Append(State).Append("\n");
      sb.Append("  City: ").Append(City).Append("\n");
      sb.Append("  Address2: ").Append(Address2).Append("\n");
      sb.Append("  Address1: ").Append(Address1).Append("\n");
      sb.Append("  Primary: ").Append(Primary).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
