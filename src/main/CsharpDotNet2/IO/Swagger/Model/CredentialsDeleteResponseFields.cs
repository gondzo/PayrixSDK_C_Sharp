using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class CredentialsDeleteResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Entity that this Credential resource belongs to.
    /// </summary>
    /// <value>The identifier of the Entity that this Credential resource belongs to.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The name of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The name of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A description of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>A description of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The username to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.
    /// </summary>
    /// <value>The username to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.</value>
    [DataMember(Name="username", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "username")]
    public string Username { get; set; }

    /// <summary>
    /// The password to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.
    /// </summary>
    /// <value>The password to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.</value>
    [DataMember(Name="password", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "password")]
    public string Password { get; set; }

    /// <summary>
    /// The username to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.
    /// </summary>
    /// <value>The username to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.</value>
    [DataMember(Name="connectUsername", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "connectUsername")]
    public string ConnectUsername { get; set; }

    /// <summary>
    /// The password to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.
    /// </summary>
    /// <value>The password to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.</value>
    [DataMember(Name="connectPassword", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "connectPassword")]
    public string ConnectPassword { get; set; }

    /// <summary>
    /// Gets or Sets Integration
    /// </summary>
    [DataMember(Name="integration", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "integration")]
    public string Integration { get; set; }

    /// <summary>
    /// The type of action that this Credential is authorized to perform.  Valid values are:  '1': Transaction - this Credential can be used to send Transactions to the processor.  '2': Batch - this Credential can be used to send Batches to the processor for settlement.  '3': Boarding - this Credential can be used to board a Merchant with the processor.  '4': Payout - this Credential can be used to send out a Payout instruction.  '5': Chargeback - this Credential can be used to retrieve or update a Chargeback with the processor.  '6': Report - this Credential can be used to receive reports from the processor.  '7': Account - this Credential can be used to verify a bank account when you use it with a bank account verification Integration.  '8': Verification - this Credential can be used to verify data about a Merchant when you use it with a merchant verification integration.
    /// </summary>
    /// <value>The type of action that this Credential is authorized to perform.  Valid values are:  '1': Transaction - this Credential can be used to send Transactions to the processor.  '2': Batch - this Credential can be used to send Batches to the processor for settlement.  '3': Boarding - this Credential can be used to board a Merchant with the processor.  '4': Payout - this Credential can be used to send out a Payout instruction.  '5': Chargeback - this Credential can be used to retrieve or update a Chargeback with the processor.  '6': Report - this Credential can be used to receive reports from the processor.  '7': Account - this Credential can be used to verify a bank account when you use it with a bank account verification Integration.  '8': Verification - this Credential can be used to verify data about a Merchant when you use it with a merchant verification integration.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class CredentialsDeleteResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Username: ").Append(Username).Append("\n");
      sb.Append("  Password: ").Append(Password).Append("\n");
      sb.Append("  ConnectUsername: ").Append(ConnectUsername).Append("\n");
      sb.Append("  ConnectPassword: ").Append(ConnectPassword).Append("\n");
      sb.Append("  Integration: ").Append(Integration).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
