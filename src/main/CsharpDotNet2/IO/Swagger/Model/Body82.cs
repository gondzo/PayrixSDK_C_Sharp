using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body82 {
    /// <summary>
    /// The Customer that this Token is associated with.
    /// </summary>
    /// <value>The Customer that this Token is associated with.</value>
    [DataMember(Name="customer", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "customer")]
    public string Customer { get; set; }

    /// <summary>
    /// Gets or Sets Payment
    /// </summary>
    [DataMember(Name="payment", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "payment")]
    public TokensidPayment Payment { get; set; }

    /// <summary>
    /// The expiry month for the payment method associated with this Token.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.  The value must reflect a future date.
    /// </summary>
    /// <value>The expiry month for the payment method associated with this Token.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.  The value must reflect a future date.</value>
    [DataMember(Name="expiration", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "expiration")]
    public string Expiration { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body82 {\n");
      sb.Append("  Customer: ").Append(Customer).Append("\n");
      sb.Append("  Payment: ").Append(Payment).Append("\n");
      sb.Append("  Expiration: ").Append(Expiration).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
