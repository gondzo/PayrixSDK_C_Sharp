using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// Where the response lists multiple resources, the API splits the response into several &#39;pages&#39;.  This object indicates the current and last available pages in the list.
  /// </summary>
  [DataContract]
  public class InlineResponse200ResponseDetailsPage {
    /// <summary>
    /// The current page in the paginated resource list.
    /// </summary>
    /// <value>The current page in the paginated resource list.</value>
    [DataMember(Name="current", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "current")]
    public int? Current { get; set; }

    /// <summary>
    /// The last available page in the paginated resource list.
    /// </summary>
    /// <value>The last available page in the paginated resource list.</value>
    [DataMember(Name="last", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "last")]
    public int? Last { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InlineResponse200ResponseDetailsPage {\n");
      sb.Append("  Current: ").Append(Current).Append("\n");
      sb.Append("  Last: ").Append(Last).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
