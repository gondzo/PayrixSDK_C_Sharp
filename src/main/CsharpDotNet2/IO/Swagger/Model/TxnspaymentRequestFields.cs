using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// The payment method associated with this Transaction, including the card details.
  /// </summary>
  [DataContract]
  public class TxnspaymentRequestFields {
    /// <summary>
    /// The payment method for thei Transaction.  This field is specified as an integer.  Valid values are:  '1': American Express  '2': Visa  '3': MasterCard  '4': Diners Club  '5': Discover  '6': PayPal  '7': Debit card  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account  '12': Gift card  '13': EBT  '14':WIC.
    /// </summary>
    /// <value>The payment method for thei Transaction.  This field is specified as an integer.  Valid values are:  '1': American Express  '2': Visa  '3': MasterCard  '4': Diners Club  '5': Discover  '6': PayPal  '7': Debit card  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account  '12': Gift card  '13': EBT  '14':WIC.</value>
    [DataMember(Name="method", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "method")]
    public int? Method { get; set; }

    /// <summary>
    /// The card number of the credit card associated with this Transaction.
    /// </summary>
    /// <value>The card number of the credit card associated with this Transaction.</value>
    [DataMember(Name="number", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "number")]
    public string Number { get; set; }

    /// <summary>
    /// The routing code for the eCheck or bank account payment associated with this Transaction.
    /// </summary>
    /// <value>The routing code for the eCheck or bank account payment associated with this Transaction.</value>
    [DataMember(Name="routing", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "routing")]
    public string Routing { get; set; }

    /// <summary>
    /// The expiration date of the credit card associated with this Transaction.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.
    /// </summary>
    /// <value>The expiration date of the credit card associated with this Transaction.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.</value>
    [DataMember(Name="expiration", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "expiration")]
    public string Expiration { get; set; }

    /// <summary>
    /// The Card Verification Value (CVV) number of the credit card associated with this Transaction.  This field is expressed as an integer.
    /// </summary>
    /// <value>The Card Verification Value (CVV) number of the credit card associated with this Transaction.  This field is expressed as an integer.</value>
    [DataMember(Name="cvv", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "cvv")]
    public int? Cvv { get; set; }

    /// <summary>
    /// The 'Track' data (either Track 1, Track 2, or both) of the card associated with this Transaction.  This field is stored as a text string.
    /// </summary>
    /// <value>The 'Track' data (either Track 1, Track 2, or both) of the card associated with this Transaction.  This field is stored as a text string.</value>
    [DataMember(Name="track", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "track")]
    public string Track { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TxnspaymentRequestFields {\n");
      sb.Append("  Method: ").Append(Method).Append("\n");
      sb.Append("  Number: ").Append(Number).Append("\n");
      sb.Append("  Routing: ").Append(Routing).Append("\n");
      sb.Append("  Expiration: ").Append(Expiration).Append("\n");
      sb.Append("  Cvv: ").Append(Cvv).Append("\n");
      sb.Append("  Track: ").Append(Track).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
