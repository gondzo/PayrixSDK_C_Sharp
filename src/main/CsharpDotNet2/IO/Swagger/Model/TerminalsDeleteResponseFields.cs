using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class TerminalsDeleteResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Merchant that owns this terminals resource.
    /// </summary>
    /// <value>The identifier of the Merchant that owns this terminals resource.</value>
    [DataMember(Name="merchant", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "merchant")]
    public string Merchant { get; set; }

    /// <summary>
    /// The type of terminal.  This field is stored as an integer and must be between 1 and 417.
    /// </summary>
    /// <value>The type of terminal.  This field is stored as an integer and must be between 1 and 417.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// How is the terminal employed.  Valid values are:  '1': Retail.  '2': Retail with tips.  '3': Restaurant.  '4': Lodging.  '5': Bar.  '6': Cash Advance.  '7': Mail/Telephone Order.  '8': Pay At The Pump.  '9': Service Rest.  '10': E-commerce.  '11': Direct Marketing.  '12': Fine Dining.  '13': Gift Card Only.
    /// </summary>
    /// <value>How is the terminal employed.  Valid values are:  '1': Retail.  '2': Retail with tips.  '3': Restaurant.  '4': Lodging.  '5': Bar.  '6': Cash Advance.  '7': Mail/Telephone Order.  '8': Pay At The Pump.  '9': Service Rest.  '10': E-commerce.  '11': Direct Marketing.  '12': Fine Dining.  '13': Gift Card Only.</value>
    [DataMember(Name="environment", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "environment")]
    public int? Environment { get; set; }

    /// <summary>
    /// If the terminal should be manually or automatically closed for the day.  Valid values are:  '0': Automatic.  '1': Manual.
    /// </summary>
    /// <value>If the terminal should be manually or automatically closed for the day.  Valid values are:  '0': Automatic.  '1': Manual.</value>
    [DataMember(Name="autoClose", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "autoClose")]
    public int? AutoClose { get; set; }

    /// <summary>
    /// The time when the terminal should be automatically closed for the day. This field is only required when AutoClose is set to automatic.  The format should be HHMM (1145, 2200, etc).
    /// </summary>
    /// <value>The time when the terminal should be automatically closed for the day. This field is only required when AutoClose is set to automatic.  The format should be HHMM (1145, 2200, etc).</value>
    [DataMember(Name="autoCloseTime", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "autoCloseTime")]
    public int? AutoCloseTime { get; set; }

    /// <summary>
    /// The name of this Terminal.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The name of this Terminal.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A description of the Terminal.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>A description of the Terminal.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The first line of the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The first line of the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="address1", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address1")]
    public string Address1 { get; set; }

    /// <summary>
    /// The second line of the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The second line of the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="address2", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address2")]
    public string Address2 { get; set; }

    /// <summary>
    /// The name of the city in the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The name of the city in the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="city", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "city")]
    public string City { get; set; }

    /// <summary>
    /// The U.S. state in the address associated with this Terminal's location. Valid values are: AL, AK, AZ, AR, CA, CO, CT, DE, DC, FL, GA, HI, ID, IL, IN, IA, KS, KY, LA, ME, MD, MA, MI, MN, MS, MO, MT, NE, NV, NH, NJ, NM, NY, NC, ND, OH, OK, OR, PA, RI, SC, SD, TN, TX, UT, VT, VA, WA, WV, WI and WY.
    /// </summary>
    /// <value>The U.S. state in the address associated with this Terminal's location. Valid values are: AL, AK, AZ, AR, CA, CO, CT, DE, DC, FL, GA, HI, ID, IL, IN, IA, KS, KY, LA, ME, MD, MA, MI, MN, MS, MO, MT, NE, NV, NH, NJ, NM, NY, NC, ND, OH, OK, OR, PA, RI, SC, SD, TN, TX, UT, VT, VA, WA, WV, WI and WY.</value>
    [DataMember(Name="state", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "state")]
    public string State { get; set; }

    /// <summary>
    /// The ZIP code in the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The ZIP code in the address associated with this Terminal's location.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="zip", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "zip")]
    public string Zip { get; set; }

    /// <summary>
    /// The country in the address associated with the Terminal's location. Currently, this field only accepts the value 'USA'.
    /// </summary>
    /// <value>The country in the address associated with the Terminal's location. Currently, this field only accepts the value 'USA'.</value>
    [DataMember(Name="country", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "country")]
    public string Country { get; set; }

    /// <summary>
    /// Gets or Sets Timezone
    /// </summary>
    [DataMember(Name="timezone", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "timezone")]
    public int? Timezone { get; set; }

    /// <summary>
    /// The current status of the terminal.  Valid values are:  '0': Inactive.  '1': Active.  This field is optional and is set to Active by default.
    /// </summary>
    /// <value>The current status of the terminal.  Valid values are:  '0': Inactive.  '1': Active.  This field is optional and is set to Active by default.</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TerminalsDeleteResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Merchant: ").Append(Merchant).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Environment: ").Append(Environment).Append("\n");
      sb.Append("  AutoClose: ").Append(AutoClose).Append("\n");
      sb.Append("  AutoCloseTime: ").Append(AutoCloseTime).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Address1: ").Append(Address1).Append("\n");
      sb.Append("  Address2: ").Append(Address2).Append("\n");
      sb.Append("  City: ").Append(City).Append("\n");
      sb.Append("  State: ").Append(State).Append("\n");
      sb.Append("  Zip: ").Append(Zip).Append("\n");
      sb.Append("  Country: ").Append(Country).Append("\n");
      sb.Append("  Timezone: ").Append(Timezone).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
