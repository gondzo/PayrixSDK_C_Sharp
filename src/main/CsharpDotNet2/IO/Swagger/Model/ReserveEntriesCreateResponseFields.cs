using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ReserveEntriesCreateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the Fund that this reserveEntries resource relates to.
    /// </summary>
    /// <value>The identifier of the Fund that this reserveEntries resource relates to.</value>
    [DataMember(Name="fund", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fund")]
    public string Fund { get; set; }

    /// <summary>
    /// This field indicates that this reserveEntry was triggered from a Transaction.  This field stores the identifier of the Transaction.
    /// </summary>
    /// <value>This field indicates that this reserveEntry was triggered from a Transaction.  This field stores the identifier of the Transaction.</value>
    [DataMember(Name="txn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txn")]
    public string Txn { get; set; }

    /// <summary>
    /// Gets or Sets TxnHold
    /// </summary>
    [DataMember(Name="txnHold", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txnHold")]
    public string TxnHold { get; set; }

    /// <summary>
    /// This field indicates that this reserveEntry was triggered from an automatic reserve.  This field stores the identifier of the Reserve resource.
    /// </summary>
    /// <value>This field indicates that this reserveEntry was triggered from an automatic reserve.  This field stores the identifier of the Reserve resource.</value>
    [DataMember(Name="reserve", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reserve")]
    public string Reserve { get; set; }

    /// <summary>
    /// This field indicates that this reserveEntry was triggered from a manual change to an entityReserve.  This field stores the identifier of the entityReserve resource.
    /// </summary>
    /// <value>This field indicates that this reserveEntry was triggered from a manual change to an entityReserve.  This field stores the identifier of the entityReserve resource.</value>
    [DataMember(Name="entityReserve", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entityReserve")]
    public string EntityReserve { get; set; }

    /// <summary>
    /// This field indicates that this reserveEntry shows funds moving out of reserve.  This field stores the identifier of the reserveEntry resource that moved the funds into the reserve.
    /// </summary>
    /// <value>This field indicates that this reserveEntry shows funds moving out of reserve.  This field stores the identifier of the reserveEntry resource that moved the funds into the reserve.</value>
    [DataMember(Name="reserveEntry", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reserveEntry")]
    public string ReserveEntry { get; set; }

    /// <summary>
    /// A description of this reserveEntries resource.   This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>A description of this reserveEntries resource.   This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The date on which the funds in reserve should be released.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.
    /// </summary>
    /// <value>The date on which the funds in reserve should be released.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.</value>
    [DataMember(Name="release", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "release")]
    public int? Release { get; set; }

    /// <summary>
    /// The amount held in reserve in this reserveEntries resource.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount held in reserve in this reserveEntries resource.  This field is specified as an integer in cents.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public int? Amount { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ReserveEntriesCreateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Fund: ").Append(Fund).Append("\n");
      sb.Append("  Txn: ").Append(Txn).Append("\n");
      sb.Append("  TxnHold: ").Append(TxnHold).Append("\n");
      sb.Append("  Reserve: ").Append(Reserve).Append("\n");
      sb.Append("  EntityReserve: ").Append(EntityReserve).Append("\n");
      sb.Append("  ReserveEntry: ").Append(ReserveEntry).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Release: ").Append(Release).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
