using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body57 {
    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the Login resource for which this orgFlows resource is triggered.
    /// </summary>
    /// <value>The identifier of the Login resource for which this orgFlows resource is triggered.</value>
    [DataMember(Name="forlogin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "forlogin")]
    public string Forlogin { get; set; }

    /// <summary>
    /// Whether this resource should affect logins recursively - in other words, affect this Login and all its child Logins.  Valid values are:  '0': Not recursive. The orgFlow only affects the Login identified in the 'forLogin' field.  '1': Recursive. The orgFlow affects the Login identified in the 'forLogin' field and all its child Logins.
    /// </summary>
    /// <value>Whether this resource should affect logins recursively - in other words, affect this Login and all its child Logins.  Valid values are:  '0': Not recursive. The orgFlow only affects the Login identified in the 'forLogin' field.  '1': Recursive. The orgFlow affects the Login identified in the 'forLogin' field and all its child Logins.</value>
    [DataMember(Name="recursive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "recursive")]
    public int? Recursive { get; set; }

    /// <summary>
    /// This field sets the trigger that determines when this orgFlow runs.   Valid values are:  '1': Trigger at Merchant creation time.  '2': Trigger when a Merchant check returns a low score.  '3': Trigger when a Merchant check returns a high score.  '4': Trigger at Merchant boarding time.
    /// </summary>
    /// <value>This field sets the trigger that determines when this orgFlow runs.   Valid values are:  '1': Trigger at Merchant creation time.  '2': Trigger when a Merchant check returns a low score.  '3': Trigger when a Merchant check returns a high score.  '4': Trigger at Merchant boarding time.</value>
    [DataMember(Name="trigger", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "trigger")]
    public int? Trigger { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body57 {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Forlogin: ").Append(Forlogin).Append("\n");
      sb.Append("  Recursive: ").Append(Recursive).Append("\n");
      sb.Append("  Trigger: ").Append(Trigger).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
