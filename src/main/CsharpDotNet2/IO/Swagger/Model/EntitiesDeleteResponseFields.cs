using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class EntitiesDeleteResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The incoming ip address from which this Entity was created.
    /// </summary>
    /// <value>The incoming ip address from which this Entity was created.</value>
    [DataMember(Name="ipCreated", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ipCreated")]
    public string IpCreated { get; set; }

    /// <summary>
    /// The incoming ip address from which this Entity was last modified.
    /// </summary>
    /// <value>The incoming ip address from which this Entity was last modified.</value>
    [DataMember(Name="ipModified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ipModified")]
    public string IpModified { get; set; }

    /// <summary>
    /// The client ip address from which the Entity was created.  Valid values are any Ipv4 or Ipv6 address.
    /// </summary>
    /// <value>The client ip address from which the Entity was created.  Valid values are any Ipv4 or Ipv6 address.</value>
    [DataMember(Name="clientIp", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "clientIp")]
    public string ClientIp { get; set; }

    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The parameter associated with this Entity.
    /// </summary>
    /// <value>The parameter associated with this Entity.</value>
    [DataMember(Name="parameter", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "parameter")]
    public string _Parameter { get; set; }

    /// <summary>
    /// The type of Entity.  This field is specified as an integer.  Valid values are '0' (sole proprietor), '1' (corporation), '2' (limited liability company), '3' (partnership), '4' (association), '5' (non-profit organization) and '6' (government organization).
    /// </summary>
    /// <value>The type of Entity.  This field is specified as an integer.  Valid values are '0' (sole proprietor), '1' (corporation), '2' (limited liability company), '3' (partnership), '4' (association), '5' (non-profit organization) and '6' (government organization).</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// The name of this Entity.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The name of this Entity.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// The first line of the address associated with this Entity.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The first line of the address associated with this Entity.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="address1", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address1")]
    public string Address1 { get; set; }

    /// <summary>
    /// The second line of the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The second line of the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="address2", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address2")]
    public string Address2 { get; set; }

    /// <summary>
    /// The name of the city in the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The name of the city in the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="city", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "city")]
    public string City { get; set; }

    /// <summary>
    /// The U.S. state associated with this Entity.  Valid values are any U.S. state's 2 character postal abbreviation.
    /// </summary>
    /// <value>The U.S. state associated with this Entity.  Valid values are any U.S. state's 2 character postal abbreviation.</value>
    [DataMember(Name="state", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "state")]
    public string State { get; set; }

    /// <summary>
    /// The ZIP code in the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The ZIP code in the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="zip", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "zip")]
    public string Zip { get; set; }

    /// <summary>
    /// The country in the address associated with the Entity. Currently, this field only accepts the value 'USA'.
    /// </summary>
    /// <value>The country in the address associated with the Entity. Currently, this field only accepts the value 'USA'.</value>
    [DataMember(Name="country", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "country")]
    public string Country { get; set; }

    /// <summary>
    /// Gets or Sets Timezone
    /// </summary>
    [DataMember(Name="timezone", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "timezone")]
    public int? Timezone { get; set; }

    /// <summary>
    /// The phone number associated with this Entity.  This field is stored as a text string and must be between 10 and 15 characters long.
    /// </summary>
    /// <value>The phone number associated with this Entity.  This field is stored as a text string and must be between 10 and 15 characters long.</value>
    [DataMember(Name="phone", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "phone")]
    public string Phone { get; set; }

    /// <summary>
    /// The fax number associated with this Entity.  This field is stored as a text string and must be between 10 and 15 characters long.
    /// </summary>
    /// <value>The fax number associated with this Entity.  This field is stored as a text string and must be between 10 and 15 characters long.</value>
    [DataMember(Name="fax", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fax")]
    public string Fax { get; set; }

    /// <summary>
    /// The email address associated with this Entity.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The email address associated with this Entity.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="email", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "email")]
    public string Email { get; set; }

    /// <summary>
    /// The web site URL associated with this Entity.  This field is stored as a text string and must be between 0 and 50 characters long.
    /// </summary>
    /// <value>The web site URL associated with this Entity.  This field is stored as a text string and must be between 0 and 50 characters long.</value>
    [DataMember(Name="website", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "website")]
    public string Website { get; set; }

    /// <summary>
    /// The IRS Employer ID (EID) number for the Entity.
    /// </summary>
    /// <value>The IRS Employer ID (EID) number for the Entity.</value>
    [DataMember(Name="ein", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ein")]
    public string Ein { get; set; }

    /// <summary>
    /// Gets or Sets TcVersion
    /// </summary>
    [DataMember(Name="tcVersion", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tcVersion")]
    public string TcVersion { get; set; }

    /// <summary>
    /// Gets or Sets TcDate
    /// </summary>
    [DataMember(Name="tcDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tcDate")]
    public DateTime? TcDate { get; set; }

    /// <summary>
    /// Gets or Sets TcIp
    /// </summary>
    [DataMember(Name="tcIp", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tcIp")]
    public string TcIp { get; set; }

    /// <summary>
    /// Gets or Sets TcAcceptDate
    /// </summary>
    [DataMember(Name="tcAcceptDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tcAcceptDate")]
    public int? TcAcceptDate { get; set; }

    /// <summary>
    /// Gets or Sets TcAcceptIp
    /// </summary>
    [DataMember(Name="tcAcceptIp", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tcAcceptIp")]
    public string TcAcceptIp { get; set; }

    /// <summary>
    /// The currency of this Entity.  Currently, this field only accepts the value 'USD'.
    /// </summary>
    /// <value>The currency of this Entity.  Currently, this field only accepts the value 'USD'.</value>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// Custom, free-form field for client-supplied text.
    /// </summary>
    /// <value>Custom, free-form field for client-supplied text.</value>
    [DataMember(Name="custom", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "custom")]
    public string Custom { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class EntitiesDeleteResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  IpCreated: ").Append(IpCreated).Append("\n");
      sb.Append("  IpModified: ").Append(IpModified).Append("\n");
      sb.Append("  ClientIp: ").Append(ClientIp).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  _Parameter: ").Append(_Parameter).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Address1: ").Append(Address1).Append("\n");
      sb.Append("  Address2: ").Append(Address2).Append("\n");
      sb.Append("  City: ").Append(City).Append("\n");
      sb.Append("  State: ").Append(State).Append("\n");
      sb.Append("  Zip: ").Append(Zip).Append("\n");
      sb.Append("  Country: ").Append(Country).Append("\n");
      sb.Append("  Timezone: ").Append(Timezone).Append("\n");
      sb.Append("  Phone: ").Append(Phone).Append("\n");
      sb.Append("  Fax: ").Append(Fax).Append("\n");
      sb.Append("  Email: ").Append(Email).Append("\n");
      sb.Append("  Website: ").Append(Website).Append("\n");
      sb.Append("  Ein: ").Append(Ein).Append("\n");
      sb.Append("  TcVersion: ").Append(TcVersion).Append("\n");
      sb.Append("  TcDate: ").Append(TcDate).Append("\n");
      sb.Append("  TcIp: ").Append(TcIp).Append("\n");
      sb.Append("  TcAcceptDate: ").Append(TcAcceptDate).Append("\n");
      sb.Append("  TcAcceptIp: ").Append(TcAcceptIp).Append("\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  Custom: ").Append(Custom).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
