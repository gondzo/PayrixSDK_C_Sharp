using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class BatchesUpdateRest {
    /// <summary>
    /// The current status of this Batch.  Valid values are:  '0': Open - This Batch can accept more Transactions.  '1': Closed - this Batch is closed to new Transactions and is ready to be sent to the processor.  '2': Processing - this Batch is being processed for settlement.  '3': Processed - this Batch has been processed.
    /// </summary>
    /// <value>The current status of this Batch.  Valid values are:  '0': Open - This Batch can accept more Transactions.  '1': Closed - this Batch is closed to new Transactions and is ready to be sent to the processor.  '2': Processing - this Batch is being processed for settlement.  '3': Processed - this Batch has been processed.</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// The merchant's reference code of the batch.  This field is stored as a text string and must be between 0 and 50 characters long.
    /// </summary>
    /// <value>The merchant's reference code of the batch.  This field is stored as a text string and must be between 0 and 50 characters long.</value>
    [DataMember(Name="clientRef", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "clientRef")]
    public string ClientRef { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BatchesUpdateRest {\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  ClientRef: ").Append(ClientRef).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
