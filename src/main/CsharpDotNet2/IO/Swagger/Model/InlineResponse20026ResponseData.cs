using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InlineResponse20026ResponseData {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Fee that this Fee Rule applies.
    /// </summary>
    /// <value>The identifier of the Fee that this Fee Rule applies.</value>
    [DataMember(Name="fee", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fee")]
    public string Fee { get; set; }

    /// <summary>
    /// The name of this Fee Rule.  This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>The name of this Fee Rule.  This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// Gets or Sets Description
    /// </summary>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The type of logic to apply with this Fee Rule.  This field is specified as an integer.  Valid values are:  '1': Less than - the Fee applies only if the triggered amount is lower than the amount set in the 'value' field of the Fee Rule,  '2': Equal to - the Fee applies only if the transaction amount is exactly the same as the amount set in the 'value' field of the Fee Rule,  '3': Not equal to - the Fee applies only if the transaction amount is not exactly equal to the amount set in the 'value' field of the Fee Rule,  '4': Greater than - the Fee applies only if the transaction amount is higher than the amount set in the 'value' field of the Fee Rule and  '5': Swiped - the Fee applies based on a determination of whether the cardholder was present during the transaction.
    /// </summary>
    /// <value>The type of logic to apply with this Fee Rule.  This field is specified as an integer.  Valid values are:  '1': Less than - the Fee applies only if the triggered amount is lower than the amount set in the 'value' field of the Fee Rule,  '2': Equal to - the Fee applies only if the transaction amount is exactly the same as the amount set in the 'value' field of the Fee Rule,  '3': Not equal to - the Fee applies only if the transaction amount is not exactly equal to the amount set in the 'value' field of the Fee Rule,  '4': Greater than - the Fee applies only if the transaction amount is higher than the amount set in the 'value' field of the Fee Rule and  '5': Swiped - the Fee applies based on a determination of whether the cardholder was present during the transaction.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// The value to compare against when evaluating this Fee Rule.  When the 'type' field is set to one of the comparison operators ('1 - Less than', '2 - Equal to', '3 - Not equal to', or '4 - Greater than'), this field represents the comparator value in cents.  When the 'type' field is set to '5' (Swiped), this field represents the cardholder presence state to check the Transaction against. A value of '1' means that the card was swiped and the cardholder was present, while a value of '0' means that the card was not swiped and the cardholder was not present.
    /// </summary>
    /// <value>The value to compare against when evaluating this Fee Rule.  When the 'type' field is set to one of the comparison operators ('1 - Less than', '2 - Equal to', '3 - Not equal to', or '4 - Greater than'), this field represents the comparator value in cents.  When the 'type' field is set to '5' (Swiped), this field represents the cardholder presence state to check the Transaction against. A value of '1' means that the card was swiped and the cardholder was present, while a value of '0' means that the card was not swiped and the cardholder was not present.</value>
    [DataMember(Name="value", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "value")]
    public string Value { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }

    /// <summary>
    /// A name for a group of rules to be applied in conjunction when evaluating this Fee Rule.  When grouping is used the Fee will be allowed to be processed if all rules are matched.
    /// </summary>
    /// <value>A name for a group of rules to be applied in conjunction when evaluating this Fee Rule.  When grouping is used the Fee will be allowed to be processed if all rules are matched.</value>
    [DataMember(Name="grouping", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "grouping")]
    public string Grouping { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InlineResponse20026ResponseData {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Fee: ").Append(Fee).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Value: ").Append(Value).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("  Grouping: ").Append(Grouping).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
