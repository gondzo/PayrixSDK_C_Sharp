using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body87 {
    /// <summary>
    /// If the Transaction is linked to a Batch, this field specifies the identifier of the Batch.
    /// </summary>
    /// <value>If the Transaction is linked to a Batch, this field specifies the identifier of the Batch.</value>
    [DataMember(Name="batch", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "batch")]
    public string Batch { get; set; }

    /// <summary>
    /// The expiration date of this Transaction.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.  The value must reflect a future date.
    /// </summary>
    /// <value>The expiration date of this Transaction.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.  The value must reflect a future date.</value>
    [DataMember(Name="expiration", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "expiration")]
    public string Expiration { get; set; }

    /// <summary>
    /// The date on which the Transaction was authorized.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the past.
    /// </summary>
    /// <value>The date on which the Transaction was authorized.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the past.</value>
    [DataMember(Name="authDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "authDate")]
    public int? AuthDate { get; set; }

    /// <summary>
    /// The authorization code for this Transaction.  This field is stored as a text string and must be between 0 and 20 characters long.
    /// </summary>
    /// <value>The authorization code for this Transaction.  This field is stored as a text string and must be between 0 and 20 characters long.</value>
    [DataMember(Name="authCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "authCode")]
    public string AuthCode { get; set; }

    /// <summary>
    /// A date indicating when this Transaction was settled.  This field is set automatically.
    /// </summary>
    /// <value>A date indicating when this Transaction was settled.  This field is set automatically.</value>
    [DataMember(Name="settled", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "settled")]
    public int? Settled { get; set; }

    /// <summary>
    /// The currency of the settled total.  This field is set automatically.
    /// </summary>
    /// <value>The currency of the settled total.  This field is set automatically.</value>
    [DataMember(Name="settledCurrency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "settledCurrency")]
    public string SettledCurrency { get; set; }

    /// <summary>
    /// The total amount that was settled.   This field is specified as an integer in cents and is set automatically.
    /// </summary>
    /// <value>The total amount that was settled.   This field is specified as an integer in cents and is set automatically.</value>
    [DataMember(Name="settledTotal", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "settledTotal")]
    public int? SettledTotal { get; set; }

    /// <summary>
    /// Whether to allow partial amount authorizations of this Transaction.  For example, if the transaction amount is $1000 and the processor only authorizes a smaller amount, then enabling this field  lets the Transaction proceed anyway.  A value of '1' means that partial amount authorizations are allowed and a value of '0' means that partial amount authorizations are not allowed.
    /// </summary>
    /// <value>Whether to allow partial amount authorizations of this Transaction.  For example, if the transaction amount is $1000 and the processor only authorizes a smaller amount, then enabling this field  lets the Transaction proceed anyway.  A value of '1' means that partial amount authorizations are allowed and a value of '0' means that partial amount authorizations are not allowed.</value>
    [DataMember(Name="allowPartial", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "allowPartial")]
    public int? AllowPartial { get; set; }

    /// <summary>
    /// The client ip address from which the Transaction was created.  Valid values are any Ipv4 or Ipv6 address.
    /// </summary>
    /// <value>The client ip address from which the Transaction was created.  Valid values are any Ipv4 or Ipv6 address.</value>
    [DataMember(Name="clientIp", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "clientIp")]
    public string ClientIp { get; set; }

    /// <summary>
    /// Indicates whether the Transaction is reserved and the action that will be taken as a result.  This field is specified as an integer.  Valid values are:  '0': Not reserved  '1': If the Transaction is a sale or authorization, then block the capture of the Transaction.  '2': Apply a manual override to any checks on the Transaction and allow it to proceed and  '3': Move all funds from this Transaction into a reserve.
    /// </summary>
    /// <value>Indicates whether the Transaction is reserved and the action that will be taken as a result.  This field is specified as an integer.  Valid values are:  '0': Not reserved  '1': If the Transaction is a sale or authorization, then block the capture of the Transaction.  '2': Apply a manual override to any checks on the Transaction and allow it to proceed and  '3': Move all funds from this Transaction into a reserve.</value>
    [DataMember(Name="reserved", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reserved")]
    public int? Reserved { get; set; }

    /// <summary>
    /// The last transaction stage check for risk.
    /// </summary>
    /// <value>The last transaction stage check for risk.</value>
    [DataMember(Name="checkStage", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "checkStage")]
    public string CheckStage { get; set; }

    /// <summary>
    /// The status of the Transaction. Valid values are '0' (pending), '1' (approved), '2' (failed), '3' (captured), '4' (settled) and '5' (returned).
    /// </summary>
    /// <value>The status of the Transaction. Valid values are '0' (pending), '1' (approved), '2' (failed), '3' (captured), '4' (settled) and '5' (returned).</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body87 {\n");
      sb.Append("  Batch: ").Append(Batch).Append("\n");
      sb.Append("  Expiration: ").Append(Expiration).Append("\n");
      sb.Append("  AuthDate: ").Append(AuthDate).Append("\n");
      sb.Append("  AuthCode: ").Append(AuthCode).Append("\n");
      sb.Append("  Settled: ").Append(Settled).Append("\n");
      sb.Append("  SettledCurrency: ").Append(SettledCurrency).Append("\n");
      sb.Append("  SettledTotal: ").Append(SettledTotal).Append("\n");
      sb.Append("  AllowPartial: ").Append(AllowPartial).Append("\n");
      sb.Append("  ClientIp: ").Append(ClientIp).Append("\n");
      sb.Append("  Reserved: ").Append(Reserved).Append("\n");
      sb.Append("  CheckStage: ").Append(CheckStage).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
