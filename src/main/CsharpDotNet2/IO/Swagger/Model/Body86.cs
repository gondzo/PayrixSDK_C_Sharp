using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body86 {
    /// <summary>
    /// The identifier of the Login that owns this txnHolds resource.
    /// </summary>
    /// <value>The identifier of the Login that owns this txnHolds resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the Txn that is being held with this txnHold.
    /// </summary>
    /// <value>The identifier of the Txn that is being held with this txnHold.</value>
    [DataMember(Name="txn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txn")]
    public string Txn { get; set; }

    /// <summary>
    /// If this txnHold resource was triggered through a txnVerification, then this field stores the identifier of the TxnVerification.
    /// </summary>
    /// <value>If this txnHold resource was triggered through a txnVerification, then this field stores the identifier of the TxnVerification.</value>
    [DataMember(Name="txnVerification", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txnVerification")]
    public string TxnVerification { get; set; }

    /// <summary>
    /// The action taken on the referenced Txn.  This field is specified as an integer.  Valid values are:  '1': Block. Block the Transaction from proceeding. This returns an error.  '2': Reserved for future use.  '3': Hold. Hold the Transaction. It will not be captured until it is manually released.  '4': Reserve. Reserve the Transaction. The funds for the transaction will not be released until the Transaction is manually reviewed.
    /// </summary>
    /// <value>The action taken on the referenced Txn.  This field is specified as an integer.  Valid values are:  '1': Block. Block the Transaction from proceeding. This returns an error.  '2': Reserved for future use.  '3': Hold. Hold the Transaction. It will not be captured until it is manually released.  '4': Reserve. Reserve the Transaction. The funds for the transaction will not be released until the Transaction is manually reviewed.</value>
    [DataMember(Name="action", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "action")]
    public int? Action { get; set; }

    /// <summary>
    /// If this txnHold was released, this will contain the timestamp for when it was released.
    /// </summary>
    /// <value>If this txnHold was released, this will contain the timestamp for when it was released.</value>
    [DataMember(Name="released", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "released")]
    public DateTime? Released { get; set; }

    /// <summary>
    /// If this txnHold was reviewed, this will contain the timestamp for when it was reviewed.
    /// </summary>
    /// <value>If this txnHold was reviewed, this will contain the timestamp for when it was reviewed.</value>
    [DataMember(Name="reviewed", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reviewed")]
    public DateTime? Reviewed { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body86 {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Txn: ").Append(Txn).Append("\n");
      sb.Append("  TxnVerification: ").Append(TxnVerification).Append("\n");
      sb.Append("  Action: ").Append(Action).Append("\n");
      sb.Append("  Released: ").Append(Released).Append("\n");
      sb.Append("  Reviewed: ").Append(Reviewed).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
