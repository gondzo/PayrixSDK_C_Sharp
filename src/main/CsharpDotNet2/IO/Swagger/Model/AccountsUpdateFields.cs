using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class AccountsUpdateFields {
    /// <summary>
    /// Gets or Sets Account
    /// </summary>
    [DataMember(Name="account", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "account")]
    public AccountsidAccount Account { get; set; }

    /// <summary>
    /// A client-supplied name for this bank account.
    /// </summary>
    /// <value>A client-supplied name for this bank account.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A client-supplied description for this bank account.
    /// </summary>
    /// <value>A client-supplied description for this bank account.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// Indicates whether the Account is the 'primary' Account for the associated Entity.  Only one Account associated with each Entity can be the 'primary' Account.  A value of '1' means the Account is the primary and a value of '0' means the Account is not the primary.
    /// </summary>
    /// <value>Indicates whether the Account is the 'primary' Account for the associated Entity.  Only one Account associated with each Entity can be the 'primary' Account.  A value of '1' means the Account is the primary and a value of '0' means the Account is not the primary.</value>
    [DataMember(Name="primary", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "primary")]
    public int? Primary { get; set; }

    /// <summary>
    /// The status of the Account.  Valid values are:  '0': Not Ready. The account holder is not yet ready to verify the Account.  '1': Ready. The account is ready to be verified.  '2': Challenged - the account has processed the challenge.  '3': Verified. The Account has been verified.  '4': Manual. There has been an issue during verification and further attempts to verify the Account will require manual intervention.
    /// </summary>
    /// <value>The status of the Account.  Valid values are:  '0': Not Ready. The account holder is not yet ready to verify the Account.  '1': Ready. The account is ready to be verified.  '2': Challenged - the account has processed the challenge.  '3': Verified. The Account has been verified.  '4': Manual. There has been an issue during verification and further attempts to verify the Account will require manual intervention.</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccountsUpdateFields {\n");
      sb.Append("  Account: ").Append(Account).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Primary: ").Append(Primary).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
