using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class OrgEntitiesUpdateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Org that this orgEntity is associated with.
    /// </summary>
    /// <value>The identifier of the Org that this orgEntity is associated with.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// The identifier of the Entity that this orgEntity is associated with.
    /// </summary>
    /// <value>The identifier of the Entity that this orgEntity is associated with.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class OrgEntitiesUpdateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
