using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ChargebackMessagesCreateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Chargeback resource that this chargebackMessage relates to.
    /// </summary>
    /// <value>The identifier of the Chargeback resource that this chargebackMessage relates to.</value>
    [DataMember(Name="chargeback", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "chargeback")]
    public string Chargeback { get; set; }

    /// <summary>
    /// The date of this chargebackMessage.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.
    /// </summary>
    /// <value>The date of this chargebackMessage.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.</value>
    [DataMember(Name="date", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "date")]
    public int? Date { get; set; }

    /// <summary>
    /// The type of this chargebackMessage.   Valid values are:  '1': Assign. Request to assign the Chargeback to another party.  '2': Notate.  '3': Accept liability. The Merchant accepts liability for this Chargeback.  '4': Represent. The Merchant wishes to dispute the Chargeback and request a representment.  '5': Respond. The Merchant requests a response from the other party.  '6': Request Arbitration. The Merchant wishes to enter arbitration to determine the outcome of the Chargeback
    /// </summary>
    /// <value>The type of this chargebackMessage.   Valid values are:  '1': Assign. Request to assign the Chargeback to another party.  '2': Notate.  '3': Accept liability. The Merchant accepts liability for this Chargeback.  '4': Represent. The Merchant wishes to dispute the Chargeback and request a representment.  '5': Respond. The Merchant requests a response from the other party.  '6': Request Arbitration. The Merchant wishes to enter arbitration to determine the outcome of the Chargeback</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// Gets or Sets FromQueue
    /// </summary>
    [DataMember(Name="fromQueue", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fromQueue")]
    public string FromQueue { get; set; }

    /// <summary>
    /// Gets or Sets ToQueue
    /// </summary>
    [DataMember(Name="toQueue", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "toQueue")]
    public string ToQueue { get; set; }

    /// <summary>
    /// The identifier of the Contact for this chargebackMessage.
    /// </summary>
    /// <value>The identifier of the Contact for this chargebackMessage.</value>
    [DataMember(Name="contact", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "contact")]
    public string Contact { get; set; }

    /// <summary>
    /// The amount that this chargebackMessage corresponds to.  For example, if the 'type' is set to '3' (Accept Liability), then this amount indicates that the liability should be for this amount.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount that this chargebackMessage corresponds to.  For example, if the 'type' is set to '3' (Accept Liability), then this amount indicates that the liability should be for this amount.  This field is specified as an integer in cents.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public int? Amount { get; set; }

    /// <summary>
    /// The currency of the amount in this chargebackMessage.  Currently, this field only accepts the value 'USD'.
    /// </summary>
    /// <value>The currency of the amount in this chargebackMessage.  Currently, this field only accepts the value 'USD'.</value>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// A free-text note relating to this chargebackMessage.
    /// </summary>
    /// <value>A free-text note relating to this chargebackMessage.</value>
    [DataMember(Name="note", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "note")]
    public string Note { get; set; }

    /// <summary>
    /// The current status of the Chargeback.  Valid values are:  '1': Requested. The Chargeback has been requested from the processor.  '2': Processing. The Chargeback is being processed by the card processor.  '3': Failed. The Chargeback has failed because of a technical problem.  '4': Denied. The issuer has denied the Chargeback.  '5': Processed. The Chargeback has been accepted and processed.
    /// </summary>
    /// <value>The current status of the Chargeback.  Valid values are:  '1': Requested. The Chargeback has been requested from the processor.  '2': Processing. The Chargeback is being processed by the card processor.  '3': Failed. The Chargeback has failed because of a technical problem.  '4': Denied. The issuer has denied the Chargeback.  '5': Processed. The Chargeback has been accepted and processed.</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }

    /// <summary>
    /// Gets or Sets Imported
    /// </summary>
    [DataMember(Name="imported", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "imported")]
    public int? Imported { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ChargebackMessagesCreateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Chargeback: ").Append(Chargeback).Append("\n");
      sb.Append("  Date: ").Append(Date).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  FromQueue: ").Append(FromQueue).Append("\n");
      sb.Append("  ToQueue: ").Append(ToQueue).Append("\n");
      sb.Append("  Contact: ").Append(Contact).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  Note: ").Append(Note).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("  Imported: ").Append(Imported).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
