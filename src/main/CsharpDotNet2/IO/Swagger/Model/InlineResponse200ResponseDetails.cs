using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// An object describing the response, including the request ID and pagination indicators.
  /// </summary>
  [DataContract]
  public class InlineResponse200ResponseDetails {
    /// <summary>
    /// A unique identifier set by the API client, that is echoed by the response.  The request ID can be useful in troubleshooting and monitoring.
    /// </summary>
    /// <value>A unique identifier set by the API client, that is echoed by the response.  The request ID can be useful in troubleshooting and monitoring.</value>
    [DataMember(Name="requestId", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "requestId")]
    public string RequestId { get; set; }

    /// <summary>
    /// Gets or Sets Page
    /// </summary>
    [DataMember(Name="page", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "page")]
    public InlineResponse200ResponseDetailsPage Page { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InlineResponse200ResponseDetails {\n");
      sb.Append("  RequestId: ").Append(RequestId).Append("\n");
      sb.Append("  Page: ").Append(Page).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
