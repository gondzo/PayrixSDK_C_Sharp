using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class TxnResultsQueryResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Transaction associated with this txnResults resource.
    /// </summary>
    /// <value>The identifier of the Transaction associated with this txnResults resource.</value>
    [DataMember(Name="txn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txn")]
    public string Txn { get; set; }

    /// <summary>
    /// The type of this txnResult.  This field is specified as an integer.  Valid values are:  '1': A general type of result.  '2': Fraud prevention alert  '3': Processor error.
    /// </summary>
    /// <value>The type of this txnResult.  This field is specified as an integer.  Valid values are:  '1': A general type of result.  '2': Fraud prevention alert  '3': Processor error.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// A message that accompanies and describes this Transaction result.
    /// </summary>
    /// <value>A message that accompanies and describes this Transaction result.</value>
    [DataMember(Name="message", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "message")]
    public string Message { get; set; }

    /// <summary>
    /// The result code that is associated with this txnResult.  This field is specified as an integer.  Valid values are:  '0': Transaction approved.  '1': Partially approved. The processor has only approved a portion of the total transaction amount.  '2': Declined. The processor has declined the Transaction.  '3': The supplied CVV code matches.  '4': The supplied CVV code does not match.  '5': The ZIP code in the Transaction data does not match the customer details held by the card issuer.  '6': The address in the Transaction data does not match the customer details held by the card issuer.  '7': The name in the Transaction data does not match the customer details held by the card issuer.  '8': The name and phone number in the Transaction data do not match the details held by the card issuer.  '9': The name and email address in the Transaction data do not match the customer details held by the card issuer.  '10': The phone number in the Transaction data does not match the customer details held by the card issuer.  '11': The phone number and email address in the Transaction data do not match the customer details held by the card issuer.  '12': The email address in the Transaction data does not match the customer details held by the card issuer.  '13': The customer name could not be found in the Transaction data.  '14': The customer name and phone number were not found in the Transaction data.  '15': The customer name and email address were not found in the Transaction data.  '16': The customer phone number was not found in the Transaction data.  '17': The customer phone number and email address were not found in the Transaction data.  '18': The customer email address was not found in the Transaction data.  '19': Information about the customer was not found in the Transaction data.  '20': Non-sufficient funds. The customer did not have sufficient credit or balance to cover the Transaction.  '21': The account in the Transaction data is not valid.
    /// </summary>
    /// <value>The result code that is associated with this txnResult.  This field is specified as an integer.  Valid values are:  '0': Transaction approved.  '1': Partially approved. The processor has only approved a portion of the total transaction amount.  '2': Declined. The processor has declined the Transaction.  '3': The supplied CVV code matches.  '4': The supplied CVV code does not match.  '5': The ZIP code in the Transaction data does not match the customer details held by the card issuer.  '6': The address in the Transaction data does not match the customer details held by the card issuer.  '7': The name in the Transaction data does not match the customer details held by the card issuer.  '8': The name and phone number in the Transaction data do not match the details held by the card issuer.  '9': The name and email address in the Transaction data do not match the customer details held by the card issuer.  '10': The phone number in the Transaction data does not match the customer details held by the card issuer.  '11': The phone number and email address in the Transaction data do not match the customer details held by the card issuer.  '12': The email address in the Transaction data does not match the customer details held by the card issuer.  '13': The customer name could not be found in the Transaction data.  '14': The customer name and phone number were not found in the Transaction data.  '15': The customer name and email address were not found in the Transaction data.  '16': The customer phone number was not found in the Transaction data.  '17': The customer phone number and email address were not found in the Transaction data.  '18': The customer email address was not found in the Transaction data.  '19': Information about the customer was not found in the Transaction data.  '20': Non-sufficient funds. The customer did not have sufficient credit or balance to cover the Transaction.  '21': The account in the Transaction data is not valid.</value>
    [DataMember(Name="code", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "code")]
    public int? Code { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TxnResultsQueryResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Txn: ").Append(Txn).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Message: ").Append(Message).Append("\n");
      sb.Append("  Code: ").Append(Code).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
