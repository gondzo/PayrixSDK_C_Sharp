using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body65 {
    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the Entity that this Payout is associated with.
    /// </summary>
    /// <value>The identifier of the Entity that this Payout is associated with.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The identifier of the Account that this Payout is associated with.  This account will either receive the funds or be debited for the funds every time a Disbursement occurs, depending on the direction of the Disbursement.
    /// </summary>
    /// <value>The identifier of the Account that this Payout is associated with.  This account will either receive the funds or be debited for the funds every time a Disbursement occurs, depending on the direction of the Disbursement.</value>
    [DataMember(Name="account", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "account")]
    public string Account { get; set; }

    /// <summary>
    /// The identifier of the PayoutFlow associated with this Payout.
    /// </summary>
    /// <value>The identifier of the PayoutFlow associated with this Payout.</value>
    [DataMember(Name="payoutFlow", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "payoutFlow")]
    public string PayoutFlow { get; set; }

    /// <summary>
    /// The name of this Payout.  This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>The name of this Payout.  This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A description of this Payout.   This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>A description of this Payout.   This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The schedule that determines when this Payout is triggered to be paid.  Valid values are:  '1': Daily - the Payout is paid every day.  '2': Weekly - the Payout is paid every week.  '3': Monthly - the Payout is paid every month.  '4': Annually - the Payout is paid every year.  '5': Single - the Payout is a one-off payment.
    /// </summary>
    /// <value>The schedule that determines when this Payout is triggered to be paid.  Valid values are:  '1': Daily - the Payout is paid every day.  '2': Weekly - the Payout is paid every week.  '3': Monthly - the Payout is paid every month.  '4': Annually - the Payout is paid every year.  '5': Single - the Payout is a one-off payment.</value>
    [DataMember(Name="schedule", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "schedule")]
    public int? Schedule { get; set; }

    /// <summary>
    /// A multiplier that you can use to adjust the schedule set in the 'schedule' field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if 'schedule' is set to '1' (meaning 'daily'), then a 'scheduleFactor' value of '2' would cause the Payout to trigger every two days.
    /// </summary>
    /// <value>A multiplier that you can use to adjust the schedule set in the 'schedule' field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if 'schedule' is set to '1' (meaning 'daily'), then a 'scheduleFactor' value of '2' would cause the Payout to trigger every two days.</value>
    [DataMember(Name="scheduleFactor", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "scheduleFactor")]
    public int? ScheduleFactor { get; set; }

    /// <summary>
    /// The date on which payment of the Payout should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future, or the present date.
    /// </summary>
    /// <value>The date on which payment of the Payout should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future, or the present date.</value>
    [DataMember(Name="start", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "start")]
    public int? Start { get; set; }

    /// <summary>
    /// The currency of the amount in this Payout.  This field is only required when Um is set to ACTUAL. If this field is not set we will process disbursements for all currencies.  Currently, this field only accepts the value 'USD'.
    /// </summary>
    /// <value>The currency of the amount in this Payout.  This field is only required when Um is set to ACTUAL. If this field is not set we will process disbursements for all currencies.  Currently, this field only accepts the value 'USD'.</value>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// The unit of measure for this Payout.  Valid values are:  '1': Percentage - the Payout is a percentage of the current available funds for this Entity that should be paid to their Account, specified in the 'amount' field in basis points. '2': Actual - the Payout is a fixed amount, specified in the 'amount' field as an integer in cents.  '3': Negative percentage - the Payout is a percentage of the balance, specified in the 'amount' field as a negative integer in basis points. The direction of the Payout payment is reversed. For example, if the Entity has a negative balance of $10 and the amount is set to 10000 (100%), then $10 will be drawn from their account to fully replenish the balance to $0.
    /// </summary>
    /// <value>The unit of measure for this Payout.  Valid values are:  '1': Percentage - the Payout is a percentage of the current available funds for this Entity that should be paid to their Account, specified in the 'amount' field in basis points. '2': Actual - the Payout is a fixed amount, specified in the 'amount' field as an integer in cents.  '3': Negative percentage - the Payout is a percentage of the balance, specified in the 'amount' field as a negative integer in basis points. The direction of the Payout payment is reversed. For example, if the Entity has a negative balance of $10 and the amount is set to 10000 (100%), then $10 will be drawn from their account to fully replenish the balance to $0.</value>
    [DataMember(Name="um", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "um")]
    public int? Um { get; set; }

    /// <summary>
    /// The total amount of the Payout resource that is created.  This field is specified as an integer.  The units used in this field are determined by the value of the 'um' field on the Payout. If the 'um' field is set to '1' or '3', then this field specifies the Payout percentage to levy in basis points. If the 'um' field is set to '2', then this field specifies the Payout in cents.
    /// </summary>
    /// <value>The total amount of the Payout resource that is created.  This field is specified as an integer.  The units used in this field are determined by the value of the 'um' field on the Payout. If the 'um' field is set to '1' or '3', then this field specifies the Payout percentage to levy in basis points. If the 'um' field is set to '2', then this field specifies the Payout in cents.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public int? Amount { get; set; }

    /// <summary>
    /// Gets or Sets Minimum
    /// </summary>
    [DataMember(Name="minimum", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "minimum")]
    public int? Minimum { get; set; }

    /// <summary>
    /// An optional field indicating the minimum balance you want to maintain, despite any Payouts occurring. If the Payout would reduce the balance to below this value, then it is not processed.  This field is specified as an integer in cents.  For example, a float value of 1000 would ensure that a balance of 10 USD is maintained at all times.
    /// </summary>
    /// <value>An optional field indicating the minimum balance you want to maintain, despite any Payouts occurring. If the Payout would reduce the balance to below this value, then it is not processed.  This field is specified as an integer in cents.  For example, a float value of 1000 would ensure that a balance of 10 USD is maintained at all times.</value>
    [DataMember(Name="float", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "float")]
    public int? _Float { get; set; }

    /// <summary>
    /// Whether to skip the creation of disbursements on holidays and weekends. A value of '1' means skip and a value of '0' means do not skip.
    /// </summary>
    /// <value>Whether to skip the creation of disbursements on holidays and weekends. A value of '1' means skip and a value of '0' means do not skip.</value>
    [DataMember(Name="skipOffDays", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "skipOffDays")]
    public int? SkipOffDays { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body65 {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Account: ").Append(Account).Append("\n");
      sb.Append("  PayoutFlow: ").Append(PayoutFlow).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Schedule: ").Append(Schedule).Append("\n");
      sb.Append("  ScheduleFactor: ").Append(ScheduleFactor).Append("\n");
      sb.Append("  Start: ").Append(Start).Append("\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  Um: ").Append(Um).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("  Minimum: ").Append(Minimum).Append("\n");
      sb.Append("  _Float: ").Append(_Float).Append("\n");
      sb.Append("  SkipOffDays: ").Append(SkipOffDays).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
