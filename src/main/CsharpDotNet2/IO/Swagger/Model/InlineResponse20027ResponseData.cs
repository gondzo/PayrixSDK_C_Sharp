using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InlineResponse20027ResponseData {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Entity that will charge this Fee.
    /// </summary>
    /// <value>The identifier of the Entity that will charge this Fee.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The identifier of the Entity that this Fee applies for.
    /// </summary>
    /// <value>The identifier of the Entity that this Fee applies for.</value>
    [DataMember(Name="forentity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "forentity")]
    public string Forentity { get; set; }

    /// <summary>
    /// The identifier of the Org who should pay this Fee on behalf of the Entity identified in the value of the 'forentity' field.  This field is optional. If it is set, then the Fee is charged to this Org instead.
    /// </summary>
    /// <value>The identifier of the Org who should pay this Fee on behalf of the Entity identified in the value of the 'forentity' field.  This field is optional. If it is set, then the Fee is charged to this Org instead.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// The type of the fee.   Valid values are:  '1': FEE - A regular Fee to be charged.  '2': ASSESSMENT - A fee chaged by a third-party platform.
    /// </summary>
    /// <value>The type of the fee.   Valid values are:  '1': FEE - A regular Fee to be charged.  '2': ASSESSMENT - A fee chaged by a third-party platform.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// The name of this Fee.  This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>The name of this Fee.  This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A description of this Fee.   This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>A description of this Fee.   This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The schedule that determines when this Fee is triggered to be charged.  Valid values are:  '1': Daily - the Fee is charged every day.  '2': Weekly - the Fee is charged every week.  '3': Monthly - the Fee is charged every month.  '4': Annually - the Fee is charged every year.  '5': Single - the Fee is a one-off charge.  '6': Auth - the Fee is triggered at the time of authorization of a transaction.  '7': Capture - the Fee triggers at the capture time of a Transaction.  '8': Refund - the Fee triggers when a refund transaction is processed.  '9': Board - the Fee triggers when the Merchant is boarded.  '10': Payout - the Fee triggers when a payout is processed.  '11': Chargeback - the Fee triggers when a card chargeback occurs.  '12': Overdraft - the Fee triggers when an overdraft usage charge from a bank is levied.  '13': Interchange - the Fee triggers when interchange Fees are assessed for the Transactions of this Merchant.  '14': Processor - the Fee triggers when the Transactions of this Merchant are processed by a payment processor.  '15': ACH failure - the Fee triggers when an automated clearing house failure occurs.  '16': Account - the Fee triggers when a bank account is verified.
    /// </summary>
    /// <value>The schedule that determines when this Fee is triggered to be charged.  Valid values are:  '1': Daily - the Fee is charged every day.  '2': Weekly - the Fee is charged every week.  '3': Monthly - the Fee is charged every month.  '4': Annually - the Fee is charged every year.  '5': Single - the Fee is a one-off charge.  '6': Auth - the Fee is triggered at the time of authorization of a transaction.  '7': Capture - the Fee triggers at the capture time of a Transaction.  '8': Refund - the Fee triggers when a refund transaction is processed.  '9': Board - the Fee triggers when the Merchant is boarded.  '10': Payout - the Fee triggers when a payout is processed.  '11': Chargeback - the Fee triggers when a card chargeback occurs.  '12': Overdraft - the Fee triggers when an overdraft usage charge from a bank is levied.  '13': Interchange - the Fee triggers when interchange Fees are assessed for the Transactions of this Merchant.  '14': Processor - the Fee triggers when the Transactions of this Merchant are processed by a payment processor.  '15': ACH failure - the Fee triggers when an automated clearing house failure occurs.  '16': Account - the Fee triggers when a bank account is verified.</value>
    [DataMember(Name="schedule", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "schedule")]
    public int? Schedule { get; set; }

    /// <summary>
    /// A multiplier that you can use to adjust the schedule set in the 'schedule' field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if 'schedule' is set to '1' (meaning 'daily'), then a 'scheduleFactor' value of '2' would cause the Fee to trigger every two days.
    /// </summary>
    /// <value>A multiplier that you can use to adjust the schedule set in the 'schedule' field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if 'schedule' is set to '1' (meaning 'daily'), then a 'scheduleFactor' value of '2' would cause the Fee to trigger every two days.</value>
    [DataMember(Name="scheduleFactor", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "scheduleFactor")]
    public int? ScheduleFactor { get; set; }

    /// <summary>
    /// The date on which charging of the Fee should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future, or the present date.
    /// </summary>
    /// <value>The date on which charging of the Fee should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future, or the present date.</value>
    [DataMember(Name="start", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "start")]
    public int? Start { get; set; }

    /// <summary>
    /// The date on which charging of the Fee should end.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future.
    /// </summary>
    /// <value>The date on which charging of the Fee should end.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the future.</value>
    [DataMember(Name="finish", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "finish")]
    public int? Finish { get; set; }

    /// <summary>
    /// The unit of measure for this Fee.  Valid values are:  '2': The Fee is a fixed amount, specified in the 'amount' field as an integer in cents.  '1': The Fee is a percentage of the transaction amount, specified in the 'amount' field in basis points. Note that percentage measures only make sense where the Fee schedule is set to trigger the Fee when a transaction event happens, such as an authorization, capture, or refund.
    /// </summary>
    /// <value>The unit of measure for this Fee.  Valid values are:  '2': The Fee is a fixed amount, specified in the 'amount' field as an integer in cents.  '1': The Fee is a percentage of the transaction amount, specified in the 'amount' field in basis points. Note that percentage measures only make sense where the Fee schedule is set to trigger the Fee when a transaction event happens, such as an authorization, capture, or refund.</value>
    [DataMember(Name="um", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "um")]
    public int? Um { get; set; }

    /// <summary>
    /// The total amount of this Fee.  This field is specified as an integer.  The units used in this field are determined by the value of the 'um' field on the Fee. If the 'um' field is set to 'percentage', then this field specifies the Fee percentage to levy in basis points. If the 'um' field is set to 'amount', then this field specifies the Fee in cents.
    /// </summary>
    /// <value>The total amount of this Fee.  This field is specified as an integer.  The units used in this field are determined by the value of the 'um' field on the Fee. If the 'um' field is set to 'percentage', then this field specifies the Fee percentage to levy in basis points. If the 'um' field is set to 'amount', then this field specifies the Fee in cents.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public string Amount { get; set; }

    /// <summary>
    /// The currency of the amount.  This field is only required when Um is set to 'ACTUAL'.  Currently, this field only accepts the value 'USD'.
    /// </summary>
    /// <value>The currency of the amount.  This field is only required when Um is set to 'ACTUAL'.  Currently, this field only accepts the value 'USD'.</value>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InlineResponse20027ResponseData {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Forentity: ").Append(Forentity).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Schedule: ").Append(Schedule).Append("\n");
      sb.Append("  ScheduleFactor: ").Append(ScheduleFactor).Append("\n");
      sb.Append("  Start: ").Append(Start).Append("\n");
      sb.Append("  Finish: ").Append(Finish).Append("\n");
      sb.Append("  Um: ").Append(Um).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
