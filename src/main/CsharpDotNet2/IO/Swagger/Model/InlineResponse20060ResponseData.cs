using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InlineResponse20060ResponseData {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Login that owns this txnChecks resource.
    /// </summary>
    /// <value>The identifier of the Login that owns this txnChecks resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// If this txnCheck resource relates to an Org, then this field stores the identifier of the Org.
    /// </summary>
    /// <value>If this txnCheck resource relates to an Org, then this field stores the identifier of the Org.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// If this txnCheck resource relates to an Entity, then this field stores the identifier of the Entity.
    /// </summary>
    /// <value>If this txnCheck resource relates to an Entity, then this field stores the identifier of the Entity.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The identifier of a linked txnCheck resource that acts in coordination with this txnCheck. When you define a txnCheck attribute here, the referenced txnCheck becomes the 'parent' resource. The action field set on the 'parent' resource determines the action that occurs if all of the linked txnChecks fail.
    /// </summary>
    /// <value>The identifier of a linked txnCheck resource that acts in coordination with this txnCheck. When you define a txnCheck attribute here, the referenced txnCheck becomes the 'parent' resource. The action field set on the 'parent' resource determines the action that occurs if all of the linked txnChecks fail.</value>
    [DataMember(Name="txnCheck", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txnCheck")]
    public string TxnCheck { get; set; }

    /// <summary>
    /// The type of check to perform.  This field is specified as an integer.  Valid values are: n'1': Exceeded the maximum number of allowed failed authorizations.  '2': Exceeded the maximum allowed sale total value.  '3': Exceeded the maximum allowed refund total value.  '4': Exceeded the allowed maximum payment size (individual transaction amount).  '5': Exceeded the maximum allowed ratio of refunds to sales.  '6': Exceeded the maximum allowed number of successful authorizations.  '7': Exceeded the maximum allowed number of failed authorizations for a particular IP address.  '8': Exceeded the maximum allowed ratio of failed authorizations for a particular IP address.  '9': The Merchant is not active.  '10': Exceeded the maximum allowed number of failed transactions.  '11': Exceeded the maximum allowed number of transactions without authorizations.  '12': Refund transaction does not have an associated sale transaction.  '13': Exceeded the maximum number of refund transactions that do not have associated sale Transactions.  '14': Exceeded the maximum authorized value for Transactions with failed authorizations.
    /// </summary>
    /// <value>The type of check to perform.  This field is specified as an integer.  Valid values are: n'1': Exceeded the maximum number of allowed failed authorizations.  '2': Exceeded the maximum allowed sale total value.  '3': Exceeded the maximum allowed refund total value.  '4': Exceeded the allowed maximum payment size (individual transaction amount).  '5': Exceeded the maximum allowed ratio of refunds to sales.  '6': Exceeded the maximum allowed number of successful authorizations.  '7': Exceeded the maximum allowed number of failed authorizations for a particular IP address.  '8': Exceeded the maximum allowed ratio of failed authorizations for a particular IP address.  '9': The Merchant is not active.  '10': Exceeded the maximum allowed number of failed transactions.  '11': Exceeded the maximum allowed number of transactions without authorizations.  '12': Refund transaction does not have an associated sale transaction.  '13': Exceeded the maximum number of refund transactions that do not have associated sale Transactions.  '14': Exceeded the maximum authorized value for Transactions with failed authorizations.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// The transaction stage that this txnCheck should apply at.   Valid values are:  'AUTH': Apply this txnCheck during transaction authorization.  'CAPTURE': Apply this txnCheck during transaction capture.  'REFUND': Apply this txnCheck when processing a refund.
    /// </summary>
    /// <value>The transaction stage that this txnCheck should apply at.   Valid values are:  'AUTH': Apply this txnCheck during transaction authorization.  'CAPTURE': Apply this txnCheck during transaction capture.  'REFUND': Apply this txnCheck when processing a refund.</value>
    [DataMember(Name="stage", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "stage")]
    public string Stage { get; set; }

    /// <summary>
    /// The action to take when this check fails.  This field is specified as an integer.  Valid values are:  '1': Block. Block the Transaction from proceeding. This returns an error.  '2': No capture. Prevent the Transaction from being captured.  '3': Hold. Hold the Transaction. It will not be captured until it is manually released.  '4': Reserve. Reserve the Transaction. The funds for the transaction will not be released until the Transaction is manually reviewed.
    /// </summary>
    /// <value>The action to take when this check fails.  This field is specified as an integer.  Valid values are:  '1': Block. Block the Transaction from proceeding. This returns an error.  '2': No capture. Prevent the Transaction from being captured.  '3': Hold. Hold the Transaction. It will not be captured until it is manually released.  '4': Reserve. Reserve the Transaction. The funds for the transaction will not be released until the Transaction is manually reviewed.</value>
    [DataMember(Name="action", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "action")]
    public int? Action { get; set; }

    /// <summary>
    /// A sequence number to use when applying multiple linked txnChecks.  When two or more txnChecks are linked using their 'txnCheck' fields, the checks with lower 'sequence' numbers are applied first.  This field is specified as an integer.
    /// </summary>
    /// <value>A sequence number to use when applying multiple linked txnChecks.  When two or more txnChecks are linked using their 'txnCheck' fields, the checks with lower 'sequence' numbers are applied first.  This field is specified as an integer.</value>
    [DataMember(Name="sequence", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "sequence")]
    public int? Sequence { get; set; }

    /// <summary>
    /// The minimum value that the Transaction must be to trigger this txnCheck.  The unit of measure is determined by the type of check.
    /// </summary>
    /// <value>The minimum value that the Transaction must be to trigger this txnCheck.  The unit of measure is determined by the type of check.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public int? Amount { get; set; }

    /// <summary>
    /// The amount that the associated Transaction should be compared against. The units for this field depend on the type of check.  For checks that are based on a ratio, specify the amount as a percentage in basis points.  For checks that are based on a dollar amount, specify the amount in cents.  For checks that are based on a count, specify the exact value as an integer.
    /// </summary>
    /// <value>The amount that the associated Transaction should be compared against. The units for this field depend on the type of check.  For checks that are based on a ratio, specify the amount as a percentage in basis points.  For checks that are based on a dollar amount, specify the amount in cents.  For checks that are based on a count, specify the exact value as an integer.</value>
    [DataMember(Name="value", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "value")]
    public int? Value { get; set; }

    /// <summary>
    /// A date indicator that determines how far back in time time-based checks should be checked. This field works in conjunction with the 'periodFactor' field, which multiplies this basic period.  For example, maximum transaction volume within the last three weeks would use a 'period' value of '2' for 'weeks' and a 'periodSchedule' of '3', to indicate three weeks.  Valid values are:  '1': Days - the basic value for the date range is set in days.  '2': Weeks - the basic value for the date range is set in weeks.  '3': Months - the basic value for the date range is set in months.  '4': Years - the basic value for the date range is set in days.
    /// </summary>
    /// <value>A date indicator that determines how far back in time time-based checks should be checked. This field works in conjunction with the 'periodFactor' field, which multiplies this basic period.  For example, maximum transaction volume within the last three weeks would use a 'period' value of '2' for 'weeks' and a 'periodSchedule' of '3', to indicate three weeks.  Valid values are:  '1': Days - the basic value for the date range is set in days.  '2': Weeks - the basic value for the date range is set in weeks.  '3': Months - the basic value for the date range is set in months.  '4': Years - the basic value for the date range is set in days.</value>
    [DataMember(Name="period", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "period")]
    public int? Period { get; set; }

    /// <summary>
    /// Gets or Sets PeriodFactor
    /// </summary>
    [DataMember(Name="periodFactor", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "periodFactor")]
    public int? PeriodFactor { get; set; }

    /// <summary>
    /// A lower cut-off value for the score in this txnCheck.  The check fails if the score is equal to or below this value.
    /// </summary>
    /// <value>A lower cut-off value for the score in this txnCheck.  The check fails if the score is equal to or below this value.</value>
    [DataMember(Name="low", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "low")]
    public int? Low { get; set; }

    /// <summary>
    /// An upper cut-off value for the score in this txnCheck.  The check fails if the score is equal to or above this value.
    /// </summary>
    /// <value>An upper cut-off value for the score in this txnCheck.  The check fails if the score is equal to or above this value.</value>
    [DataMember(Name="high", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "high")]
    public int? High { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InlineResponse20060ResponseData {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  TxnCheck: ").Append(TxnCheck).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Stage: ").Append(Stage).Append("\n");
      sb.Append("  Action: ").Append(Action).Append("\n");
      sb.Append("  Sequence: ").Append(Sequence).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("  Value: ").Append(Value).Append("\n");
      sb.Append("  Period: ").Append(Period).Append("\n");
      sb.Append("  PeriodFactor: ").Append(PeriodFactor).Append("\n");
      sb.Append("  Low: ").Append(Low).Append("\n");
      sb.Append("  High: ").Append(High).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
