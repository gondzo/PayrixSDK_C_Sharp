using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class MerchantChecksUpdateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Login that owns this merchantChecks resource.
    /// </summary>
    /// <value>The identifier of the Login that owns this merchantChecks resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// If this merchantChecks resource relates to an Org, then this field stores the identifier of the Org.
    /// </summary>
    /// <value>If this merchantChecks resource relates to an Org, then this field stores the identifier of the Org.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// If this merchantChecks resource relates to an Entity, then this field stores the identifier of the Entity.
    /// </summary>
    /// <value>If this merchantChecks resource relates to an Entity, then this field stores the identifier of the Entity.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The type of check to perform on the Merchant.   This field is specified as an integer.  Valid values are:  '1': OFAC. Check the Merchant against the Specially Designated Nationals (SDN) list operated by the Office of Foreign Assets Control at the U.S. Department of the Treasury.  '2': Check that the data held on the Entity that is associated with this Merchant matches the data retrieved from external data sources.  '3': Check that the data held on the Owner (primary Member) that is associated with this Merchant matches the data retrieved from external data sources.  '4': Check that the Member and Entity associated with this account are have a genuine association, as determined by consulting external data sources.
    /// </summary>
    /// <value>The type of check to perform on the Merchant.   This field is specified as an integer.  Valid values are:  '1': OFAC. Check the Merchant against the Specially Designated Nationals (SDN) list operated by the Office of Foreign Assets Control at the U.S. Department of the Treasury.  '2': Check that the data held on the Entity that is associated with this Merchant matches the data retrieved from external data sources.  '3': Check that the data held on the Owner (primary Member) that is associated with this Merchant matches the data retrieved from external data sources.  '4': Check that the Member and Entity associated with this account are have a genuine association, as determined by consulting external data sources.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// The event that triggers this txnCheck on the Merchant.  This field is specified as an integer.  Valid values are:  '1': Preboard. Check the Merchant before they are boarded.  '2': Check the Merchant after they are boarded.  '3': Check the Merchant when they process a Transaction.  '4': Check the Merchant when their transaction volume hits a certain amount.  '5': Check the Merchant when a Payout occurs.  '6': Check the Merchant when the volume of Payouts to the Merchant hits a certain amount.
    /// </summary>
    /// <value>The event that triggers this txnCheck on the Merchant.  This field is specified as an integer.  Valid values are:  '1': Preboard. Check the Merchant before they are boarded.  '2': Check the Merchant after they are boarded.  '3': Check the Merchant when they process a Transaction.  '4': Check the Merchant when their transaction volume hits a certain amount.  '5': Check the Merchant when a Payout occurs.  '6': Check the Merchant when the volume of Payouts to the Merchant hits a certain amount.</value>
    [DataMember(Name="trigger", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "trigger")]
    public int? Trigger { get; set; }

    /// <summary>
    /// A lower cut-off value for the score in this Merchant check.  The check fails if the score is equal to or below this value.
    /// </summary>
    /// <value>A lower cut-off value for the score in this Merchant check.  The check fails if the score is equal to or below this value.</value>
    [DataMember(Name="low", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "low")]
    public int? Low { get; set; }

    /// <summary>
    /// An upper cut-off value for the score in this Merchant check.  The check fails if the score is equal to or above this value.
    /// </summary>
    /// <value>An upper cut-off value for the score in this Merchant check.  The check fails if the score is equal to or above this value.</value>
    [DataMember(Name="high", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "high")]
    public int? High { get; set; }

    /// <summary>
    /// The minimum value that the Transaction must be to trigger this Merchant check.  The unit of measure is determined by the type of check.
    /// </summary>
    /// <value>The minimum value that the Transaction must be to trigger this Merchant check.  The unit of measure is determined by the type of check.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public int? Amount { get; set; }

    /// <summary>
    /// The action to take when the check fails.  This field is specified as an integer.  Valid values are:  '1': Manual. The Merchant is sent to the manual review queue.  '2': Mark the funds in the Merchant account as reserved.  '3': Limit the Merchant account by blocking transactions which are over the limit set in this check.
    /// </summary>
    /// <value>The action to take when the check fails.  This field is specified as an integer.  Valid values are:  '1': Manual. The Merchant is sent to the manual review queue.  '2': Mark the funds in the Merchant account as reserved.  '3': Limit the Merchant account by blocking transactions which are over the limit set in this check.</value>
    [DataMember(Name="action", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "action")]
    public int? Action { get; set; }

    /// <summary>
    /// The options for the Verification.  This field is specified as a sum of the desired options.  Valid values are:  '0': NONE. No options are set.  '1': KYC. KYC verification will be enabled.  '2': WATCHLIST_STANDARD. Primary member will be checked agains standard watchlists.  '4': WATCHLIST_PLUS. Primary member will be checked against additional watchlists.  '8': WATCHLIST_PREMIER. Primary member will be checked against uncommon lists like PEP, Associated family members, etc.
    /// </summary>
    /// <value>The options for the Verification.  This field is specified as a sum of the desired options.  Valid values are:  '0': NONE. No options are set.  '1': KYC. KYC verification will be enabled.  '2': WATCHLIST_STANDARD. Primary member will be checked agains standard watchlists.  '4': WATCHLIST_PLUS. Primary member will be checked against additional watchlists.  '8': WATCHLIST_PREMIER. Primary member will be checked against uncommon lists like PEP, Associated family members, etc.</value>
    [DataMember(Name="options", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "options")]
    public int? Options { get; set; }

    /// <summary>
    /// A sequence number to use when applying multiple linked checks.  When two or more checks are linked, the checks with lower 'sequence' numbers are applied first.  This field is specified as an integer.
    /// </summary>
    /// <value>A sequence number to use when applying multiple linked checks.  When two or more checks are linked, the checks with lower 'sequence' numbers are applied first.  This field is specified as an integer.</value>
    [DataMember(Name="sequence", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "sequence")]
    public int? Sequence { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class MerchantChecksUpdateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Trigger: ").Append(Trigger).Append("\n");
      sb.Append("  Low: ").Append(Low).Append("\n");
      sb.Append("  High: ").Append(High).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("  Action: ").Append(Action).Append("\n");
      sb.Append("  Options: ").Append(Options).Append("\n");
      sb.Append("  Sequence: ").Append(Sequence).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
