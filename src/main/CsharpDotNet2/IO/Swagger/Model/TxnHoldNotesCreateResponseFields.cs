using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class TxnHoldNotesCreateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the TxnHold that owns this txnHoldNotes resource.
    /// </summary>
    /// <value>The identifier of the TxnHold that owns this txnHoldNotes resource.</value>
    [DataMember(Name="txnHold", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txnHold")]
    public string TxnHold { get; set; }

    /// <summary>
    /// Free-form text for adding a message along with the action.
    /// </summary>
    /// <value>Free-form text for adding a message along with the action.</value>
    [DataMember(Name="note", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "note")]
    public string Note { get; set; }

    /// <summary>
    /// The desired action to take on the referenced TxnHold.  This field is specified as an integer.  Valid values are:  '0': Note. Just add a note to the txnHold. '1': Release. Release the hold for this TxnHold. '2': Hold. If the txnHold was released, this will allow resetting the hold. '3': Review. Mark the txnHold as having been reviewed.  '4': Re-Review. If the txnHold was marked as reviewed, this will allow resetting the review.
    /// </summary>
    /// <value>The desired action to take on the referenced TxnHold.  This field is specified as an integer.  Valid values are:  '0': Note. Just add a note to the txnHold. '1': Release. Release the hold for this TxnHold. '2': Hold. If the txnHold was released, this will allow resetting the hold. '3': Review. Mark the txnHold as having been reviewed.  '4': Re-Review. If the txnHold was marked as reviewed, this will allow resetting the review.</value>
    [DataMember(Name="action", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "action")]
    public int? Action { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TxnHoldNotesCreateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  TxnHold: ").Append(TxnHold).Append("\n");
      sb.Append("  Note: ").Append(Note).Append("\n");
      sb.Append("  Action: ").Append(Action).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
