using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class InlineResponse20023ResponseData {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Entity that this Entry refers to.
    /// </summary>
    /// <value>The identifier of the Entity that this Entry refers to.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// If the activity that this Entry refers to involves two parties in the system with one paying a charge of any kind, then this field stores the identifier of the Entity that the charge or other activity is for.
    /// </summary>
    /// <value>If the activity that this Entry refers to involves two parties in the system with one paying a charge of any kind, then this field stores the identifier of the Entity that the charge or other activity is for.</value>
    [DataMember(Name="fromentity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fromentity")]
    public string Fromentity { get; set; }

    /// <summary>
    /// The identifier of the Fund that this Entry refers to.
    /// </summary>
    /// <value>The identifier of the Fund that this Entry refers to.</value>
    [DataMember(Name="fund", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fund")]
    public string Fund { get; set; }

    /// <summary>
    /// If the activity that this Entry refers to is the charging of a Fee, then this field stores the identifier of the corresponding Fee resource.
    /// </summary>
    /// <value>If the activity that this Entry refers to is the charging of a Fee, then this field stores the identifier of the corresponding Fee resource.</value>
    [DataMember(Name="fee", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fee")]
    public string Fee { get; set; }

    /// <summary>
    /// If the activity that this Entry refers to is the charging of a Disbursement, then this field stores the identifier of the corresponding Disbursement resource.
    /// </summary>
    /// <value>If the activity that this Entry refers to is the charging of a Disbursement, then this field stores the identifier of the corresponding Disbursement resource.</value>
    [DataMember(Name="disbursement", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "disbursement")]
    public string Disbursement { get; set; }

    /// <summary>
    /// If the activity that this Entry refers to is the paying of a Refund, then this field stores the identifier of the corresponding Refund resource.
    /// </summary>
    /// <value>If the activity that this Entry refers to is the paying of a Refund, then this field stores the identifier of the corresponding Refund resource.</value>
    [DataMember(Name="refund", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "refund")]
    public string Refund { get; set; }

    /// <summary>
    /// If the activity that this Entry refers to is a Transaction, then this field stores the identifier of the corresponding Transaction resource.
    /// </summary>
    /// <value>If the activity that this Entry refers to is a Transaction, then this field stores the identifier of the corresponding Transaction resource.</value>
    [DataMember(Name="txn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txn")]
    public string Txn { get; set; }

    /// <summary>
    /// If the activity that this Entry refers to is a Chargeback, then this field stores the identifier of the corresponding Chargeback resource.
    /// </summary>
    /// <value>If the activity that this Entry refers to is a Chargeback, then this field stores the identifier of the corresponding Chargeback resource.</value>
    [DataMember(Name="chargeback", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "chargeback")]
    public string Chargeback { get; set; }

    /// <summary>
    /// Gets or Sets Adjustment
    /// </summary>
    [DataMember(Name="adjustment", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "adjustment")]
    public string Adjustment { get; set; }

    /// <summary>
    /// The type of event that triggered this Entry resource.  Valid values are:  '1': Daily - the Entry triggers every day.  '2': Weekly - the Entry triggers every week.  '3': Monthly - the Entry triggers every month.  '4': Annually - the Entry triggers every year.  '5': Single - the Entry is a one-off event.  '6': Auth - the Entry triggers at the time of authorization of a transaction.  '7': Capture - the Entry triggers at the capture time of a Transaction.  '8': Refund - the Entry triggers when a refund transaction is processed.  '9': Board - the Entry triggers when the Merchant is boarded.  '10': Payout - the Entry triggers when a payout is processed.  '11': Chargeback - the Entry triggers when a card chargeback occurs.  '12': Overdraft - the Entry triggers when an overdraft usage charge from a bank is levied.  '13': Interchange - the Entry triggers when interchange Fees are assessed for the Transactions of this Merchant.  '14': Processor - the Entry triggers when the Transactions of this Merchant are processed by a payment processor.  '15': ACH failure - the Entry triggers when an automated clearing house failure occurs.  '16': Account - the Entry triggers when a bank account is verified.
    /// </summary>
    /// <value>The type of event that triggered this Entry resource.  Valid values are:  '1': Daily - the Entry triggers every day.  '2': Weekly - the Entry triggers every week.  '3': Monthly - the Entry triggers every month.  '4': Annually - the Entry triggers every year.  '5': Single - the Entry is a one-off event.  '6': Auth - the Entry triggers at the time of authorization of a transaction.  '7': Capture - the Entry triggers at the capture time of a Transaction.  '8': Refund - the Entry triggers when a refund transaction is processed.  '9': Board - the Entry triggers when the Merchant is boarded.  '10': Payout - the Entry triggers when a payout is processed.  '11': Chargeback - the Entry triggers when a card chargeback occurs.  '12': Overdraft - the Entry triggers when an overdraft usage charge from a bank is levied.  '13': Interchange - the Entry triggers when interchange Fees are assessed for the Transactions of this Merchant.  '14': Processor - the Entry triggers when the Transactions of this Merchant are processed by a payment processor.  '15': ACH failure - the Entry triggers when an automated clearing house failure occurs.  '16': Account - the Entry triggers when a bank account is verified.</value>
    [DataMember(Name="event", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "event")]
    public int? _Event { get; set; }

    /// <summary>
    /// The identifier of the record that is associated with this Entry.
    /// </summary>
    /// <value>The identifier of the record that is associated with this Entry.</value>
    [DataMember(Name="eventId", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "eventId")]
    public string EventId { get; set; }

    /// <summary>
    /// A description of this Entry.
    /// </summary>
    /// <value>A description of this Entry.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The amount involved in this Entry. It refers to the amount charged, transferred, or disbursed.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount involved in this Entry. It refers to the amount charged, transferred, or disbursed.  This field is specified as an integer in cents.</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public string Amount { get; set; }

    /// <summary>
    /// Gets or Sets Unsourced
    /// </summary>
    [DataMember(Name="unsourced", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "unsourced")]
    public string Unsourced { get; set; }

    /// <summary>
    /// Gets or Sets Pending
    /// </summary>
    [DataMember(Name="pending", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "pending")]
    public int? Pending { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class InlineResponse20023ResponseData {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Fromentity: ").Append(Fromentity).Append("\n");
      sb.Append("  Fund: ").Append(Fund).Append("\n");
      sb.Append("  Fee: ").Append(Fee).Append("\n");
      sb.Append("  Disbursement: ").Append(Disbursement).Append("\n");
      sb.Append("  Refund: ").Append(Refund).Append("\n");
      sb.Append("  Txn: ").Append(Txn).Append("\n");
      sb.Append("  Chargeback: ").Append(Chargeback).Append("\n");
      sb.Append("  Adjustment: ").Append(Adjustment).Append("\n");
      sb.Append("  _Event: ").Append(_Event).Append("\n");
      sb.Append("  EventId: ").Append(EventId).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("  Unsourced: ").Append(Unsourced).Append("\n");
      sb.Append("  Pending: ").Append(Pending).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
