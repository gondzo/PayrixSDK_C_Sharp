using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class OrgEntitiesUpdateFields {
    /// <summary>
    /// The identifier of the Org that this orgEntity is associated with.
    /// </summary>
    /// <value>The identifier of the Org that this orgEntity is associated with.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// The identifier of the Entity that this orgEntity is associated with.
    /// </summary>
    /// <value>The identifier of the Entity that this orgEntity is associated with.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class OrgEntitiesUpdateFields {\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
