using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class MappingsUpdateRest {
    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// Gets or Sets Name
    /// </summary>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A description of this Mapping.
    /// </summary>
    /// <value>A description of this Mapping.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// A JSON document that describes the input fields that should be mapped.  This field is stored as a text string and must be between 1 and 5000 characters long.
    /// </summary>
    /// <value>A JSON document that describes the input fields that should be mapped.  This field is stored as a text string and must be between 1 and 5000 characters long.</value>
    [DataMember(Name="input", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "input")]
    public string Input { get; set; }

    /// <summary>
    /// A JSON document that describes the fields that should appear in the output, after the input fields have been mapped.  This field is stored as a text string and must be between 1 and 5000 characters long.
    /// </summary>
    /// <value>A JSON document that describes the fields that should appear in the output, after the input fields have been mapped.  This field is stored as a text string and must be between 1 and 5000 characters long.</value>
    [DataMember(Name="output", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "output")]
    public string Output { get; set; }

    /// <summary>
    /// A valid URL that represents the XML namespace of the input and output documents.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>A valid URL that represents the XML namespace of the input and output documents.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="namespace", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "namespace")]
    public string _Namespace { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class MappingsUpdateRest {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Input: ").Append(Input).Append("\n");
      sb.Append("  Output: ").Append(Output).Append("\n");
      sb.Append("  _Namespace: ").Append(_Namespace).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
