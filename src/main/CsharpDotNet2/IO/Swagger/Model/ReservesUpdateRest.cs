using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ReservesUpdateRest {
    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the Org that this Reserves resource applies to.  If you set this field, then the Reserve applies to all Entities in the Org.
    /// </summary>
    /// <value>The identifier of the Org that this Reserves resource applies to.  If you set this field, then the Reserve applies to all Entities in the Org.</value>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// The identifier of the Entity that this Reserve applies to.
    /// </summary>
    /// <value>The identifier of the Entity that this Reserve applies to.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The name of this Reserve.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The name of this Reserve.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A description of this Reserve.   This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>A description of this Reserve.   This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The percentage of funds to reserve, expressed in basis points.  For example, 25.3% is expressed as '2530'.
    /// </summary>
    /// <value>The percentage of funds to reserve, expressed in basis points.  For example, 25.3% is expressed as '2530'.</value>
    [DataMember(Name="percent", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "percent")]
    public int? Percent { get; set; }

    /// <summary>
    /// The schedule that determines when the funds in this Reserve should be released.  Valid values are:  '1': Daily - the funds are released every day.  '2': Weekly - the funds are released every week.  '3': Monthly - the funds are released every month.  '4': Annually - the funds are released every year.
    /// </summary>
    /// <value>The schedule that determines when the funds in this Reserve should be released.  Valid values are:  '1': Daily - the funds are released every day.  '2': Weekly - the funds are released every week.  '3': Monthly - the funds are released every month.  '4': Annually - the funds are released every year.</value>
    [DataMember(Name="release", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "release")]
    public int? Release { get; set; }

    /// <summary>
    /// A multiplier that you can use to adjust the schedule set in the 'release' field.  This field is specified as an integer and its value determines how the schedule is multiplied.  For example, if 'release' is set to '1' (meaning 'daily'), then a 'releaseFactor' value of '2' would cause the funds to be released from this Reserve every two days.
    /// </summary>
    /// <value>A multiplier that you can use to adjust the schedule set in the 'release' field.  This field is specified as an integer and its value determines how the schedule is multiplied.  For example, if 'release' is set to '1' (meaning 'daily'), then a 'releaseFactor' value of '2' would cause the funds to be released from this Reserve every two days.</value>
    [DataMember(Name="releaseFactor", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "releaseFactor")]
    public int? ReleaseFactor { get; set; }

    /// <summary>
    /// The date on which this Reserve resource should stop reserving funds from the Entity or Org.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.
    /// </summary>
    /// <value>The date on which this Reserve resource should stop reserving funds from the Entity or Org.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.</value>
    [DataMember(Name="finish", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "finish")]
    public int? Finish { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ReservesUpdateRest {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Percent: ").Append(Percent).Append("\n");
      sb.Append("  Release: ").Append(Release).Append("\n");
      sb.Append("  ReleaseFactor: ").Append(ReleaseFactor).Append("\n");
      sb.Append("  Finish: ").Append(Finish).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
