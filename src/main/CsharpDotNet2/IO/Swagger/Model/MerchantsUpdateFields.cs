using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class MerchantsUpdateFields {
    /// <summary>
    /// The Entity associated with this Merchant.
    /// </summary>
    /// <value>The Entity associated with this Merchant.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// The name under which the Merchant is doing business, if applicable.
    /// </summary>
    /// <value>The name under which the Merchant is doing business, if applicable.</value>
    [DataMember(Name="dba", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "dba")]
    public string Dba { get; set; }

    /// <summary>
    /// An indicator that specifies whether the Merchant is new to credit card processing.  A value of '1' means new and a value of '0' means not new. By default, merchants are considered to be new.
    /// </summary>
    /// <value>An indicator that specifies whether the Merchant is new to credit card processing.  A value of '1' means new and a value of '0' means not new. By default, merchants are considered to be new.</value>
    [DataMember(Name="new", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "new")]
    public int? _New { get; set; }

    /// <summary>
    /// The date on which the Merchant was established.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.
    /// </summary>
    /// <value>The date on which the Merchant was established.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.</value>
    [DataMember(Name="established", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "established")]
    public int? Established { get; set; }

    /// <summary>
    /// The value of the annual credit card sales of this Merchant.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The value of the annual credit card sales of this Merchant.  This field is specified as an integer in cents.</value>
    [DataMember(Name="annualCCSales", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "annualCCSales")]
    public int? AnnualCCSales { get; set; }

    /// <summary>
    /// Gets or Sets AvgTicket
    /// </summary>
    [DataMember(Name="avgTicket", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "avgTicket")]
    public int? AvgTicket { get; set; }

    /// <summary>
    /// The American Express merchant identifier for this Merchant, if applicable.
    /// </summary>
    /// <value>The American Express merchant identifier for this Merchant, if applicable.</value>
    [DataMember(Name="amex", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amex")]
    public string Amex { get; set; }

    /// <summary>
    /// The Discover merchant identifier for this Merchant, if applicable.
    /// </summary>
    /// <value>The Discover merchant identifier for this Merchant, if applicable.</value>
    [DataMember(Name="discover", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "discover")]
    public string Discover { get; set; }

    /// <summary>
    /// The Merchant Category Code (MCC) for this Merchant. This code is not required to create a Merchant, but it is required to successfully board a Merchant.
    /// </summary>
    /// <value>The Merchant Category Code (MCC) for this Merchant. This code is not required to create a Merchant, but it is required to successfully board a Merchant.</value>
    [DataMember(Name="mcc", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "mcc")]
    public string Mcc { get; set; }

    /// <summary>
    /// The status of the Merchant. Valid values are '0' (not ready), '1' (ready), '2' (boarded), '3' (manual) and '4' (denied).
    /// </summary>
    /// <value>The status of the Merchant. Valid values are '0' (not ready), '1' (ready), '2' (boarded), '3' (manual) and '4' (denied).</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }

    /// <summary>
    /// An indicator showing the version of the terms and conditions that this Merchant has accepted. The API indicates the version as an integer.
    /// </summary>
    /// <value>An indicator showing the version of the terms and conditions that this Merchant has accepted. The API indicates the version as an integer.</value>
    [DataMember(Name="tcVersion", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tcVersion")]
    public int? TcVersion { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class MerchantsUpdateFields {\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Dba: ").Append(Dba).Append("\n");
      sb.Append("  _New: ").Append(_New).Append("\n");
      sb.Append("  Established: ").Append(Established).Append("\n");
      sb.Append("  AnnualCCSales: ").Append(AnnualCCSales).Append("\n");
      sb.Append("  AvgTicket: ").Append(AvgTicket).Append("\n");
      sb.Append("  Amex: ").Append(Amex).Append("\n");
      sb.Append("  Discover: ").Append(Discover).Append("\n");
      sb.Append("  Mcc: ").Append(Mcc).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("  TcVersion: ").Append(TcVersion).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
