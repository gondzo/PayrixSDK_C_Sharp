using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// The payment method associated with this Transaction, including the card details.
  /// </summary>
  [DataContract]
  public class TxnspaymentResponseFields {
    /// <summary>
    /// The payment method for thei Transaction.  This field is specified as an integer.  Valid values are:  '1': American Express  '2': Visa  '3': MasterCard  '4': Diners Club  '5': Discover  '6': PayPal  '7': Debit card  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account  '12': Gift card  '13': EBT  '14':WIC.
    /// </summary>
    /// <value>The payment method for thei Transaction.  This field is specified as an integer.  Valid values are:  '1': American Express  '2': Visa  '3': MasterCard  '4': Diners Club  '5': Discover  '6': PayPal  '7': Debit card  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account  '12': Gift card  '13': EBT  '14':WIC.</value>
    [DataMember(Name="method", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "method")]
    public int? Method { get; set; }

    /// <summary>
    /// The card number of the credit card associated with this Transaction.
    /// </summary>
    /// <value>The card number of the credit card associated with this Transaction.</value>
    [DataMember(Name="number", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "number")]
    public string Number { get; set; }

    /// <summary>
    /// The routing code for the eCheck or bank account payment associated with this Transaction.
    /// </summary>
    /// <value>The routing code for the eCheck or bank account payment associated with this Transaction.</value>
    [DataMember(Name="routing", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "routing")]
    public string Routing { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TxnspaymentResponseFields {\n");
      sb.Append("  Method: ").Append(Method).Append("\n");
      sb.Append("  Number: ").Append(Number).Append("\n");
      sb.Append("  Routing: ").Append(Routing).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
