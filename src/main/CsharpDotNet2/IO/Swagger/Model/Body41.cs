using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body41 {
    /// <summary>
    /// The identifier of the messageThreads that owns this Messages resource.
    /// </summary>
    /// <value>The identifier of the messageThreads that owns this Messages resource.</value>
    [DataMember(Name="messageThread", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "messageThread")]
    public string MessageThread { get; set; }

    /// <summary>
    /// Gets or Sets OpposingMessage
    /// </summary>
    [DataMember(Name="opposingMessage", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "opposingMessage")]
    public string OpposingMessage { get; set; }

    /// <summary>
    /// Whether this resource is incoming or outgoing. By default, an outgoing message is assigned a '2' and incoming messages is assigned a '1'.
    /// </summary>
    /// <value>Whether this resource is incoming or outgoing. By default, an outgoing message is assigned a '2' and incoming messages is assigned a '1'.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// Whether this resource was automatically generated or not. A value of '1' means the message was auomatically generated and a value of '0' means it was manually generated.
    /// </summary>
    /// <value>Whether this resource was automatically generated or not. A value of '1' means the message was auomatically generated and a value of '0' means it was manually generated.</value>
    [DataMember(Name="generated", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "generated")]
    public int? Generated { get; set; }

    /// <summary>
    /// Whether this resource is marked as secure. A value of '1' means messages will protected in email notifications and a value of '0' means the message will display entirely.
    /// </summary>
    /// <value>Whether this resource is marked as secure. A value of '1' means messages will protected in email notifications and a value of '0' means the message will display entirely.</value>
    [DataMember(Name="secure", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "secure")]
    public int? Secure { get; set; }

    /// <summary>
    /// Whether this resource is marked as read. A value of '1' means the message has been read and a value of '0' means the message has not been read yet
    /// </summary>
    /// <value>Whether this resource is marked as read. A value of '1' means the message has been read and a value of '0' means the message has not been read yet</value>
    [DataMember(Name="read", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "read")]
    public int? Read { get; set; }

    /// <summary>
    /// Free-form text for adding a message to a messageThread resource.
    /// </summary>
    /// <value>Free-form text for adding a message to a messageThread resource.</value>
    [DataMember(Name="message", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "message")]
    public string Message { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body41 {\n");
      sb.Append("  MessageThread: ").Append(MessageThread).Append("\n");
      sb.Append("  OpposingMessage: ").Append(OpposingMessage).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Generated: ").Append(Generated).Append("\n");
      sb.Append("  Secure: ").Append(Secure).Append("\n");
      sb.Append("  Read: ").Append(Read).Append("\n");
      sb.Append("  Message: ").Append(Message).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
