using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ChargebacksUpdateRest {
    /// <summary>
    /// Gets or Sets Txn
    /// </summary>
    [DataMember(Name="txn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txn")]
    public string Txn { get; set; }

    /// <summary>
    /// Gets or Sets Merchant
    /// </summary>
    [DataMember(Name="merchant", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "merchant")]
    public string Merchant { get; set; }

    /// <summary>
    /// Gets or Sets Mid
    /// </summary>
    [DataMember(Name="mid", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "mid")]
    public string Mid { get; set; }

    /// <summary>
    /// Gets or Sets Total
    /// </summary>
    [DataMember(Name="total", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "total")]
    public int? Total { get; set; }

    /// <summary>
    /// Gets or Sets RepresentedTotal
    /// </summary>
    [DataMember(Name="representedTotal", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "representedTotal")]
    public int? RepresentedTotal { get; set; }

    /// <summary>
    /// Gets or Sets Description
    /// </summary>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// Gets or Sets Currency
    /// </summary>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// Gets or Sets _Ref
    /// </summary>
    [DataMember(Name="ref", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ref")]
    public string _Ref { get; set; }

    /// <summary>
    /// Gets or Sets Cycle
    /// </summary>
    [DataMember(Name="cycle", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "cycle")]
    public int? Cycle { get; set; }

    /// <summary>
    /// Gets or Sets Reason
    /// </summary>
    [DataMember(Name="reason", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reason")]
    public string Reason { get; set; }

    /// <summary>
    /// Gets or Sets ReasonCode
    /// </summary>
    [DataMember(Name="reasonCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reasonCode")]
    public string ReasonCode { get; set; }

    /// <summary>
    /// Gets or Sets Issued
    /// </summary>
    [DataMember(Name="issued", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "issued")]
    public int? Issued { get; set; }

    /// <summary>
    /// Gets or Sets Received
    /// </summary>
    [DataMember(Name="received", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "received")]
    public int? Received { get; set; }

    /// <summary>
    /// Gets or Sets Reply
    /// </summary>
    [DataMember(Name="reply", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reply")]
    public int? Reply { get; set; }

    /// <summary>
    /// Gets or Sets BankRef
    /// </summary>
    [DataMember(Name="bankRef", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "bankRef")]
    public string BankRef { get; set; }

    /// <summary>
    /// Gets or Sets ChargebackRef
    /// </summary>
    [DataMember(Name="chargebackRef", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "chargebackRef")]
    public string ChargebackRef { get; set; }

    /// <summary>
    /// Gets or Sets Status
    /// </summary>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ChargebacksUpdateRest {\n");
      sb.Append("  Txn: ").Append(Txn).Append("\n");
      sb.Append("  Merchant: ").Append(Merchant).Append("\n");
      sb.Append("  Mid: ").Append(Mid).Append("\n");
      sb.Append("  Total: ").Append(Total).Append("\n");
      sb.Append("  RepresentedTotal: ").Append(RepresentedTotal).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  _Ref: ").Append(_Ref).Append("\n");
      sb.Append("  Cycle: ").Append(Cycle).Append("\n");
      sb.Append("  Reason: ").Append(Reason).Append("\n");
      sb.Append("  ReasonCode: ").Append(ReasonCode).Append("\n");
      sb.Append("  Issued: ").Append(Issued).Append("\n");
      sb.Append("  Received: ").Append(Received).Append("\n");
      sb.Append("  Reply: ").Append(Reply).Append("\n");
      sb.Append("  BankRef: ").Append(BankRef).Append("\n");
      sb.Append("  ChargebackRef: ").Append(ChargebackRef).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
