using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body22 {
    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The Merchant associated with this Customer.
    /// </summary>
    /// <value>The Merchant associated with this Customer.</value>
    [DataMember(Name="merchant", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "merchant")]
    public string Merchant { get; set; }

    /// <summary>
    /// The first name associated with this Customer.
    /// </summary>
    /// <value>The first name associated with this Customer.</value>
    [DataMember(Name="first", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "first")]
    public string First { get; set; }

    /// <summary>
    /// The middle name associated with this Customer.
    /// </summary>
    /// <value>The middle name associated with this Customer.</value>
    [DataMember(Name="middle", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "middle")]
    public string Middle { get; set; }

    /// <summary>
    /// The last name associated with this Customer.
    /// </summary>
    /// <value>The last name associated with this Customer.</value>
    [DataMember(Name="last", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "last")]
    public string Last { get; set; }

    /// <summary>
    /// The name of the company associated with this Customer.
    /// </summary>
    /// <value>The name of the company associated with this Customer.</value>
    [DataMember(Name="company", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "company")]
    public string Company { get; set; }

    /// <summary>
    /// The email address of this Customer.
    /// </summary>
    /// <value>The email address of this Customer.</value>
    [DataMember(Name="email", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "email")]
    public string Email { get; set; }

    /// <summary>
    /// The first line of the address associated with this Customer.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The first line of the address associated with this Customer.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="address1", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address1")]
    public string Address1 { get; set; }

    /// <summary>
    /// The second line of the address associated with this Customer.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The second line of the address associated with this Customer.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="address2", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address2")]
    public string Address2 { get; set; }

    /// <summary>
    /// The name of the city in the address associated with this Customer.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The name of the city in the address associated with this Customer.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="city", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "city")]
    public string City { get; set; }

    /// <summary>
    /// The state associated with this Customer.  If in the U.S. this is specified as the 2 character postal abbreviation for the state, if outside of the U.S. the full state name.  This field is stored as a text string and must be between 2 and 100 characters long.
    /// </summary>
    /// <value>The state associated with this Customer.  If in the U.S. this is specified as the 2 character postal abbreviation for the state, if outside of the U.S. the full state name.  This field is stored as a text string and must be between 2 and 100 characters long.</value>
    [DataMember(Name="state", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "state")]
    public string State { get; set; }

    /// <summary>
    /// The ZIP code in the address associated with this Customer.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The ZIP code in the address associated with this Customer.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="zip", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "zip")]
    public string Zip { get; set; }

    /// <summary>
    /// The country associated with this Customer.  Valid values for this field is the 3-letter ISO code for the country.
    /// </summary>
    /// <value>The country associated with this Customer.  Valid values for this field is the 3-letter ISO code for the country.</value>
    [DataMember(Name="country", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "country")]
    public string Country { get; set; }

    /// <summary>
    /// The phone number associated with this Transaction.  This field is stored as a text string and must be between 10 and 15 characters long.
    /// </summary>
    /// <value>The phone number associated with this Transaction.  This field is stored as a text string and must be between 10 and 15 characters long.</value>
    [DataMember(Name="phone", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "phone")]
    public string Phone { get; set; }

    /// <summary>
    /// The fax number associated with this Customer.  This field is stored as a text string and must be between 10 and 15 characters long.
    /// </summary>
    /// <value>The fax number associated with this Customer.  This field is stored as a text string and must be between 10 and 15 characters long.</value>
    [DataMember(Name="fax", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fax")]
    public string Fax { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body22 {\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Merchant: ").Append(Merchant).Append("\n");
      sb.Append("  First: ").Append(First).Append("\n");
      sb.Append("  Middle: ").Append(Middle).Append("\n");
      sb.Append("  Last: ").Append(Last).Append("\n");
      sb.Append("  Company: ").Append(Company).Append("\n");
      sb.Append("  Email: ").Append(Email).Append("\n");
      sb.Append("  Address1: ").Append(Address1).Append("\n");
      sb.Append("  Address2: ").Append(Address2).Append("\n");
      sb.Append("  City: ").Append(City).Append("\n");
      sb.Append("  State: ").Append(State).Append("\n");
      sb.Append("  Zip: ").Append(Zip).Append("\n");
      sb.Append("  Country: ").Append(Country).Append("\n");
      sb.Append("  Phone: ").Append(Phone).Append("\n");
      sb.Append("  Fax: ").Append(Fax).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
