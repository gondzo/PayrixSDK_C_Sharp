using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body38 {
    /// <summary>
    /// Gets or Sets Txn
    /// </summary>
    [DataMember(Name="txn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txn")]
    public string Txn { get; set; }

    /// <summary>
    /// Gets or Sets Item
    /// </summary>
    [DataMember(Name="item", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "item")]
    public string Item { get; set; }

    /// <summary>
    /// A description of this Item.   This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>A description of this Item.   This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// A custom identifier for this line Item, such as a stock number or order code.
    /// </summary>
    /// <value>A custom identifier for this line Item, such as a stock number or order code.</value>
    [DataMember(Name="custom", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "custom")]
    public string Custom { get; set; }

    /// <summary>
    /// The quantity of this Item included in the Transaction.  This field is specified as an integer.
    /// </summary>
    /// <value>The quantity of this Item included in the Transaction.  This field is specified as an integer.</value>
    [DataMember(Name="quantity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "quantity")]
    public int? Quantity { get; set; }

    /// <summary>
    /// The amount charged for this Item.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount charged for this Item.  This field is specified as an integer in cents.</value>
    [DataMember(Name="price", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "price")]
    public int? Price { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body38 {\n");
      sb.Append("  Txn: ").Append(Txn).Append("\n");
      sb.Append("  Item: ").Append(Item).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Custom: ").Append(Custom).Append("\n");
      sb.Append("  Quantity: ").Append(Quantity).Append("\n");
      sb.Append("  Price: ").Append(Price).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
