using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class OrgFlowActionsCreateRest {
    /// <summary>
    /// The identifier of the orgFlow resource that this orgFlowActions resource is associated with.
    /// </summary>
    /// <value>The identifier of the orgFlow resource that this orgFlowActions resource is associated with.</value>
    [DataMember(Name="orgFlow", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "orgFlow")]
    public string OrgFlow { get; set; }

    /// <summary>
    /// Gets or Sets Org
    /// </summary>
    [DataMember(Name="org", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "org")]
    public string Org { get; set; }

    /// <summary>
    /// The action to take in relation to the Entity being processed.   Valid values are:  '1': Add the referenced Entity to the referenced Org.  '2': Remove the referenced Entity from the referenced Org.
    /// </summary>
    /// <value>The action to take in relation to the Entity being processed.   Valid values are:  '1': Add the referenced Entity to the referenced Org.  '2': Remove the referenced Entity from the referenced Org.</value>
    [DataMember(Name="action", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "action")]
    public int? Action { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class OrgFlowActionsCreateRest {\n");
      sb.Append("  OrgFlow: ").Append(OrgFlow).Append("\n");
      sb.Append("  Org: ").Append(Org).Append("\n");
      sb.Append("  Action: ").Append(Action).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
