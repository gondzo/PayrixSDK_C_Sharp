using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class MessageThreadsUpdateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Login that owns this messageThreads resource.
    /// </summary>
    /// <value>The identifier of the Login that owns this messageThreads resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the receiving Login of this messageThreads resource.
    /// </summary>
    /// <value>The identifier of the receiving Login of this messageThreads resource.</value>
    [DataMember(Name="forlogin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "forlogin")]
    public string Forlogin { get; set; }

    /// <summary>
    /// The identifier of the TxnHold that is related to this messageThread
    /// </summary>
    /// <value>The identifier of the TxnHold that is related to this messageThread</value>
    [DataMember(Name="txnHold", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txnHold")]
    public string TxnHold { get; set; }

    /// <summary>
    /// Gets or Sets OpposingMessageThread
    /// </summary>
    [DataMember(Name="opposingMessageThread", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "opposingMessageThread")]
    public string OpposingMessageThread { get; set; }

    /// <summary>
    /// Free-form text. By default, a messageThread resource is set as 'default'.
    /// </summary>
    /// <value>Free-form text. By default, a messageThread resource is set as 'default'.</value>
    [DataMember(Name="folder", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "folder")]
    public string Folder { get; set; }

    /// <summary>
    /// Free-form text that represents the name of the sender of a messageThread resource.
    /// </summary>
    /// <value>Free-form text that represents the name of the sender of a messageThread resource.</value>
    [DataMember(Name="sender", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "sender")]
    public string Sender { get; set; }

    /// <summary>
    /// Free-form text that represents the name of the recipient of a messageThread resource.
    /// </summary>
    /// <value>Free-form text that represents the name of the recipient of a messageThread resource.</value>
    [DataMember(Name="recipient", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "recipient")]
    public string Recipient { get; set; }

    /// <summary>
    /// Free-form text for adding a subject to a messageThread resource.
    /// </summary>
    /// <value>Free-form text for adding a subject to a messageThread resource.</value>
    [DataMember(Name="subject", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "subject")]
    public string Subject { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class MessageThreadsUpdateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Forlogin: ").Append(Forlogin).Append("\n");
      sb.Append("  TxnHold: ").Append(TxnHold).Append("\n");
      sb.Append("  OpposingMessageThread: ").Append(OpposingMessageThread).Append("\n");
      sb.Append("  Folder: ").Append(Folder).Append("\n");
      sb.Append("  Sender: ").Append(Sender).Append("\n");
      sb.Append("  Recipient: ").Append(Recipient).Append("\n");
      sb.Append("  Subject: ").Append(Subject).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
