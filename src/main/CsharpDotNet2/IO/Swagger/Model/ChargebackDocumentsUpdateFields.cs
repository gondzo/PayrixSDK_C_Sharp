using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ChargebackDocumentsUpdateFields {
    /// <summary>
    /// The ref of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.   The value is set when the file is properly integrated, otherwise will be null.
    /// </summary>
    /// <value>The ref of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.   The value is set when the file is properly integrated, otherwise will be null.</value>
    [DataMember(Name="ref", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ref")]
    public string _Ref { get; set; }

    /// <summary>
    /// The description of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The description of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The current status of the chargebackDocument.  Valid values are:  '0': Created. The ChargebackDocument has been created.  '1': Processed. The ChargebackDocument integration has been successful.  '2': Failed. The ChargebackDocument integration has failed.
    /// </summary>
    /// <value>The current status of the chargebackDocument.  Valid values are:  '0': Created. The ChargebackDocument has been created.  '1': Processed. The ChargebackDocument integration has been successful.  '2': Failed. The ChargebackDocument integration has failed.</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// The type of the file that holds this chargebackDocument.   Valid values are:  'jpg', 'jpeg', 'gif', 'png', 'pdf', 'tif', 'tiff'.   The value is set when the file is properly integrated, otherwise will be null.
    /// </summary>
    /// <value>The type of the file that holds this chargebackDocument.   Valid values are:  'jpg', 'jpeg', 'gif', 'png', 'pdf', 'tif', 'tiff'.   The value is set when the file is properly integrated, otherwise will be null.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public string Type { get; set; }

    /// <summary>
    /// The name of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.   The value is set when the file is created and properly integrated. Holds the real file name used by the user.
    /// </summary>
    /// <value>The name of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.   The value is set when the file is created and properly integrated. Holds the real file name used by the user.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ChargebackDocumentsUpdateFields {\n");
      sb.Append("  _Ref: ").Append(_Ref).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
