using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class TxnsUpdateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The incoming ip address from which this Transaction was created.
    /// </summary>
    /// <value>The incoming ip address from which this Transaction was created.</value>
    [DataMember(Name="ipCreated", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ipCreated")]
    public string IpCreated { get; set; }

    /// <summary>
    /// The incoming ip address from which this Transaction was last modified.
    /// </summary>
    /// <value>The incoming ip address from which this Transaction was last modified.</value>
    [DataMember(Name="ipModified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ipModified")]
    public string IpModified { get; set; }

    /// <summary>
    /// The identifier of the Merchant associated with this Transaction.
    /// </summary>
    /// <value>The identifier of the Merchant associated with this Transaction.</value>
    [DataMember(Name="merchant", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "merchant")]
    public string Merchant { get; set; }

    /// <summary>
    /// The identifier of the Token associated with this Transaction.
    /// </summary>
    /// <value>The identifier of the Token associated with this Transaction.</value>
    [DataMember(Name="token", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "token")]
    public string Token { get; set; }

    /// <summary>
    /// If this Transaction is related to another Transaction, then this field is set to the identifier of the other Transaction.  For example, if this Transaction is a refund, this field could be set to the identifier of the original sale Transaction.
    /// </summary>
    /// <value>If this Transaction is related to another Transaction, then this field is set to the identifier of the other Transaction.  For example, if this Transaction is a refund, this field could be set to the identifier of the original sale Transaction.</value>
    [DataMember(Name="fortxn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fortxn")]
    public string Fortxn { get; set; }

    /// <summary>
    /// If the Transaction is linked to a Batch, this field specifies the identifier of the Batch.
    /// </summary>
    /// <value>If the Transaction is linked to a Batch, this field specifies the identifier of the Batch.</value>
    [DataMember(Name="batch", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "batch")]
    public string Batch { get; set; }

    /// <summary>
    /// The identifier of the Subscription associated with this Transaction.
    /// </summary>
    /// <value>The identifier of the Subscription associated with this Transaction.</value>
    [DataMember(Name="subscription", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "subscription")]
    public string Subscription { get; set; }

    /// <summary>
    /// The type of Transaction.  This field is specified as an integer.  Valid values are:  '1': Sale Transaction. This is the most common type of Transaction, it processes a sale and charges the customer,  '2': Auth Transaction. Authorizes and holds the requested total on the credit card,  '3': Capture Transaction. Finalizes a prior Auth Transaction and charges the customer,  '4': Reverse Authorization. Reverses a prior Auth Transaction and releases the credit hold  '5': Refund Transaction. Refunds a prior Capture or Sale Transaction (total may be specified for a partial refund).  '7': Echeck Sale Transaction. Sale Transaction for ECheck payment.  '9': ECheck Refund Transaction. Refund Transaction for prior ECheck Sale Transaction.  '10': Echeck PreSale Transaction. Notification of imminent Echeck Sale Transaction.  '11': Echeck PreRefund Transaction. Notification of imminent Echeck Refund Transaction.  '12': Echeck Verification Transaction. Attempt to verify validity of Echeck payment prior to sale.  '13': Echeck Void Transaction. Voids prior Echeck transaction.
    /// </summary>
    /// <value>The type of Transaction.  This field is specified as an integer.  Valid values are:  '1': Sale Transaction. This is the most common type of Transaction, it processes a sale and charges the customer,  '2': Auth Transaction. Authorizes and holds the requested total on the credit card,  '3': Capture Transaction. Finalizes a prior Auth Transaction and charges the customer,  '4': Reverse Authorization. Reverses a prior Auth Transaction and releases the credit hold  '5': Refund Transaction. Refunds a prior Capture or Sale Transaction (total may be specified for a partial refund).  '7': Echeck Sale Transaction. Sale Transaction for ECheck payment.  '9': ECheck Refund Transaction. Refund Transaction for prior ECheck Sale Transaction.  '10': Echeck PreSale Transaction. Notification of imminent Echeck Sale Transaction.  '11': Echeck PreRefund Transaction. Notification of imminent Echeck Refund Transaction.  '12': Echeck Verification Transaction. Attempt to verify validity of Echeck payment prior to sale.  '13': Echeck Void Transaction. Voids prior Echeck transaction.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// The expiration date of this Transaction.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.  The value must reflect a future date.
    /// </summary>
    /// <value>The expiration date of this Transaction.  This field is stored as a text string in 'MMYY' format, where 'MM' is the number of a month and 'YY' is the last two digits of a year. For example, '0623' for June 2023.  The value must reflect a future date.</value>
    [DataMember(Name="expiration", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "expiration")]
    public string Expiration { get; set; }

    /// <summary>
    /// The currency of the txn.  Currently, this field only accepts the value 'USD'.
    /// </summary>
    /// <value>The currency of the txn.  Currently, this field only accepts the value 'USD'.</value>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// The date on which the Transaction was authorized.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the past.
    /// </summary>
    /// <value>The date on which the Transaction was authorized.  The date is specified as an eight digit string in YYYYMMDD format, for example, '20160120' for January 20, 2016.  The value of this field must represent a date in the past.</value>
    [DataMember(Name="authDate", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "authDate")]
    public int? AuthDate { get; set; }

    /// <summary>
    /// The authorization code for this Transaction.  This field is stored as a text string and must be between 0 and 20 characters long.
    /// </summary>
    /// <value>The authorization code for this Transaction.  This field is stored as a text string and must be between 0 and 20 characters long.</value>
    [DataMember(Name="authCode", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "authCode")]
    public string AuthCode { get; set; }

    /// <summary>
    /// A timestamp indicating when this Transaction was captured.  This field is set automatically.
    /// </summary>
    /// <value>A timestamp indicating when this Transaction was captured.  This field is set automatically.</value>
    [DataMember(Name="captured", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "captured")]
    public DateTime? Captured { get; set; }

    /// <summary>
    /// A date indicating when this Transaction was settled.  This field is set automatically.
    /// </summary>
    /// <value>A date indicating when this Transaction was settled.  This field is set automatically.</value>
    [DataMember(Name="settled", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "settled")]
    public int? Settled { get; set; }

    /// <summary>
    /// The currency of the settled total.  This field is set automatically.
    /// </summary>
    /// <value>The currency of the settled total.  This field is set automatically.</value>
    [DataMember(Name="settledCurrency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "settledCurrency")]
    public string SettledCurrency { get; set; }

    /// <summary>
    /// The total amount that was settled.   This field is specified as an integer in cents and is set automatically.
    /// </summary>
    /// <value>The total amount that was settled.   This field is specified as an integer in cents and is set automatically.</value>
    [DataMember(Name="settledTotal", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "settledTotal")]
    public int? SettledTotal { get; set; }

    /// <summary>
    /// Whether to allow partial amount authorizations of this Transaction.  For example, if the transaction amount is $1000 and the processor only authorizes a smaller amount, then enabling this field  lets the Transaction proceed anyway.  A value of '1' means that partial amount authorizations are allowed and a value of '0' means that partial amount authorizations are not allowed.
    /// </summary>
    /// <value>Whether to allow partial amount authorizations of this Transaction.  For example, if the transaction amount is $1000 and the processor only authorizes a smaller amount, then enabling this field  lets the Transaction proceed anyway.  A value of '1' means that partial amount authorizations are allowed and a value of '0' means that partial amount authorizations are not allowed.</value>
    [DataMember(Name="allowPartial", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "allowPartial")]
    public int? AllowPartial { get; set; }

    /// <summary>
    /// The identifier of the Order associated with this Transaction.  This field is stored as a text string and must be between 0 and 20 characters long.
    /// </summary>
    /// <value>The identifier of the Order associated with this Transaction.  This field is stored as a text string and must be between 0 and 20 characters long.</value>
    [DataMember(Name="order", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "order")]
    public string Order { get; set; }

    /// <summary>
    /// A description of this Transaction.   This field is stored as a text string and must be between 0 and 100 characters long.
    /// </summary>
    /// <value>A description of this Transaction.   This field is stored as a text string and must be between 0 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The descriptor used in this Transaction.   This field is stored as a text string and must be between 1 and 50 characters long. If a value is not set, an attempt is made to set a default value from the merchant information.
    /// </summary>
    /// <value>The descriptor used in this Transaction.   This field is stored as a text string and must be between 1 and 50 characters long. If a value is not set, an attempt is made to set a default value from the merchant information.</value>
    [DataMember(Name="descriptor", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "descriptor")]
    public string Descriptor { get; set; }

    /// <summary>
    /// The identifier of the terminal that processed this Transaction.  The identifier is taken from the terminal system and varies in format according to the type of terminal.  This field is stored as a text string and must be between 0 and 50 characters long.
    /// </summary>
    /// <value>The identifier of the terminal that processed this Transaction.  The identifier is taken from the terminal system and varies in format according to the type of terminal.  This field is stored as a text string and must be between 0 and 50 characters long.</value>
    [DataMember(Name="terminal", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "terminal")]
    public string Terminal { get; set; }

    /// <summary>
    /// The origin of this Transaction.  Valid values are:  '1': Originated at a credit card terminal.  '2': Originated through an eCommerce system.  '3': Originated as a mail order or telephone order transaction.  '4': Originated with Apple Pay.  '5': Originated as a 3D Secure authorized transaction.  '6': Originated as a 3D Secure transaction.  '7': Originated as a recurring transaction on the card.
    /// </summary>
    /// <value>The origin of this Transaction.  Valid values are:  '1': Originated at a credit card terminal.  '2': Originated through an eCommerce system.  '3': Originated as a mail order or telephone order transaction.  '4': Originated with Apple Pay.  '5': Originated as a 3D Secure authorized transaction.  '6': Originated as a 3D Secure transaction.  '7': Originated as a recurring transaction on the card.</value>
    [DataMember(Name="origin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "origin")]
    public int? Origin { get; set; }

    /// <summary>
    /// The amount of the total sum of this Transaction that is made up of tax.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount of the total sum of this Transaction that is made up of tax.  This field is specified as an integer in cents.</value>
    [DataMember(Name="tax", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tax")]
    public int? Tax { get; set; }

    /// <summary>
    /// The total amount of this Transaction.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The total amount of this Transaction.  This field is specified as an integer in cents.</value>
    [DataMember(Name="total", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "total")]
    public int? Total { get; set; }

    /// <summary>
    /// The amount of the total sum of this Transaction that is given as cash back.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The amount of the total sum of this Transaction that is given as cash back.  This field is specified as an integer in cents.</value>
    [DataMember(Name="cashback", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "cashback")]
    public int? Cashback { get; set; }

    /// <summary>
    /// The authorization code for this Transaction, as returned by the network.
    /// </summary>
    /// <value>The authorization code for this Transaction, as returned by the network.</value>
    [DataMember(Name="authorization", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "authorization")]
    public string Authorization { get; set; }

    /// <summary>
    /// The total amount that was approved for this Transaction by the processor.  This field is specified as an integer in cents.
    /// </summary>
    /// <value>The total amount that was approved for this Transaction by the processor.  This field is specified as an integer in cents.</value>
    [DataMember(Name="approved", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "approved")]
    public int? Approved { get; set; }

    /// <summary>
    /// Whether correct cvv was sent during this Transaction. A value of '1' means cvv was sent and was correct and a value of '0' means that cvv was not sent or was not correct.
    /// </summary>
    /// <value>Whether correct cvv was sent during this Transaction. A value of '1' means cvv was sent and was correct and a value of '0' means that cvv was not sent or was not correct.</value>
    [DataMember(Name="cvv", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "cvv")]
    public int? Cvv { get; set; }

    /// <summary>
    /// Whether the card was swiped during this Transaction.  This field is set to '1' automatically if 'track' data was received.  A value of '1' means swiped and a value of '0' means not swiped.
    /// </summary>
    /// <value>Whether the card was swiped during this Transaction.  This field is set to '1' automatically if 'track' data was received.  A value of '1' means swiped and a value of '0' means not swiped.</value>
    [DataMember(Name="swiped", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "swiped")]
    public int? Swiped { get; set; }

    /// <summary>
    /// Whether the card was dipped (using the EMV chip) during this Transaction.  This field is set to '1' automatically if 'EMV' data was received.  A value of '1' means dipped and a value of '0' means not dipped.
    /// </summary>
    /// <value>Whether the card was dipped (using the EMV chip) during this Transaction.  This field is set to '1' automatically if 'EMV' data was received.  A value of '1' means dipped and a value of '0' means not dipped.</value>
    [DataMember(Name="emv", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "emv")]
    public int? Emv { get; set; }

    /// <summary>
    /// Whether a signature was captured during this Transaction.  A value of '1' means a signature was captured and a value of '0' means a signature was not captured.  You can set this field if you took a signature for the Transaction. The API also sets this field automatically if you associate a signature to the Transaction by creating a 'txnDatas' resource.
    /// </summary>
    /// <value>Whether a signature was captured during this Transaction.  A value of '1' means a signature was captured and a value of '0' means a signature was not captured.  You can set this field if you took a signature for the Transaction. The API also sets this field automatically if you associate a signature to the Transaction by creating a 'txnDatas' resource.</value>
    [DataMember(Name="signature", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "signature")]
    public int? Signature { get; set; }

    /// <summary>
    /// Whether the card was swiped at an unattended terminal during this Transaction.  This field is set to '0' by default.  A value of '1' means the terminal was unattended and a value of '0' means the terminal was attended.
    /// </summary>
    /// <value>Whether the card was swiped at an unattended terminal during this Transaction.  This field is set to '0' by default.  A value of '1' means the terminal was unattended and a value of '0' means the terminal was attended.</value>
    [DataMember(Name="unattended", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "unattended")]
    public int? Unattended { get; set; }

    /// <summary>
    /// The client ip address from which the Transaction was created.  Valid values are any Ipv4 or Ipv6 address.
    /// </summary>
    /// <value>The client ip address from which the Transaction was created.  Valid values are any Ipv4 or Ipv6 address.</value>
    [DataMember(Name="clientIp", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "clientIp")]
    public string ClientIp { get; set; }

    /// <summary>
    /// The first name associated with this Transaction.
    /// </summary>
    /// <value>The first name associated with this Transaction.</value>
    [DataMember(Name="first", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "first")]
    public string First { get; set; }

    /// <summary>
    /// The middle name associated with this Transaction.
    /// </summary>
    /// <value>The middle name associated with this Transaction.</value>
    [DataMember(Name="middle", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "middle")]
    public string Middle { get; set; }

    /// <summary>
    /// The last name associated with this Transaction.
    /// </summary>
    /// <value>The last name associated with this Transaction.</value>
    [DataMember(Name="last", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "last")]
    public string Last { get; set; }

    /// <summary>
    /// The name of the company associated with this Transaction.  Setting this field is especially important when processing an eCheck from a company.
    /// </summary>
    /// <value>The name of the company associated with this Transaction.  Setting this field is especially important when processing an eCheck from a company.</value>
    [DataMember(Name="company", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "company")]
    public string Company { get; set; }

    /// <summary>
    /// Gets or Sets Email
    /// </summary>
    [DataMember(Name="email", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "email")]
    public string Email { get; set; }

    /// <summary>
    /// The first line of the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The first line of the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="address1", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address1")]
    public string Address1 { get; set; }

    /// <summary>
    /// The second line of the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The second line of the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="address2", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "address2")]
    public string Address2 { get; set; }

    /// <summary>
    /// The name of the city in the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The name of the city in the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="city", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "city")]
    public string City { get; set; }

    /// <summary>
    /// The state associated with this Transaction.  If in the U.S. this is specified as the 2 character postal abbreviation for the state, if outside of the U.S. the full state name.  This field is stored as a text string and must be between 2 and 100 characters long.
    /// </summary>
    /// <value>The state associated with this Transaction.  If in the U.S. this is specified as the 2 character postal abbreviation for the state, if outside of the U.S. the full state name.  This field is stored as a text string and must be between 2 and 100 characters long.</value>
    [DataMember(Name="state", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "state")]
    public string State { get; set; }

    /// <summary>
    /// The ZIP code in the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 20 characters long.
    /// </summary>
    /// <value>The ZIP code in the address associated with this Transaction.  This field is stored as a text string and must be between 1 and 20 characters long.</value>
    [DataMember(Name="zip", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "zip")]
    public string Zip { get; set; }

    /// <summary>
    /// The country associated with this Transaction.  Valid values for this field is the 3-letter ISO code for the country.
    /// </summary>
    /// <value>The country associated with this Transaction.  Valid values for this field is the 3-letter ISO code for the country.</value>
    [DataMember(Name="country", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "country")]
    public string Country { get; set; }

    /// <summary>
    /// The phone number associated with this Transaction.  This field is stored as a text string and must be between 10 and 15 characters long.
    /// </summary>
    /// <value>The phone number associated with this Transaction.  This field is stored as a text string and must be between 10 and 15 characters long.</value>
    [DataMember(Name="phone", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "phone")]
    public string Phone { get; set; }

    /// <summary>
    /// The status of the Transaction. Valid values are '0' (pending), '1' (approved), '2' (failed), '3' (captured), '4' (settled) and '5' (returned).
    /// </summary>
    /// <value>The status of the Transaction. Valid values are '0' (pending), '1' (approved), '2' (failed), '3' (captured), '4' (settled) and '5' (returned).</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// The amount of this Transaction that has been refunded.
    /// </summary>
    /// <value>The amount of this Transaction that has been refunded.</value>
    [DataMember(Name="refunded", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "refunded")]
    public int? Refunded { get; set; }

    /// <summary>
    /// Indicates whether the Transaction is reserved and the action that will be taken as a result.  This field is specified as an integer.  Valid values are:  '0': Not reserved  '1': If the Transaction is a sale or authorization, then block the capture of the Transaction.  '2': Apply a manual override to any checks on the Transaction and allow it to proceed and  '3': Move all funds from this Transaction into a reserve.
    /// </summary>
    /// <value>Indicates whether the Transaction is reserved and the action that will be taken as a result.  This field is specified as an integer.  Valid values are:  '0': Not reserved  '1': If the Transaction is a sale or authorization, then block the capture of the Transaction.  '2': Apply a manual override to any checks on the Transaction and allow it to proceed and  '3': Move all funds from this Transaction into a reserve.</value>
    [DataMember(Name="reserved", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reserved")]
    public int? Reserved { get; set; }

    /// <summary>
    /// The last transaction stage check for risk.
    /// </summary>
    /// <value>The last transaction stage check for risk.</value>
    [DataMember(Name="checkStage", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "checkStage")]
    public string CheckStage { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TxnsUpdateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  IpCreated: ").Append(IpCreated).Append("\n");
      sb.Append("  IpModified: ").Append(IpModified).Append("\n");
      sb.Append("  Merchant: ").Append(Merchant).Append("\n");
      sb.Append("  Token: ").Append(Token).Append("\n");
      sb.Append("  Fortxn: ").Append(Fortxn).Append("\n");
      sb.Append("  Batch: ").Append(Batch).Append("\n");
      sb.Append("  Subscription: ").Append(Subscription).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Expiration: ").Append(Expiration).Append("\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  AuthDate: ").Append(AuthDate).Append("\n");
      sb.Append("  AuthCode: ").Append(AuthCode).Append("\n");
      sb.Append("  Captured: ").Append(Captured).Append("\n");
      sb.Append("  Settled: ").Append(Settled).Append("\n");
      sb.Append("  SettledCurrency: ").Append(SettledCurrency).Append("\n");
      sb.Append("  SettledTotal: ").Append(SettledTotal).Append("\n");
      sb.Append("  AllowPartial: ").Append(AllowPartial).Append("\n");
      sb.Append("  Order: ").Append(Order).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Descriptor: ").Append(Descriptor).Append("\n");
      sb.Append("  Terminal: ").Append(Terminal).Append("\n");
      sb.Append("  Origin: ").Append(Origin).Append("\n");
      sb.Append("  Tax: ").Append(Tax).Append("\n");
      sb.Append("  Total: ").Append(Total).Append("\n");
      sb.Append("  Cashback: ").Append(Cashback).Append("\n");
      sb.Append("  Authorization: ").Append(Authorization).Append("\n");
      sb.Append("  Approved: ").Append(Approved).Append("\n");
      sb.Append("  Cvv: ").Append(Cvv).Append("\n");
      sb.Append("  Swiped: ").Append(Swiped).Append("\n");
      sb.Append("  Emv: ").Append(Emv).Append("\n");
      sb.Append("  Signature: ").Append(Signature).Append("\n");
      sb.Append("  Unattended: ").Append(Unattended).Append("\n");
      sb.Append("  ClientIp: ").Append(ClientIp).Append("\n");
      sb.Append("  First: ").Append(First).Append("\n");
      sb.Append("  Middle: ").Append(Middle).Append("\n");
      sb.Append("  Last: ").Append(Last).Append("\n");
      sb.Append("  Company: ").Append(Company).Append("\n");
      sb.Append("  Email: ").Append(Email).Append("\n");
      sb.Append("  Address1: ").Append(Address1).Append("\n");
      sb.Append("  Address2: ").Append(Address2).Append("\n");
      sb.Append("  City: ").Append(City).Append("\n");
      sb.Append("  State: ").Append(State).Append("\n");
      sb.Append("  Zip: ").Append(Zip).Append("\n");
      sb.Append("  Country: ").Append(Country).Append("\n");
      sb.Append("  Phone: ").Append(Phone).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Refunded: ").Append(Refunded).Append("\n");
      sb.Append("  Reserved: ").Append(Reserved).Append("\n");
      sb.Append("  CheckStage: ").Append(CheckStage).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
