using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class TxnVerificationsQueryResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of Transaction associated with this check.
    /// </summary>
    /// <value>The identifier of Transaction associated with this check.</value>
    [DataMember(Name="txn", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txn")]
    public string Txn { get; set; }

    /// <summary>
    /// Gets or Sets TxnCheck
    /// </summary>
    [DataMember(Name="txnCheck", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "txnCheck")]
    public string TxnCheck { get; set; }

    /// <summary>
    /// The type of check to perform.  This field is specified as an integer.  Valid values are: n'1': Exceeded the maximum number of allowed failed authorizations.  '2': Exceeded the maximum allowed sale total value.  '3': Exceeded the maximum allowed refund total value.  '4': Exceeded the allowed maximum payment size (individual transaction amount).  '5': Exceeded the maximum allowed ratio of refunds to sales.  '6': Exceeded the maximum allowed number of successful authorizations.  '7': Exceeded the maximum allowed number of failed authorizations for a particular IP address.  '8': Exceeded the maximum allowed ratio of failed authorizations for a particular IP address.  '9': The Merchant is not active.  '10': Exceeded the maximum allowed number of failed transactions.  '11': Exceeded the maximum allowed number of transactions without authorizations.  '12': Refund transaction does not have an associated sale transaction.  '13': Exceeded the maximum number of refund transactions that do not have associated sale Transactions.  '14': Exceeded the maximum authorized value for Transactions with failed authorizations.
    /// </summary>
    /// <value>The type of check to perform.  This field is specified as an integer.  Valid values are: n'1': Exceeded the maximum number of allowed failed authorizations.  '2': Exceeded the maximum allowed sale total value.  '3': Exceeded the maximum allowed refund total value.  '4': Exceeded the allowed maximum payment size (individual transaction amount).  '5': Exceeded the maximum allowed ratio of refunds to sales.  '6': Exceeded the maximum allowed number of successful authorizations.  '7': Exceeded the maximum allowed number of failed authorizations for a particular IP address.  '8': Exceeded the maximum allowed ratio of failed authorizations for a particular IP address.  '9': The Merchant is not active.  '10': Exceeded the maximum allowed number of failed transactions.  '11': Exceeded the maximum allowed number of transactions without authorizations.  '12': Refund transaction does not have an associated sale transaction.  '13': Exceeded the maximum number of refund transactions that do not have associated sale Transactions.  '14': Exceeded the maximum authorized value for Transactions with failed authorizations.</value>
    [DataMember(Name="type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "type")]
    public int? Type { get; set; }

    /// <summary>
    /// The score for this Transaction against the specific type of check.  This field is specified as an integer between 0 and 65535.
    /// </summary>
    /// <value>The score for this Transaction against the specific type of check.  This field is specified as an integer between 0 and 65535.</value>
    [DataMember(Name="score", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "score")]
    public int? Score { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TxnVerificationsQueryResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Txn: ").Append(Txn).Append("\n");
      sb.Append("  TxnCheck: ").Append(TxnCheck).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("  Score: ").Append(Score).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
