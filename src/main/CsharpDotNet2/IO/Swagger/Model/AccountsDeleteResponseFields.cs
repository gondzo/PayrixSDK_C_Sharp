using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class AccountsDeleteResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The identifier of the Entity associated with this Account.
    /// </summary>
    /// <value>The identifier of the Entity associated with this Account.</value>
    [DataMember(Name="entity", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "entity")]
    public string Entity { get; set; }

    /// <summary>
    /// An object representing details of the Account, including the type of Account (method), Account number and routing code.
    /// </summary>
    /// <value>An object representing details of the Account, including the type of Account (method), Account number and routing code.</value>
    [DataMember(Name="account", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "account")]
    public string Account { get; set; }

    /// <summary>
    /// A unique token that can be used to refer to this Account in other parts of the API.
    /// </summary>
    /// <value>A unique token that can be used to refer to this Account in other parts of the API.</value>
    [DataMember(Name="token", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "token")]
    public string Token { get; set; }

    /// <summary>
    /// A client-supplied name for this bank account.
    /// </summary>
    /// <value>A client-supplied name for this bank account.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A client-supplied description for this bank account.
    /// </summary>
    /// <value>A client-supplied description for this bank account.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// Indicates whether the Account is the 'primary' Account for the associated Entity.  Only one Account associated with each Entity can be the 'primary' Account.  A value of '1' means the Account is the primary and a value of '0' means the Account is not the primary.
    /// </summary>
    /// <value>Indicates whether the Account is the 'primary' Account for the associated Entity.  Only one Account associated with each Entity can be the 'primary' Account.  A value of '1' means the Account is the primary and a value of '0' means the Account is not the primary.</value>
    [DataMember(Name="primary", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "primary")]
    public int? Primary { get; set; }

    /// <summary>
    /// The status of the Account.  Valid values are:  '0': Not Ready. The account holder is not yet ready to verify the Account.  '1': Ready. The account is ready to be verified.  '2': Challenged - the account has processed the challenge.  '3': Verified. The Account has been verified.  '4': Manual. There has been an issue during verification and further attempts to verify the Account will require manual intervention.
    /// </summary>
    /// <value>The status of the Account.  Valid values are:  '0': Not Ready. The account holder is not yet ready to verify the Account.  '1': Ready. The account is ready to be verified.  '2': Challenged - the account has processed the challenge.  '3': Verified. The Account has been verified.  '4': Manual. There has been an issue during verification and further attempts to verify the Account will require manual intervention.</value>
    [DataMember(Name="status", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "status")]
    public int? Status { get; set; }

    /// <summary>
    /// The currency of this Account.  Currently, this field only accepts the value 'USD'.
    /// </summary>
    /// <value>The currency of this Account.  Currently, this field only accepts the value 'USD'.</value>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccountsDeleteResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Entity: ").Append(Entity).Append("\n");
      sb.Append("  Account: ").Append(Account).Append("\n");
      sb.Append("  Token: ").Append(Token).Append("\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Primary: ").Append(Primary).Append("\n");
      sb.Append("  Status: ").Append(Status).Append("\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
