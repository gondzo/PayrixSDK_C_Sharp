using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class OrgFlowsCreateResponseFields {
    /// <summary>
    /// The ID of this resource.
    /// </summary>
    /// <value>The ID of this resource.</value>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// The date and time at which this resource was created.
    /// </summary>
    /// <value>The date and time at which this resource was created.</value>
    [DataMember(Name="created", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "created")]
    public DateTime? Created { get; set; }

    /// <summary>
    /// The date and time at which this resource was modified.
    /// </summary>
    /// <value>The date and time at which this resource was modified.</value>
    [DataMember(Name="modified", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modified")]
    public DateTime? Modified { get; set; }

    /// <summary>
    /// The identifier of the Login that created this resource.
    /// </summary>
    /// <value>The identifier of the Login that created this resource.</value>
    [DataMember(Name="creator", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "creator")]
    public string Creator { get; set; }

    /// <summary>
    /// The identifier of the Login that last modified this resource.
    /// </summary>
    /// <value>The identifier of the Login that last modified this resource.</value>
    [DataMember(Name="modifier", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "modifier")]
    public string Modifier { get; set; }

    /// <summary>
    /// The Login that owns this resource.
    /// </summary>
    /// <value>The Login that owns this resource.</value>
    [DataMember(Name="login", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "login")]
    public string Login { get; set; }

    /// <summary>
    /// The identifier of the Login resource for which this orgFlows resource is triggered.
    /// </summary>
    /// <value>The identifier of the Login resource for which this orgFlows resource is triggered.</value>
    [DataMember(Name="forlogin", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "forlogin")]
    public string Forlogin { get; set; }

    /// <summary>
    /// Whether this resource should affect logins recursively - in other words, affect this Login and all its child Logins.  Valid values are:  '0': Not recursive. The orgFlow only affects the Login identified in the 'forLogin' field.  '1': Recursive. The orgFlow affects the Login identified in the 'forLogin' field and all its child Logins.
    /// </summary>
    /// <value>Whether this resource should affect logins recursively - in other words, affect this Login and all its child Logins.  Valid values are:  '0': Not recursive. The orgFlow only affects the Login identified in the 'forLogin' field.  '1': Recursive. The orgFlow affects the Login identified in the 'forLogin' field and all its child Logins.</value>
    [DataMember(Name="recursive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "recursive")]
    public int? Recursive { get; set; }

    /// <summary>
    /// This field sets the trigger that determines when this orgFlow runs.   Valid values are:  '1': Trigger at Merchant creation time.  '2': Trigger when a Merchant check returns a low score.  '3': Trigger when a Merchant check returns a high score.  '4': Trigger at Merchant boarding time.
    /// </summary>
    /// <value>This field sets the trigger that determines when this orgFlow runs.   Valid values are:  '1': Trigger at Merchant creation time.  '2': Trigger when a Merchant check returns a low score.  '3': Trigger when a Merchant check returns a high score.  '4': Trigger at Merchant boarding time.</value>
    [DataMember(Name="trigger", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "trigger")]
    public int? Trigger { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class OrgFlowsCreateResponseFields {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Created: ").Append(Created).Append("\n");
      sb.Append("  Modified: ").Append(Modified).Append("\n");
      sb.Append("  Creator: ").Append(Creator).Append("\n");
      sb.Append("  Modifier: ").Append(Modifier).Append("\n");
      sb.Append("  Login: ").Append(Login).Append("\n");
      sb.Append("  Forlogin: ").Append(Forlogin).Append("\n");
      sb.Append("  Recursive: ").Append(Recursive).Append("\n");
      sb.Append("  Trigger: ").Append(Trigger).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
