using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// The payment method that is associated with this Token.
  /// </summary>
  [DataContract]
  public class TokenspaymentRequestFields {
    /// <summary>
    /// The method used to make this payment.  This field is specified as an integer.  Valid values are:  '1': American Express  '2': Visa  '3': MasterCard  '4': Diners Club  '5': Discover  '6': PayPal  '7': Debit card  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account  '12': Gift card  '13': EBT  '14':WIC.
    /// </summary>
    /// <value>The method used to make this payment.  This field is specified as an integer.  Valid values are:  '1': American Express  '2': Visa  '3': MasterCard  '4': Diners Club  '5': Discover  '6': PayPal  '7': Debit card  '8': Checking account  '9': Savings account  '10': Corporate checking account and  '11': Corporate savings account  '12': Gift card  '13': EBT  '14':WIC.</value>
    [DataMember(Name="method", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "method")]
    public int? Method { get; set; }

    /// <summary>
    /// The card number or bank account number of the payment method associated with this Token.
    /// </summary>
    /// <value>The card number or bank account number of the payment method associated with this Token.</value>
    [DataMember(Name="number", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "number")]
    public string Number { get; set; }

    /// <summary>
    /// If the payment method associated with this Token is a bank account, then this field contains the routing code for the bank account.
    /// </summary>
    /// <value>If the payment method associated with this Token is a bank account, then this field contains the routing code for the bank account.</value>
    [DataMember(Name="routing", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "routing")]
    public string Routing { get; set; }

    /// <summary>
    /// Gets or Sets Expiration
    /// </summary>
    [DataMember(Name="expiration", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "expiration")]
    public string Expiration { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TokenspaymentRequestFields {\n");
      sb.Append("  Method: ").Append(Method).Append("\n");
      sb.Append("  Number: ").Append(Number).Append("\n");
      sb.Append("  Routing: ").Append(Routing).Append("\n");
      sb.Append("  Expiration: ").Append(Expiration).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
