using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Body19 {
    /// <summary>
    /// The name of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>The name of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// A description of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.
    /// </summary>
    /// <value>A description of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long.</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// The username to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.
    /// </summary>
    /// <value>The username to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.</value>
    [DataMember(Name="username", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "username")]
    public string Username { get; set; }

    /// <summary>
    /// The password to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.
    /// </summary>
    /// <value>The password to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.</value>
    [DataMember(Name="password", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "password")]
    public string Password { get; set; }

    /// <summary>
    /// The username to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.
    /// </summary>
    /// <value>The username to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.</value>
    [DataMember(Name="connectUsername", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "connectUsername")]
    public string ConnectUsername { get; set; }

    /// <summary>
    /// The password to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.
    /// </summary>
    /// <value>The password to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration.</value>
    [DataMember(Name="connectPassword", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "connectPassword")]
    public string ConnectPassword { get; set; }

    /// <summary>
    /// Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.
    /// </summary>
    /// <value>Whether this resource is marked as inactive. A value of '1' means inactive and a value of '0' means active.</value>
    [DataMember(Name="inactive", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "inactive")]
    public int? Inactive { get; set; }

    /// <summary>
    /// Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.
    /// </summary>
    /// <value>Whether this resource is marked as frozen. A value of '1' means frozen and a value of '0' means not frozen.</value>
    [DataMember(Name="frozen", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "frozen")]
    public int? Frozen { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Body19 {\n");
      sb.Append("  Name: ").Append(Name).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Username: ").Append(Username).Append("\n");
      sb.Append("  Password: ").Append(Password).Append("\n");
      sb.Append("  ConnectUsername: ").Append(ConnectUsername).Append("\n");
      sb.Append("  ConnectPassword: ").Append(ConnectPassword).Append("\n");
      sb.Append("  Inactive: ").Append(Inactive).Append("\n");
      sb.Append("  Frozen: ").Append(Frozen).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
