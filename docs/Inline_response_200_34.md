# IO.Swagger.Model.InlineResponse20034
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Response** | [**InlineResponse20034Response**](InlineResponse20034Response.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

