# IO.Swagger.Model.InlineResponse20018ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Entity** | **string** |  | [optional] 
**Account** | **string** |  | [optional] 
**Payout** | **string** |  | [optional] 
**Funding** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Amount** | **int?** |  | [optional] 
**Status** | **int?** |  | [optional] 
**Processed** | **DateTime?** |  | [optional] 
**Currency** | **string** |  | [optional] 
**Platform** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

