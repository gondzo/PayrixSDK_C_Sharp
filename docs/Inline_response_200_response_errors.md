# IO.Swagger.Model.InlineResponse200ResponseErrors
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **int?** | The error code associated with a particular error. | [optional] 
**Severity** | **int?** | The severity level of a particular error.  Possible values are &#39;0&#39; (informational), &#39;1&#39; (warning), &#39;2&#39; (error) and &#39;4&#39; (failure). | [optional] 
**Message** | **string** | The message associated with a particular error. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

