# IO.Swagger.Model.ChargebackMessageResultsQueryResponseRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Response** | [**InlineResponse20011Response**](InlineResponse20011Response.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

