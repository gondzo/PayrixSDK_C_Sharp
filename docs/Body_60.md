# IO.Swagger.Model.Body60
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Forlogin** | **string** | The identifier of the Login resource that should be marked as part of the Org identified in the &#39;org&#39; field. | 
**Org** | **string** | The identifier of the Org resource that the Login identified in the &#39;forlogin&#39; field should be marked as part of. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

