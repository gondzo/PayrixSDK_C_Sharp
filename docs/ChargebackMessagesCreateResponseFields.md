# IO.Swagger.Model.ChargebackMessagesCreateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Chargeback** | **string** | The identifier of the Chargeback resource that this chargebackMessage relates to. | [optional] 
**Date** | **int?** | The date of this chargebackMessage.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016. | [optional] 
**Type** | **int?** | The type of this chargebackMessage.   Valid values are:  &#39;1&#39;: Assign. Request to assign the Chargeback to another party.  &#39;2&#39;: Notate.  &#39;3&#39;: Accept liability. The Merchant accepts liability for this Chargeback.  &#39;4&#39;: Represent. The Merchant wishes to dispute the Chargeback and request a representment.  &#39;5&#39;: Respond. The Merchant requests a response from the other party.  &#39;6&#39;: Request Arbitration. The Merchant wishes to enter arbitration to determine the outcome of the Chargeback | [optional] 
**FromQueue** | **string** |  | [optional] 
**ToQueue** | **string** |  | [optional] 
**Contact** | **string** | The identifier of the Contact for this chargebackMessage. | [optional] 
**Amount** | **int?** | The amount that this chargebackMessage corresponds to.  For example, if the &#39;type&#39; is set to &#39;3&#39; (Accept Liability), then this amount indicates that the liability should be for this amount.  This field is specified as an integer in cents. | [optional] 
**Currency** | **string** | The currency of the amount in this chargebackMessage.  Currently, this field only accepts the value &#39;USD&#39;. | [optional] 
**Note** | **string** | A free-text note relating to this chargebackMessage. | [optional] 
**Status** | **int?** | The current status of the Chargeback.  Valid values are:  &#39;1&#39;: Requested. The Chargeback has been requested from the processor.  &#39;2&#39;: Processing. The Chargeback is being processed by the card processor.  &#39;3&#39;: Failed. The Chargeback has failed because of a technical problem.  &#39;4&#39;: Denied. The issuer has denied the Chargeback.  &#39;5&#39;: Processed. The Chargeback has been accepted and processed. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 
**Imported** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

