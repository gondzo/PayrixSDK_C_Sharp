# IO.Swagger.Model.Body40
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The identifier of the Login. | [optional] 
**Division** | **string** |  | [optional] 
**Roles** | **int?** | The roles associated with this Login, specified as an integer. | 
**Username** | **string** | The username associated with this Login.  This field is stored as a text string and must be between 0 and 50 characters long. | 
**Password** | **string** | The password associated with this Login.  This field is stored as a text string and must be between 0 and 100 characters long. | 
**First** | **string** | The first name associated with this Login. | 
**Middle** | **string** | The middle name associated with this Login. | [optional] 
**Last** | **string** | The last name associated with this Login. | 
**Email** | **string** | The email address associated with this Login. | 
**Address1** | **string** | The first line of the address associated with this Login.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Address2** | **string** | The second line of the address associated with this Login.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**City** | **string** | The name of the city in the address associated with this Login.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**State** | **string** | The state associated with this Login.  If in the U.S. this is specified as the 2 character postal abbreviation for the state, if outside of the U.S. the full state name.  This field is stored as a text string and must be between 2 and 100 characters long. | [optional] 
**Zip** | **string** | The ZIP code in the address associated with this Login.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**Country** | **string** | The country associated with this Customer.  Valid values for this field is the 3-letter ISO code for the country. | [optional] 
**Phone** | **string** | The phone number associated with this Login.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Fax** | **string** | The fax number associated with this Login.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Inactive** | **int?** | Whether this Login is marked as inactive.  A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this Login should be marked as frozen.  A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

