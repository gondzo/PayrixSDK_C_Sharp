# IO.Swagger.Model.FeeRulesQueryResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Fee** | **string** | The identifier of the Fee that this Fee Rule applies. | [optional] 
**Name** | **string** | The name of this Fee Rule.  This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Description** | **string** |  | [optional] 
**Type** | **int?** | The type of logic to apply with this Fee Rule.  This field is specified as an integer.  Valid values are:  &#39;1&#39;: Less than - the Fee applies only if the triggered amount is lower than the amount set in the &#39;value&#39; field of the Fee Rule,  &#39;2&#39;: Equal to - the Fee applies only if the transaction amount is exactly the same as the amount set in the &#39;value&#39; field of the Fee Rule,  &#39;3&#39;: Not equal to - the Fee applies only if the transaction amount is not exactly equal to the amount set in the &#39;value&#39; field of the Fee Rule,  &#39;4&#39;: Greater than - the Fee applies only if the transaction amount is higher than the amount set in the &#39;value&#39; field of the Fee Rule and  &#39;5&#39;: Swiped - the Fee applies based on a determination of whether the cardholder was present during the transaction. | [optional] 
**Value** | **string** | The value to compare against when evaluating this Fee Rule.  When the &#39;type&#39; field is set to one of the comparison operators (&#39;1 - Less than&#39;, &#39;2 - Equal to&#39;, &#39;3 - Not equal to&#39;, or &#39;4 - Greater than&#39;), this field represents the comparator value in cents.  When the &#39;type&#39; field is set to &#39;5&#39; (Swiped), this field represents the cardholder presence state to check the Transaction against. A value of &#39;1&#39; means that the card was swiped and the cardholder was present, while a value of &#39;0&#39; means that the card was not swiped and the cardholder was not present. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 
**Grouping** | **string** | A name for a group of rules to be applied in conjunction when evaluating this Fee Rule.  When grouping is used the Fee will be allowed to be processed if all rules are matched. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

