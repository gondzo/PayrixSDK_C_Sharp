# IO.Swagger.Model.InlineResponse20063ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The identifier of the Login that owns this txnHolds resource. | [optional] 
**Txn** | **string** | The identifier of the Txn that is being held with this txnHold. | [optional] 
**TxnVerification** | **string** | If this txnHold resource was triggered through a txnVerification, then this field stores the identifier of the TxnVerification. | [optional] 
**Action** | **int?** | The action taken on the referenced Txn.  This field is specified as an integer.  Valid values are:  &#39;1&#39;: Block. Block the Transaction from proceeding. This returns an error.  &#39;2&#39;: Reserved for future use.  &#39;3&#39;: Hold. Hold the Transaction. It will not be captured until it is manually released.  &#39;4&#39;: Reserve. Reserve the Transaction. The funds for the transaction will not be released until the Transaction is manually reviewed. | [optional] 
**Released** | **DateTime?** | If this txnHold was released, this will contain the timestamp for when it was released. | [optional] 
**Reviewed** | **DateTime?** | If this txnHold was reviewed, this will contain the timestamp for when it was reviewed. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

