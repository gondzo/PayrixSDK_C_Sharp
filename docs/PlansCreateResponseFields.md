# IO.Swagger.Model.PlansCreateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Merchant** | **string** | The identifier of the Merchant associated with this Plan. | [optional] 
**Name** | **string** | The name of this Plan.  This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Description** | **string** | A description of this Plan.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Schedule** | **int?** |  | [optional] 
**ScheduleFactor** | **int?** |  | [optional] 
**Amount** | **int?** | The amount to charge with each payment under this Plan.  This field is specified as an integer in cents. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

