# IO.Swagger.Model.InlineResponse20034ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The identifier of the Login that owns this messageThreads resource. | [optional] 
**Forlogin** | **string** | The identifier of the receiving Login of this messageThreads resource. | [optional] 
**TxnHold** | **string** | The identifier of the TxnHold that is related to this messageThread | [optional] 
**OpposingMessageThread** | **string** |  | [optional] 
**Folder** | **string** | Free-form text. By default, a messageThread resource is set as &#39;default&#39;. | [optional] 
**Sender** | **string** | Free-form text that represents the name of the sender of a messageThread resource. | [optional] 
**Recipient** | **string** | Free-form text that represents the name of the recipient of a messageThread resource. | [optional] 
**Subject** | **string** | Free-form text for adding a subject to a messageThread resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

