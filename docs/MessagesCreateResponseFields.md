# IO.Swagger.Model.MessagesCreateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**MessageThread** | **string** | The identifier of the messageThreads that owns this Messages resource. | [optional] 
**OpposingMessage** | **string** |  | [optional] 
**Type** | **int?** | Whether this resource is incoming or outgoing. By default, an outgoing message is assigned a &#39;2&#39; and incoming messages is assigned a &#39;1&#39;. | [optional] 
**Generated** | **int?** | Whether this resource was automatically generated or not. A value of &#39;1&#39; means the message was auomatically generated and a value of &#39;0&#39; means it was manually generated. | [optional] 
**Secure** | **int?** | Whether this resource is marked as secure. A value of &#39;1&#39; means messages will protected in email notifications and a value of &#39;0&#39; means the message will display entirely. | [optional] 
**Read** | **int?** | Whether this resource is marked as read. A value of &#39;1&#39; means the message has been read and a value of &#39;0&#39; means the message has not been read yet | [optional] 
**Message** | **string** | Free-form text for adding a message to a messageThread resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

