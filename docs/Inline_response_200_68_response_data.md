# IO.Swagger.Model.InlineResponse20068ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The Login that owns this resource. | [optional] 
**Txn** | **string** | The identifier of Transaction associated with this check. | [optional] 
**TxnCheck** | **string** |  | [optional] 
**Type** | **int?** | The type of check to perform.  This field is specified as an integer.  Valid values are: n&#39;1&#39;: Exceeded the maximum number of allowed failed authorizations.  &#39;2&#39;: Exceeded the maximum allowed sale total value.  &#39;3&#39;: Exceeded the maximum allowed refund total value.  &#39;4&#39;: Exceeded the allowed maximum payment size (individual transaction amount).  &#39;5&#39;: Exceeded the maximum allowed ratio of refunds to sales.  &#39;6&#39;: Exceeded the maximum allowed number of successful authorizations.  &#39;7&#39;: Exceeded the maximum allowed number of failed authorizations for a particular IP address.  &#39;8&#39;: Exceeded the maximum allowed ratio of failed authorizations for a particular IP address.  &#39;9&#39;: The Merchant is not active.  &#39;10&#39;: Exceeded the maximum allowed number of failed transactions.  &#39;11&#39;: Exceeded the maximum allowed number of transactions without authorizations.  &#39;12&#39;: Refund transaction does not have an associated sale transaction.  &#39;13&#39;: Exceeded the maximum number of refund transactions that do not have associated sale Transactions.  &#39;14&#39;: Exceeded the maximum authorized value for Transactions with failed authorizations. | [optional] 
**Score** | **int?** | The score for this Transaction against the specific type of check.  This field is specified as an integer between 0 and 65535. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

