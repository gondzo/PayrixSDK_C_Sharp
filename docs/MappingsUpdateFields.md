# IO.Swagger.Model.MappingsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**Name** | **string** |  | [optional] 
**Description** | **string** | A description of this Mapping. | [optional] 
**Input** | **string** | A JSON document that describes the input fields that should be mapped.  This field is stored as a text string and must be between 1 and 5000 characters long. | [optional] 
**Output** | **string** | A JSON document that describes the fields that should appear in the output, after the input fields have been mapped.  This field is stored as a text string and must be between 1 and 5000 characters long. | [optional] 
**_Namespace** | **string** | A valid URL that represents the XML namespace of the input and output documents.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

