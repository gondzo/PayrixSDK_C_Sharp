# IO.Swagger.Model.Body81
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Payment** | [**TokensidPayment**](TokensidPayment.md) |  | [optional] 
**Expiration** | **string** | The expiry month for the payment method associated with this Token.  This field is stored as a text string in &#39;MMYY&#39; format, where &#39;MM&#39; is the number of a month and &#39;YY&#39; is the last two digits of a year. For example, &#39;0623&#39; for June 2023.  The value must reflect a future date. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

