# IO.Swagger.Model.OrgFlowActionsUpdateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**OrgFlow** | **string** | The identifier of the orgFlow resource that this orgFlowActions resource is associated with. | [optional] 
**Org** | **string** |  | [optional] 
**Action** | **int?** | The action to take in relation to the Entity being processed.   Valid values are:  &#39;1&#39;: Add the referenced Entity to the referenced Org.  &#39;2&#39;: Remove the referenced Entity from the referenced Org. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

