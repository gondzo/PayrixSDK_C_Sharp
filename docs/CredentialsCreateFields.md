# IO.Swagger.Model.CredentialsCreateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Entity** | **string** | The identifier of the Entity that this Credential resource belongs to. | 
**Name** | **string** | The name of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Description** | **string** | A description of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Username** | **string** | The username to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long. | 
**Password** | **string** | The password to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long. | [optional] 
**ConnectUsername** | **string** | The username to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration. | [optional] 
**ConnectPassword** | **string** | The password to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration. | [optional] 
**Integration** | **string** |  | 
**Type** | **int?** | The type of action that this Credential is authorized to perform.  Valid values are:  &#39;1&#39;: Transaction - this Credential can be used to send Transactions to the processor.  &#39;2&#39;: Batch - this Credential can be used to send Batches to the processor for settlement.  &#39;3&#39;: Boarding - this Credential can be used to board a Merchant with the processor.  &#39;4&#39;: Payout - this Credential can be used to send out a Payout instruction.  &#39;5&#39;: Chargeback - this Credential can be used to retrieve or update a Chargeback with the processor.  &#39;6&#39;: Report - this Credential can be used to receive reports from the processor.  &#39;7&#39;: Account - this Credential can be used to verify a bank account when you use it with a bank account verification Integration.  &#39;8&#39;: Verification - this Credential can be used to verify data about a Merchant when you use it with a merchant verification integration. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

