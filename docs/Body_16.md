# IO.Swagger.Model.Body16
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The identifier of the Login resource that this confirmation code relates to. | 
**Type** | **int?** | The type of this confirmCode.  Valid values are:  &#39;1&#39;: A confirmation code related to a &#39;forgotten password&#39; request.  &#39;2&#39;: A confirmation code related to an email address verification request | 
**Email** | **string** | If the &#39;type&#39; of this confirmation code is &#39;2&#39; (email), then this field represents the email address that requires verification. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

