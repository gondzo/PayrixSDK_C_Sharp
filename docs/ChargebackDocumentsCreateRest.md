# IO.Swagger.Model.ChargebackDocumentsCreateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Chargeback** | **string** | The identifier of the Chargeback resource that this chargebackDocument relates to. | 
**_Ref** | **string** | The ref of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.   The value is set when the file is properly integrated, otherwise will be null. | [optional] 
**Description** | **string** | The description of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Status** | **int?** | The current status of the chargebackDocument.  Valid values are:  &#39;0&#39;: Created. The ChargebackDocument has been created.  &#39;1&#39;: Processed. The ChargebackDocument integration has been successful.  &#39;2&#39;: Failed. The ChargebackDocument integration has failed. | 
**Type** | **string** | The type of the file that holds this chargebackDocument.   Valid values are:  &#39;jpg&#39;, &#39;jpeg&#39;, &#39;gif&#39;, &#39;png&#39;, &#39;pdf&#39;, &#39;tif&#39;, &#39;tiff&#39;.   The value is set when the file is properly integrated, otherwise will be null. | [optional] 
**Name** | **string** | The name of this chargebackDocument.  This field is stored as a text string and must be between 1 and 100 characters long.   The value is set when the file is created and properly integrated. Holds the real file name used by the user. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

