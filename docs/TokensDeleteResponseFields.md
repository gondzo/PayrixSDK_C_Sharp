# IO.Swagger.Model.TokensDeleteResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Customer** | **string** | The Customer that this Token is associated with. | [optional] 
**Payment** | **string** | The payment method that is associated with this Token. | [optional] 
**Token** | **string** | The auto-generated token identifier. | [optional] 
**Expiration** | **string** | The expiry month for the payment method associated with this Token.  This field is stored as a text string in &#39;MMYY&#39; format, where &#39;MM&#39; is the number of a month and &#39;YY&#39; is the last two digits of a year. For example, &#39;0623&#39; for June 2023.  The value must reflect a future date. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

