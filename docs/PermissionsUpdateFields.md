# IO.Swagger.Model.PermissionsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The identifier of the Login resource that owns this Permission. | 
**Fromlogin** | **string** | If you are delegating Permissions from a Login that you own, then this field stores the identifier of the Login resource whose access you want to delegate. | [optional] 
**Tologin** | **string** | If you are delegating Permissions to a Login, then this field stores the identifier of the Login resource that should be granted the Permission. | [optional] 
**Fromorg** | **string** | If you are delegating Permissions from an Org that you own, then this field stores the identifier of the Org resource whose access you want to delegate. | [optional] 
**Toorg** | **string** | If you are delegating Permissions to an Org, then this field stores the identifier of the Org resource that should be granted the Permission. | [optional] 
**Resource** | **int?** |  | 
**View** | **int?** | The level of access to delegate to the target Login or Org to view resources of this type.  Valid values are:  &#39;0&#39;: No delegation. Do not modify the privileges of the target Entity or Org.  &#39;1&#39;: Allow. Grant this level of access.   &#39;2&#39;: Deny. Prevent this level of access. | 
**Add** | **int?** | The level of access to delegate to the target Login or Org to add resources of this type.  Valid values are:  &#39;0&#39;: No delegation. Do not modify the privileges of the target Entity or Org.  &#39;1&#39;: Allow. Grant this level of access.   &#39;2&#39;: Deny. Prevent this level of access. | 
**Edit** | **int?** | The level of access to delegate to the target Login or Org to edit resources of this type.  Valid values are:  &#39;0&#39;: No delegation. Do not modify the privileges of the target Entity or Org.  &#39;1&#39;: Allow. Grant this level of access.   &#39;2&#39;: Deny. Prevent this level of access. | 
**Destroy** | **int?** | The level of access to delegate to the target Login or Org to delete resources of this type.  Valid values are:  &#39;0&#39;: No delegation. Do not modify the privileges of the target Entity or Org.  &#39;1&#39;: Allow. Grant this level of access.   &#39;2&#39;: Deny. Prevent this level of access. | 
**Reference** | **int?** | The level of access to delegate to the target Login or Org to reference resources of this type in other resources.  Valid values are:  &#39;0&#39;: No delegation. Do not modify the privileges of the target Entity or Org.  &#39;1&#39;: Allow. Grant this level of access.   &#39;2&#39;: Deny. Prevent this level of access. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

