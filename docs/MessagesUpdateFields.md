# IO.Swagger.Model.MessagesUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MessageThread** | **string** | The identifier of the messageThreads that owns this Messages resource. | 
**OpposingMessage** | **string** |  | [optional] 
**Type** | **int?** | Whether this resource is incoming or outgoing. By default, an outgoing message is assigned a &#39;2&#39; and incoming messages is assigned a &#39;1&#39;. | 
**Generated** | **int?** | Whether this resource was automatically generated or not. A value of &#39;1&#39; means the message was auomatically generated and a value of &#39;0&#39; means it was manually generated. | 
**Secure** | **int?** | Whether this resource is marked as secure. A value of &#39;1&#39; means messages will protected in email notifications and a value of &#39;0&#39; means the message will display entirely. | 
**Read** | **int?** | Whether this resource is marked as read. A value of &#39;1&#39; means the message has been read and a value of &#39;0&#39; means the message has not been read yet | 
**Message** | **string** | Free-form text for adding a message to a messageThread resource. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

