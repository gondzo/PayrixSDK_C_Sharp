# IO.Swagger.Model.AccountsaccountRequestFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Method** | **int?** | The type of the Account.  This field is specified as an integer.  Valid values are:  &#39;8&#39;: Checking account  &#39;9&#39;: Savings account  &#39;10&#39;: Corporate checking account and  &#39;11&#39;: Corporate savings account | 
**Number** | **string** | The number of the bank account. | 
**Routing** | **string** | The routing number of the bank account. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

