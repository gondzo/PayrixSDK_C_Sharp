# IO.Swagger.Model.InlineResponse2003ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The Login that owns this Adjustment. | [optional] 
**Entity** | **string** | The Entity that will receive the set amount. | [optional] 
**Fromentity** | **string** | The Entity that will pay for the adjustment.  This is an optional field and should only be used when the amount is being transfered from the fromentity to the entity. | [optional] 
**Description** | **string** | A description of the Adjustment. | [optional] 
**Amount** | **string** | The amount of the Adjustment. | [optional] 
**Currency** | **string** | The currency of the amount.  Currently, this field only accepts the value &#39;USD&#39;. | [optional] 
**Platform** | **string** | The platform used for the adjustment.  This field is required if the adjustment is not between entities. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

