# IO.Swagger.Model.IplistsUpdateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The Login that owns this resource. | [optional] 
**Version** | **int?** | Whether this IP List contains IPv4 addresses (for example, 198.51.100.113) or IPv6 addresses (for example, 2001:0db8:85a3:0000:0000:8a2e:0370:7334).  You can only use one type in each IP List resource. | [optional] 
**Type** | **int?** | The type of this IP List.  This field is specified as an integer.  Valid values are:  &#39;0&#39;: Blacklisted IP address range  &#39;1&#39;: Whitelisted IP address range | [optional] 
**Start** | **string** | The lowest IP address that should be included in this IP List.  The valid data type for this field depends on whether the IP list is set to be an IPv4 or IPv6 list in the &#39;type&#39; field.  For an IPv4 list, only IPv4 addresses such as 198.51.100.113 are permitted. For an IPv6 list, only IPv6 addresses such as 2001:0db8:85a3:0000:0000:8a2e:0370:7334 are permitted. | [optional] 
**Finish** | **string** | The highest IP address that should be included in this IP List.  The valid values for this field depend on whether the IP list is set to be an IPv4 or IPv6 list in the &#39;type&#39; field.  For an IPv4 list, only IPv4 addresses, such as &#39;198.51.100.113&#39;, are permitted. For an IPv6 list, only IPv6 addresses, such as &#39;2001:0db8:85a3:0000:0000:8a2e:0370:7334&#39; are permitted. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

