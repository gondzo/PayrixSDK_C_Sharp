# IO.Swagger.Model.InlineResponse20044ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Forlogin** | **string** | The identifier of the Login resource that should be marked as part of the Org identified in the &#39;org&#39; field. | [optional] 
**Org** | **string** | The identifier of the Org resource that the Login identified in the &#39;forlogin&#39; field should be marked as part of. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

