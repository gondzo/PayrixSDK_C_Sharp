# IO.Swagger.Model.Body28
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**Fund** | **string** | The identifier of the Fund that this entityReserves resource relates to. | 
**Total** | **int?** | The amount held in this entityReserve.  This field is specified as an integer in cents. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

