# IO.Swagger.Model.Body11
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Merchant** | **string** | The identifier of the Merchant that is associated with this Batch. | 
**Status** | **int?** | The current status of this Batch.  Valid values are:  &#39;0&#39;: Open - This Batch can accept more Transactions.  &#39;1&#39;: Closed - this Batch is closed to new Transactions and is ready to be sent to the processor.  &#39;2&#39;: Processing - this Batch is being processed for settlement.  &#39;3&#39;: Processed - this Batch has been processed. | 
**ClientRef** | **string** | The merchant&#39;s reference code of the batch.  This field is stored as a text string and must be between 0 and 50 characters long. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

