# IO.Swagger.Model.InlineResponse200ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Entity** | **string** | The identifier of the Entity associated with this Account. | [optional] 
**Account** | **string** | An object representing details of the Account, including the type of Account (method), Account number and routing code. | [optional] 
**Token** | **string** | A unique token that can be used to refer to this Account in other parts of the API. | [optional] 
**Name** | **string** | A client-supplied name for this bank account. | [optional] 
**Description** | **string** | A client-supplied description for this bank account. | [optional] 
**Primary** | **int?** | Indicates whether the Account is the &#39;primary&#39; Account for the associated Entity.  Only one Account associated with each Entity can be the &#39;primary&#39; Account.  A value of &#39;1&#39; means the Account is the primary and a value of &#39;0&#39; means the Account is not the primary. | [optional] 
**Status** | **int?** | The status of the Account.  Valid values are:  &#39;0&#39;: Not Ready. The account holder is not yet ready to verify the Account.  &#39;1&#39;: Ready. The account is ready to be verified.  &#39;2&#39;: Challenged - the account has processed the challenge.  &#39;3&#39;: Verified. The Account has been verified.  &#39;4&#39;: Manual. There has been an issue during verification and further attempts to verify the Account will require manual intervention. | [optional] 
**Currency** | **string** | The currency of this Account.  Currently, this field only accepts the value &#39;USD&#39;. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

