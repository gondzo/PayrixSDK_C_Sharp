# IO.Swagger.Model.SubscriptionTokensCreateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Subscription** | **string** | The identifier of the Subscription resource that this Subscription Token is associated with.  This is the Subscription that the Token identified in the &#39;token&#39; field of this resource is responsible for paying for. | 
**Token** | **string** | The identifier of the Token resource that this Subscription Token is associated with.  This resource identifies the means of payment for the Subscription identified in the &#39;subscription&#39; field of this resource. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

