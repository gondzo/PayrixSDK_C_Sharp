# IO.Swagger.Model.Body5
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**Forlogin** | **string** | The identifier of the Login that this Alert relates to.  The Alert is triggered based on the activity of this Login. | [optional] 
**Fororg** | **string** | The identifier of the Org that this Alert relates to.  The Alert is triggered based on the activity of this Org. | [optional] 
**Name** | **string** | The name of this Alert.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Description** | **string** |  | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

