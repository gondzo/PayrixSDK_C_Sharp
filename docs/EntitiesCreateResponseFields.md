# IO.Swagger.Model.EntitiesCreateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**IpCreated** | **string** | The incoming ip address from which this Entity was created. | [optional] 
**IpModified** | **string** | The incoming ip address from which this Entity was last modified. | [optional] 
**ClientIp** | **string** | The client ip address from which the Entity was created.  Valid values are any Ipv4 or Ipv6 address. | [optional] 
**Login** | **string** | The Login that owns this resource. | [optional] 
**_Parameter** | **string** | The parameter associated with this Entity. | [optional] 
**Type** | **int?** | The type of Entity.  This field is specified as an integer.  Valid values are &#39;0&#39; (sole proprietor), &#39;1&#39; (corporation), &#39;2&#39; (limited liability company), &#39;3&#39; (partnership), &#39;4&#39; (association), &#39;5&#39; (non-profit organization) and &#39;6&#39; (government organization). | [optional] 
**Name** | **string** | The name of this Entity.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Address1** | **string** | The first line of the address associated with this Entity.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Address2** | **string** | The second line of the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**City** | **string** | The name of the city in the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**State** | **string** | The U.S. state associated with this Entity.  Valid values are any U.S. state&#39;s 2 character postal abbreviation. | [optional] 
**Zip** | **string** | The ZIP code in the address associated with this Entity.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**Country** | **string** | The country in the address associated with the Entity. Currently, this field only accepts the value &#39;USA&#39;. | [optional] 
**Timezone** | **int?** |  | [optional] 
**Phone** | **string** | The phone number associated with this Entity.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Fax** | **string** | The fax number associated with this Entity.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Email** | **string** | The email address associated with this Entity.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Website** | **string** | The web site URL associated with this Entity.  This field is stored as a text string and must be between 0 and 50 characters long. | [optional] 
**Ein** | **string** | The IRS Employer ID (EID) number for the Entity. | [optional] 
**TcVersion** | **string** |  | [optional] 
**TcDate** | **DateTime?** |  | [optional] 
**TcIp** | **string** |  | [optional] 
**TcAcceptDate** | **int?** |  | [optional] 
**TcAcceptIp** | **string** |  | [optional] 
**Currency** | **string** | The currency of this Entity.  Currently, this field only accepts the value &#39;USD&#39;. | [optional] 
**Custom** | **string** | Custom, free-form field for client-supplied text. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

