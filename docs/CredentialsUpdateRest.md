# IO.Swagger.Model.CredentialsUpdateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Description** | **string** | A description of this Credential resource.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Username** | **string** | The username to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long. | 
**Password** | **string** | The password to use when authenticating to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long. | [optional] 
**ConnectUsername** | **string** | The username to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration. | [optional] 
**ConnectPassword** | **string** | The password to use when connecting to the integration associated with this Credential resource.  This field is stored as a text string and must be between 1 and 50 characters long.  This field is only necessary when it is required by the integration. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

