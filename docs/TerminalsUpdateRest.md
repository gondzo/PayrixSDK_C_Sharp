# IO.Swagger.Model.TerminalsUpdateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **int?** | The type of terminal.  This field is stored as an integer and must be between 1 and 417. | 
**Environment** | **int?** | How is the terminal employed.  Valid values are:  &#39;1&#39;: Retail.  &#39;2&#39;: Retail with tips.  &#39;3&#39;: Restaurant.  &#39;4&#39;: Lodging.  &#39;5&#39;: Bar.  &#39;6&#39;: Cash Advance.  &#39;7&#39;: Mail/Telephone Order.  &#39;8&#39;: Pay At The Pump.  &#39;9&#39;: Service Rest.  &#39;10&#39;: E-commerce.  &#39;11&#39;: Direct Marketing.  &#39;12&#39;: Fine Dining.  &#39;13&#39;: Gift Card Only. | [optional] 
**AutoClose** | **int?** | If the terminal should be manually or automatically closed for the day.  Valid values are:  &#39;0&#39;: Automatic.  &#39;1&#39;: Manual. | 
**AutoCloseTime** | **int?** | The time when the terminal should be automatically closed for the day. This field is only required when AutoClose is set to automatic.  The format should be HHMM (1145, 2200, etc). | 
**Name** | **string** | The name of this Terminal.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Description** | **string** | A description of the Terminal.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Address1** | **string** | The first line of the address associated with this Terminal&#39;s location.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Address2** | **string** | The second line of the address associated with this Terminal&#39;s location.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**City** | **string** | The name of the city in the address associated with this Terminal&#39;s location.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**State** | **string** | The U.S. state in the address associated with this Terminal&#39;s location. Valid values are: AL, AK, AZ, AR, CA, CO, CT, DE, DC, FL, GA, HI, ID, IL, IN, IA, KS, KY, LA, ME, MD, MA, MI, MN, MS, MO, MT, NE, NV, NH, NJ, NM, NY, NC, ND, OH, OK, OR, PA, RI, SC, SD, TN, TX, UT, VT, VA, WA, WV, WI and WY. | [optional] 
**Zip** | **string** | The ZIP code in the address associated with this Terminal&#39;s location.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**Country** | **string** | The country in the address associated with the Terminal&#39;s location. Currently, this field only accepts the value &#39;USA&#39;. | [optional] 
**Timezone** | **int?** |  | [optional] 
**Status** | **int?** | The current status of the terminal.  Valid values are:  &#39;0&#39;: Inactive.  &#39;1&#39;: Active.  This field is optional and is set to Active by default. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

