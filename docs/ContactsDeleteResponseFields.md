# IO.Swagger.Model.ContactsDeleteResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Entity** | **string** | The identifier of the Entity that this Contact relates to. | [optional] 
**First** | **string** | The first name associated with this Contact. | [optional] 
**Middle** | **string** | The middle name associated with this Contact. | [optional] 
**Last** | **string** | The last name associated with this Contact. | [optional] 
**Description** | **string** | A description of this Contact. | [optional] 
**Email** | **string** | The email address of this Contact. | [optional] 
**Fax** | **string** | The fax number associated with this Contact.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Phone** | **string** | The phone number associated with this Contact.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Country** | **string** | The country associated with this Contact.  Valid values for this field is the 3-letter ISO code for the country. | [optional] 
**Zip** | **string** | The ZIP code in the address associated with this Contact.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**State** | **string** | The state associated with this Contact.  If in the U.S. this is specified as the 2 character postal abbreviation for the state, if outside of the U.S. the full state name.  This field is stored as a text string and must be between 2 and 100 characters long. | [optional] 
**City** | **string** | The name of the city in the address associated with this Contact.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**Address2** | **string** | The second line of the address associated with this Contact.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**Address1** | **string** | The first line of the address associated with this Contact.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

