# IO.Swagger.Model.OrgFlowsUpdateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The Login that owns this resource. | [optional] 
**Forlogin** | **string** | The identifier of the Login resource for which this orgFlows resource is triggered. | [optional] 
**Recursive** | **int?** | Whether this resource should affect logins recursively - in other words, affect this Login and all its child Logins.  Valid values are:  &#39;0&#39;: Not recursive. The orgFlow only affects the Login identified in the &#39;forLogin&#39; field.  &#39;1&#39;: Recursive. The orgFlow affects the Login identified in the &#39;forLogin&#39; field and all its child Logins. | [optional] 
**Trigger** | **int?** | This field sets the trigger that determines when this orgFlow runs.   Valid values are:  &#39;1&#39;: Trigger at Merchant creation time.  &#39;2&#39;: Trigger when a Merchant check returns a low score.  &#39;3&#39;: Trigger when a Merchant check returns a high score.  &#39;4&#39;: Trigger at Merchant boarding time. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

