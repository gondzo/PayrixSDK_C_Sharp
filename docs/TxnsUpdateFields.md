# IO.Swagger.Model.TxnsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Batch** | **string** | If the Transaction is linked to a Batch, this field specifies the identifier of the Batch. | [optional] 
**Expiration** | **string** | The expiration date of this Transaction.  This field is stored as a text string in &#39;MMYY&#39; format, where &#39;MM&#39; is the number of a month and &#39;YY&#39; is the last two digits of a year. For example, &#39;0623&#39; for June 2023.  The value must reflect a future date. | [optional] 
**AuthDate** | **int?** | The date on which the Transaction was authorized.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016.  The value of this field must represent a date in the past. | [optional] 
**AuthCode** | **string** | The authorization code for this Transaction.  This field is stored as a text string and must be between 0 and 20 characters long. | [optional] 
**Settled** | **int?** | A date indicating when this Transaction was settled.  This field is set automatically. | [optional] 
**SettledCurrency** | **string** | The currency of the settled total.  This field is set automatically. | [optional] 
**SettledTotal** | **int?** | The total amount that was settled.   This field is specified as an integer in cents and is set automatically. | [optional] 
**AllowPartial** | **int?** | Whether to allow partial amount authorizations of this Transaction.  For example, if the transaction amount is $1000 and the processor only authorizes a smaller amount, then enabling this field  lets the Transaction proceed anyway.  A value of &#39;1&#39; means that partial amount authorizations are allowed and a value of &#39;0&#39; means that partial amount authorizations are not allowed. | 
**ClientIp** | **string** | The client ip address from which the Transaction was created.  Valid values are any Ipv4 or Ipv6 address. | [optional] 
**Reserved** | **int?** | Indicates whether the Transaction is reserved and the action that will be taken as a result.  This field is specified as an integer.  Valid values are:  &#39;0&#39;: Not reserved  &#39;1&#39;: If the Transaction is a sale or authorization, then block the capture of the Transaction.  &#39;2&#39;: Apply a manual override to any checks on the Transaction and allow it to proceed and  &#39;3&#39;: Move all funds from this Transaction into a reserve. | [optional] 
**CheckStage** | **string** | The last transaction stage check for risk. | [optional] 
**Status** | **int?** | The status of the Transaction. Valid values are &#39;0&#39; (pending), &#39;1&#39; (approved), &#39;2&#39; (failed), &#39;3&#39; (captured), &#39;4&#39; (settled) and &#39;5&#39; (returned). | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

