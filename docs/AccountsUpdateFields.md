# IO.Swagger.Model.AccountsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Account** | [**AccountsidAccount**](AccountsidAccount.md) |  | [optional] 
**Name** | **string** | A client-supplied name for this bank account. | [optional] 
**Description** | **string** | A client-supplied description for this bank account. | [optional] 
**Primary** | **int?** | Indicates whether the Account is the &#39;primary&#39; Account for the associated Entity.  Only one Account associated with each Entity can be the &#39;primary&#39; Account.  A value of &#39;1&#39; means the Account is the primary and a value of &#39;0&#39; means the Account is not the primary. | 
**Status** | **int?** | The status of the Account.  Valid values are:  &#39;0&#39;: Not Ready. The account holder is not yet ready to verify the Account.  &#39;1&#39;: Ready. The account is ready to be verified.  &#39;2&#39;: Challenged - the account has processed the challenge.  &#39;3&#39;: Verified. The Account has been verified.  &#39;4&#39;: Manual. There has been an issue during verification and further attempts to verify the Account will require manual intervention. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

