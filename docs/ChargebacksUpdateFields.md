# IO.Swagger.Model.ChargebacksUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Txn** | **string** |  | [optional] 
**Merchant** | **string** |  | 
**Mid** | **string** |  | 
**Total** | **int?** |  | 
**RepresentedTotal** | **int?** |  | [optional] 
**Description** | **string** |  | [optional] 
**Currency** | **string** |  | 
**_Ref** | **string** |  | 
**Cycle** | **int?** |  | [optional] 
**Reason** | **string** |  | [optional] 
**ReasonCode** | **string** |  | [optional] 
**Issued** | **int?** |  | [optional] 
**Received** | **int?** |  | [optional] 
**Reply** | **int?** |  | [optional] 
**BankRef** | **string** |  | [optional] 
**ChargebackRef** | **string** |  | [optional] 
**Status** | **int?** |  | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

