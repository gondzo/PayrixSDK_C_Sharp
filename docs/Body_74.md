# IO.Swagger.Model.Body74
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**_Public** | **int?** | Whether this Session should have access to only public resources.  A value of &#39;1&#39; means that the Session can only access public resources and a value of &#39;0&#39; means that the Session can also access private resources.  Public resources include Transactions, Tokens, Customers and Items. All other resources are private. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

