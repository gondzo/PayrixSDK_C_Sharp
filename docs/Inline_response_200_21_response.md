# IO.Swagger.Model.InlineResponse20021Response
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**List&lt;InlineResponse20021ResponseData&gt;**](InlineResponse20021ResponseData.md) | The data object that the API returns for this request. | [optional] 
**Details** | [**InlineResponse200ResponseDetails**](InlineResponse200ResponseDetails.md) |  | [optional] 
**Errors** | [**List&lt;InlineResponse200ResponseErrors&gt;**](InlineResponse200ResponseErrors.md) | An array of zero or more errors that occurred when the API processed the request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

