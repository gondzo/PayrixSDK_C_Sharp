# IO.Swagger.Model.Body35
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**Type** | **int?** | The type of this IP List.  This field is specified as an integer.  Valid values are:  &#39;0&#39;: Blacklisted IP address range  &#39;1&#39;: Whitelisted IP address range | 
**Start** | **string** | The lowest IP address that should be included in this IP List.  The valid data type for this field depends on whether the IP list is set to be an IPv4 or IPv6 list in the &#39;type&#39; field.  For an IPv4 list, only IPv4 addresses such as 198.51.100.113 are permitted. For an IPv6 list, only IPv6 addresses such as 2001:0db8:85a3:0000:0000:8a2e:0370:7334 are permitted. | 
**Finish** | **string** | The highest IP address that should be included in this IP List.  The valid values for this field depend on whether the IP list is set to be an IPv4 or IPv6 list in the &#39;type&#39; field.  For an IPv4 list, only IPv4 addresses, such as &#39;198.51.100.113&#39;, are permitted. For an IPv6 list, only IPv6 addresses, such as &#39;2001:0db8:85a3:0000:0000:8a2e:0370:7334&#39; are permitted. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

