# IO.Swagger.Model.InlineResponse2002ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Account** | **string** | The identifier of the Account that you want to verify. | [optional] 
**Type** | **int?** | The type of account verification you want to perform.  Valid values are:  &#39;1&#39;: Debit amount verification  &#39;2&#39;: Credential verification. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

