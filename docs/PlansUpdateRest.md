# IO.Swagger.Model.PlansUpdateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of this Plan.  This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Description** | **string** | A description of this Plan.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Schedule** | **int?** |  | 
**ScheduleFactor** | **int?** |  | 
**Amount** | **int?** | The amount to charge with each payment under this Plan.  This field is specified as an integer in cents. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

