# IO.Swagger.Model.MessageThreadsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The identifier of the Login that owns this messageThreads resource. | 
**Forlogin** | **string** | The identifier of the receiving Login of this messageThreads resource. | [optional] 
**TxnHold** | **string** | The identifier of the TxnHold that is related to this messageThread | [optional] 
**OpposingMessageThread** | **string** |  | [optional] 
**Folder** | **string** | Free-form text. By default, a messageThread resource is set as &#39;default&#39;. | 
**Sender** | **string** | Free-form text that represents the name of the sender of a messageThread resource. | 
**Recipient** | **string** | Free-form text that represents the name of the recipient of a messageThread resource. | 
**Subject** | **string** | Free-form text for adding a subject to a messageThread resource. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

