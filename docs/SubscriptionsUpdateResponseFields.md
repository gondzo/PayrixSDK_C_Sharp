# IO.Swagger.Model.SubscriptionsUpdateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Plan** | **string** | The identifier of the Plan that this Subscription is associated with.  The Plan determines the frequency and amount of each payment. | [optional] 
**Start** | **int?** | The date on which the Subscription should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016.  The value of this field must represent a date in the future. | [optional] 
**Finish** | **int?** | The date on which the Subscription should finish.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016.  The value of this field must represent a date in the future. | [optional] 
**Tax** | **int?** | The amount of the total sum of this Subscription that is made up of tax.  This field is specified as an integer in cents. | [optional] 
**Descriptor** | **string** | The descriptor used in this Subscription.   This field is stored as a text string and must be between 1 and 50 characters long. If a value is not set, an attempt is made to set a default value from the merchant information. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

