# IO.Swagger.Model.Body9
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The login under which to perform this action. If you do not supply a login in this field, then the API defaults to the login that is currently authenticated. | 
**Name** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**_Public** | **int?** | Whether this API key should have access to only public resources.  A value of &#39;1&#39; means that the key can only access public resources and a value of &#39;0&#39; means that the key can also access private resources.  Public resources include Transactions, Tokens, Customers and Items. All other resources are private. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

