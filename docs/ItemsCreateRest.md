# IO.Swagger.Model.ItemsCreateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Txn** | **string** |  | 
**Item** | **string** |  | 
**Description** | **string** | A description of this Item.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Custom** | **string** | A custom identifier for this line Item, such as a stock number or order code. | [optional] 
**Quantity** | **int?** | The quantity of this Item included in the Transaction.  This field is specified as an integer. | 
**Price** | **int?** | The amount charged for this Item.  This field is specified as an integer in cents. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

