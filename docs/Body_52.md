# IO.Swagger.Model.Body52
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Org** | **string** | The identifier of the Org that this orgEntity is associated with. | 
**Entity** | **string** | The identifier of the Entity that this orgEntity is associated with. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

