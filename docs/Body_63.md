# IO.Swagger.Model.Body63
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**PayoutLogin** | **string** | The Login that will own the Payout resource. When set to null, the Payout resource will be owned by the triggerring Entity. | [optional] 
**Org** | **string** | The identifier of the Org that this payoutFlows resource applies to.  If you set this field, then the payoutFlow applies to all Entities in the Org. | [optional] 
**Entity** | **string** | The identifier of the Entity that this payoutFlow applies to. | [optional] 
**Trigger** | **int?** | The event on the Org or Entity that should trigger the creation of an associated Payout resource.  Valid values are:  &#39;1&#39;: Trigger the creation of the Payout when the primary bank account is associated.  &#39;2&#39;: Trigger the creation of the Payout when the Merchant is boarded. | 
**Schedule** | **int?** | The schedule that determines when the Payout resource that is created should be triggered to be paid.  Valid values are:  &#39;1&#39;: Daily - the Payout is paid every day.  &#39;2&#39;: Weekly - the Payout is paid every week.  &#39;3&#39;: Monthly - the Payout is paid every month.  &#39;4&#39;: Annually - the Payout is paid every year.  &#39;5&#39;: Single - the Payout is a one-off payment. | 
**ScheduleFactor** | **int?** | A multiplier that you can use to adjust the schedule set in the &#39;schedule&#39; field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This affects the Payout resource that is created by this payoutFlow.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if &#39;schedule&#39; is set to &#39;1&#39; (meaning &#39;daily&#39;), then a &#39;scheduleFactor&#39; value of &#39;2&#39; would cause the Payout to trigger every two days. | 
**Um** | **int?** | The unit of measure for the Payout resource that is created.  Valid values are:  &#39;1&#39;: Percentage - the Payout is a percentage of the current available funds for this Entity that should be paid to their Account, specified in the &#39;amount&#39; field in basis points. &#39;2&#39;: Actual - the Payout is a fixed amount, specified in the &#39;amount&#39; field as an integer in cents.  &#39;3&#39;: Negative percentage - the Payout is a percentage of the balance, specified in the &#39;amount&#39; field as a negative integer in basis points. The direction of the Payout payment is reversed. For example, if the Entity has a negative balance of $10 and the amount is set to 10000 (100%), then $10 will be drawn from their account to fully replenish the balance to $0. | 
**Amount** | **int?** |  | 
**Minimum** | **int?** |  | [optional] 
**PayoutInactive** | **int?** | Whether the Payout resource will be marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**SkipOffDays** | **int?** | Whether the Payout resource will be marked to skip the creation of disbursements on holidays and weekends. A value of &#39;1&#39; means skip and a value of &#39;0&#39; means do not skip. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

