# IO.Swagger.Model.TxnHoldNotesQueryResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**TxnHold** | **string** | The identifier of the TxnHold that owns this txnHoldNotes resource. | [optional] 
**Note** | **string** | Free-form text for adding a message along with the action. | [optional] 
**Action** | **int?** | The desired action to take on the referenced TxnHold.  This field is specified as an integer.  Valid values are:  &#39;0&#39;: Note. Just add a note to the txnHold. &#39;1&#39;: Release. Release the hold for this TxnHold. &#39;2&#39;: Hold. If the txnHold was released, this will allow resetting the hold. &#39;3&#39;: Review. Mark the txnHold as having been reviewed.  &#39;4&#39;: Re-Review. If the txnHold was marked as reviewed, this will allow resetting the review. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

