# IO.Swagger.Model.AlertActionsCreateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Alert** | **string** | The identifier of the Alert resource that defines this alertAction. | 
**Type** | **string** | The medium to use to deliver this Alert.  Valid values are:  &#39;email&#39;: Deliver the Alert to an email address.  &#39;web&#39;: Deliver the Alert through a web site notification.  &#39;app&#39;: Deliver the Alert through a mobile application notification.  &#39;sms&#39;: Deliver the Alert through an SMS message to a mobile device. | 
**Options** | **string** | When the &#39;type&#39; field of this resource is set to &#39;web&#39;, this field determines the format that the Alert data should be sent in.   Valid values are:  &#39;JSON&#39;: JSON document serialization.  &#39;XML&#39;: XML document serialization.  &#39;SOAP&#39;: SOAP XML document serialization.  &#39;FORM&#39;:HTML form data serialization. | 
**Value** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

