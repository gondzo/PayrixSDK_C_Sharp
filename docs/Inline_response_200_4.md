# IO.Swagger.Model.InlineResponse2004
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Response** | [**InlineResponse2004Response**](InlineResponse2004Response.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

