# IO.Swagger.Model.TxnsPayment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Method** | **int?** | The payment method for thei Transaction.  This field is specified as an integer.  Valid values are:  &#39;1&#39;: American Express  &#39;2&#39;: Visa  &#39;3&#39;: MasterCard  &#39;4&#39;: Diners Club  &#39;5&#39;: Discover  &#39;6&#39;: PayPal  &#39;7&#39;: Debit card  &#39;8&#39;: Checking account  &#39;9&#39;: Savings account  &#39;10&#39;: Corporate checking account and  &#39;11&#39;: Corporate savings account  &#39;12&#39;: Gift card  &#39;13&#39;: EBT  &#39;14&#39;:WIC. | 
**Number** | **string** | The card number of the credit card associated with this Transaction. | 
**Routing** | **string** | The routing code for the eCheck or bank account payment associated with this Transaction. | [optional] 
**Expiration** | **string** | The expiration date of the credit card associated with this Transaction.  This field is stored as a text string in &#39;MMYY&#39; format, where &#39;MM&#39; is the number of a month and &#39;YY&#39; is the last two digits of a year. For example, &#39;0623&#39; for June 2023. | [optional] 
**Cvv** | **int?** | The Card Verification Value (CVV) number of the credit card associated with this Transaction.  This field is expressed as an integer. | [optional] 
**Track** | **string** | The &#39;Track&#39; data (either Track 1, Track 2, or both) of the card associated with this Transaction.  This field is stored as a text string. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

