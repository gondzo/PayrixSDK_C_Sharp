# IO.Swagger.Model.MerchantsQueryResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**LastActivity** | **DateTime?** | The date and time on which this Merchant last processed a Transaction. | [optional] 
**Entity** | **string** | The Entity associated with this Merchant. | [optional] 
**Dba** | **string** | The name under which the Merchant is doing business, if applicable. | [optional] 
**_New** | **int?** | An indicator that specifies whether the Merchant is new to credit card processing.  A value of &#39;1&#39; means new and a value of &#39;0&#39; means not new. By default, merchants are considered to be new. | [optional] 
**Established** | **int?** | The date on which the Merchant was established.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016. | [optional] 
**AnnualCCSales** | **int?** | The value of the annual credit card sales of this Merchant.  This field is specified as an integer in cents. | [optional] 
**AvgTicket** | **int?** |  | [optional] 
**Amex** | **string** | The American Express merchant identifier for this Merchant, if applicable. | [optional] 
**Discover** | **string** | The Discover merchant identifier for this Merchant, if applicable. | [optional] 
**Mcc** | **string** | The Merchant Category Code (MCC) for this Merchant. This code is not required to create a Merchant, but it is required to successfully board a Merchant. | [optional] 
**Status** | **int?** | The status of the Merchant. Valid values are &#39;0&#39; (not ready), &#39;1&#39; (ready), &#39;2&#39; (boarded), &#39;3&#39; (manual) and &#39;4&#39; (denied). | [optional] 
**Boarded** | **int?** | The date and time on which this Merchant was successfully boarded. | [optional] 
**TinStatus** | **int?** | The Tax ID (TIN) status of the merchant. Valid values are &#39;0&#39; (pending), &#39;1&#39; (valid) and &#39;2&#39; (invalid). | [optional] 
**TcVersion** | **int?** | An indicator showing the version of the terms and conditions that this Merchant has accepted. The API indicates the version as an integer. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

