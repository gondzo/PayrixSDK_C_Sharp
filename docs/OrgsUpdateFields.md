# IO.Swagger.Model.OrgsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this Org. | 
**Name** | **string** | The name of this Org.  This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Description** | **string** | A description of this Org.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

