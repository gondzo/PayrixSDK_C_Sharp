# IO.Swagger.Model.MembersUpdateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Merchant** | **string** | The identifier of the Merchant associated with this Member. | 
**Title** | **string** | The title that this Member holds in relation to the associated Merchant.  For example, &#39;CEO&#39;, &#39;Owner&#39; or &#39;Director of Finance&#39;. | [optional] 
**First** | **string** | The first name associated with this Member. | 
**Middle** | **string** | The middle name associated with this Member. | [optional] 
**Last** | **string** | The last name associated with this Member. | 
**Ssn** | **string** | The social security number of this Member. This field is required if the Merchant associated with the Member is a sole trader. | [optional] 
**Dob** | **int?** | The date of birth of this Member.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016. | 
**Dl** | **string** | The driver&#39;s license number of this Member. | [optional] 
**Dlstate** | **string** | The U.S. state where the driver&#39;s license of this Member was issued.  Valid values are any U.S. state&#39;s 2 character postal abbreviation. | [optional] 
**Email** | **string** | The email address of this Member. | 
**Ownership** | **int?** | The share of the Member&#39;s ownership of the associated Merchant, expressed in basis points.  For example, 25.3% is expressed as &#39;2530&#39;. | 
**Primary** | **int?** | Indicates whether the Member is the &#39;primary&#39; contact for the associated Merchant. Only one Member associated with each Merchant can be the &#39;primary&#39; Member.  A value of &#39;1&#39; means primary and a value of &#39;0&#39; means not primary. | 
**Address1** | **string** | The first line of the address associated with this Member.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Address2** | **string** | The second line of the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**City** | **string** | The name of the city in the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**State** | **string** | The U.S. state associated with this Member.  Valid values are any U.S. state&#39;s 2 character postal abbreviation. | [optional] 
**Zip** | **string** | The ZIP code in the address associated with this Member.  This field is stored as a text string and must be between 1 and 20 characters long. | [optional] 
**Country** | **string** | The country in the address associated with the Member. Currently, this field only accepts the value &#39;USA&#39;. | [optional] 
**Phone** | **string** | The phone number associated with this Member.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Fax** | **string** | The fax number associated with this Member.  This field is stored as a text string and must be between 10 and 15 characters long. | [optional] 
**Inactive** | **int?** | Whether this Member is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this Member should be marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

