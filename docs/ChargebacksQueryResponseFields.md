# IO.Swagger.Model.ChargebacksQueryResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Merchant** | **string** |  | [optional] 
**Txn** | **string** |  | [optional] 
**Mid** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Total** | **int?** |  | [optional] 
**RepresentedTotal** | **int?** |  | [optional] 
**Cycle** | **int?** |  | [optional] 
**Currency** | **string** |  | [optional] 
**_Ref** | **string** |  | [optional] 
**Reason** | **string** |  | [optional] 
**ReasonCode** | **string** |  | [optional] 
**Issued** | **int?** |  | [optional] 
**Received** | **int?** |  | [optional] 
**Reply** | **int?** |  | [optional] 
**BankRef** | **string** |  | [optional] 
**ChargebackRef** | **string** |  | [optional] 
**Status** | **int?** |  | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

