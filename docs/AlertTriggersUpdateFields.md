# IO.Swagger.Model.AlertTriggersUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Alert** | **string** | The identifier of the Alert resource that you want to invoke with this trigger. | 
**_Event** | **string** | The event type that triggers the associated Alert.  Valid values are:  &#39;create&#39;: Triggers when the associated  resource is created.  &#39;update&#39;: Triggers when the associated resource is updated.  &#39;delete&#39;: Triggers when the associated  resource is created.  &#39;ownership&#39;: Triggers when the ownership of the associated resource changes.  &#39;board&#39;: Triggers when a Merchant is boarded.  &#39;txnhold&#39;: Triggers when a transaction is held for review.  &#39;batch&#39;: Triggers when Transactions are captured in a batch.  &#39;account&#39;: Triggers when the Account associated with a Merchant is updated.  &#39;payout&#39;: Triggers when a Payout occurs.  &#39;fee&#39;: Triggers when an Entity is charged a Fee.  | 
**Resource** | **int?** |  | 
**Name** | **string** | The name of this alertTrigger.  This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Description** | **string** |  | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

