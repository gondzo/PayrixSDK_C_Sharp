# IO.Swagger.Model.DivisionsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**Name** | **string** |  | 
**Email** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

