# IO.Swagger.Model.MerchantChecksDeleteResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The identifier of the Login that owns this merchantChecks resource. | [optional] 
**Org** | **string** | If this merchantChecks resource relates to an Org, then this field stores the identifier of the Org. | [optional] 
**Entity** | **string** | If this merchantChecks resource relates to an Entity, then this field stores the identifier of the Entity. | [optional] 
**Type** | **int?** | The type of check to perform on the Merchant.   This field is specified as an integer.  Valid values are:  &#39;1&#39;: OFAC. Check the Merchant against the Specially Designated Nationals (SDN) list operated by the Office of Foreign Assets Control at the U.S. Department of the Treasury.  &#39;2&#39;: Check that the data held on the Entity that is associated with this Merchant matches the data retrieved from external data sources.  &#39;3&#39;: Check that the data held on the Owner (primary Member) that is associated with this Merchant matches the data retrieved from external data sources.  &#39;4&#39;: Check that the Member and Entity associated with this account are have a genuine association, as determined by consulting external data sources. | [optional] 
**Trigger** | **int?** | The event that triggers this txnCheck on the Merchant.  This field is specified as an integer.  Valid values are:  &#39;1&#39;: Preboard. Check the Merchant before they are boarded.  &#39;2&#39;: Check the Merchant after they are boarded.  &#39;3&#39;: Check the Merchant when they process a Transaction.  &#39;4&#39;: Check the Merchant when their transaction volume hits a certain amount.  &#39;5&#39;: Check the Merchant when a Payout occurs.  &#39;6&#39;: Check the Merchant when the volume of Payouts to the Merchant hits a certain amount. | [optional] 
**Low** | **int?** | A lower cut-off value for the score in this Merchant check.  The check fails if the score is equal to or below this value. | [optional] 
**High** | **int?** | An upper cut-off value for the score in this Merchant check.  The check fails if the score is equal to or above this value. | [optional] 
**Amount** | **int?** | The minimum value that the Transaction must be to trigger this Merchant check.  The unit of measure is determined by the type of check. | [optional] 
**Action** | **int?** | The action to take when the check fails.  This field is specified as an integer.  Valid values are:  &#39;1&#39;: Manual. The Merchant is sent to the manual review queue.  &#39;2&#39;: Mark the funds in the Merchant account as reserved.  &#39;3&#39;: Limit the Merchant account by blocking transactions which are over the limit set in this check. | [optional] 
**Options** | **int?** | The options for the Verification.  This field is specified as a sum of the desired options.  Valid values are:  &#39;0&#39;: NONE. No options are set.  &#39;1&#39;: KYC. KYC verification will be enabled.  &#39;2&#39;: WATCHLIST_STANDARD. Primary member will be checked agains standard watchlists.  &#39;4&#39;: WATCHLIST_PLUS. Primary member will be checked against additional watchlists.  &#39;8&#39;: WATCHLIST_PREMIER. Primary member will be checked against uncommon lists like PEP, Associated family members, etc. | [optional] 
**Sequence** | **int?** | A sequence number to use when applying multiple linked checks.  When two or more checks are linked, the checks with lower &#39;sequence&#39; numbers are applied first.  This field is specified as an integer. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

