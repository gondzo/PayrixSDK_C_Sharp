# IO.Swagger.Model.OrgFlowActionsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrgFlow** | **string** | The identifier of the orgFlow resource that this orgFlowActions resource is associated with. | 
**Org** | **string** |  | 
**Action** | **int?** | The action to take in relation to the Entity being processed.   Valid values are:  &#39;1&#39;: Add the referenced Entity to the referenced Org.  &#39;2&#39;: Remove the referenced Entity from the referenced Org. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

