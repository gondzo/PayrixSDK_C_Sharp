# IO.Swagger.Model.Body70
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Entry** | **string** | The identifier of the Entries resource that is being refunded. | 
**Description** | **string** | A description of this Refund.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Amount** | **int?** | The amount of this Refund.  This field is specified as an integer in cents.  This field is optional. If it is not set, then the API uses the amount that is specified in the related Entry resource. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

