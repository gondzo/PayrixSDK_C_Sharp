# IO.Swagger.Model.TokenspaymentResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Method** | **int?** | The method used to make this payment.  This field is specified as an integer.  Valid values are:  &#39;1&#39;: American Express  &#39;2&#39;: Visa  &#39;3&#39;: MasterCard  &#39;4&#39;: Diners Club  &#39;5&#39;: Discover  &#39;6&#39;: PayPal  &#39;7&#39;: Debit card  &#39;8&#39;: Checking account  &#39;9&#39;: Savings account  &#39;10&#39;: Corporate checking account and  &#39;11&#39;: Corporate savings account  &#39;12&#39;: Gift card  &#39;13&#39;: EBT  &#39;14&#39;:WIC. | [optional] 
**Number** | **string** | The card number or bank account number of the payment method associated with this Token. | [optional] 
**Routing** | **string** | If the payment method associated with this Token is a bank account, then this field contains the routing code for the bank account. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

