# IO.Swagger.Model.ChargebackMessageResultsQueryResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**ChargebackMessage** | **string** | The identifier of the chargebackMessage resource that this chargebackMessageResult relates to. | [optional] 
**Type** | **int?** | The type of this chargebackMessageResult.  Valid values are:  &#39;1&#39;: A general type of result.  &#39;2&#39;: Platform message | [optional] 
**Message** | **string** | The Chargeback message itself. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

