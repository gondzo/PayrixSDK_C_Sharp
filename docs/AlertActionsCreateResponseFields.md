# IO.Swagger.Model.AlertActionsCreateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Type** | **string** | The medium to use to deliver this Alert.  Valid values are:  &#39;email&#39;: Deliver the Alert to an email address.  &#39;web&#39;: Deliver the Alert through a web site notification.  &#39;app&#39;: Deliver the Alert through a mobile application notification.  &#39;sms&#39;: Deliver the Alert through an SMS message to a mobile device. | [optional] 
**Options** | **string** | When the &#39;type&#39; field of this resource is set to &#39;web&#39;, this field determines the format that the Alert data should be sent in.   Valid values are:  &#39;JSON&#39;: JSON document serialization.  &#39;XML&#39;: XML document serialization.  &#39;SOAP&#39;: SOAP XML document serialization.  &#39;FORM&#39;:HTML form data serialization. | [optional] 
**Value** | **string** |  | [optional] 
**Alert** | **string** | The identifier of the Alert resource that defines this alertAction. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

