# IO.Swagger.Model.InlineResponse20050ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Entry** | **string** | The identifier of the Entries resource that is being refunded. | [optional] 
**Description** | **string** | A description of this Refund.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Amount** | **int?** | The amount of this Refund.  This field is specified as an integer in cents.  This field is optional. If it is not set, then the API uses the amount that is specified in the related Entry resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

