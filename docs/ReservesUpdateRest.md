# IO.Swagger.Model.ReservesUpdateRest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**Org** | **string** | The identifier of the Org that this Reserves resource applies to.  If you set this field, then the Reserve applies to all Entities in the Org. | [optional] 
**Entity** | **string** | The identifier of the Entity that this Reserve applies to. | [optional] 
**Name** | **string** | The name of this Reserve.  This field is stored as a text string and must be between 1 and 100 characters long. | [optional] 
**Description** | **string** | A description of this Reserve.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Percent** | **int?** | The percentage of funds to reserve, expressed in basis points.  For example, 25.3% is expressed as &#39;2530&#39;. | 
**Release** | **int?** | The schedule that determines when the funds in this Reserve should be released.  Valid values are:  &#39;1&#39;: Daily - the funds are released every day.  &#39;2&#39;: Weekly - the funds are released every week.  &#39;3&#39;: Monthly - the funds are released every month.  &#39;4&#39;: Annually - the funds are released every year. | 
**ReleaseFactor** | **int?** | A multiplier that you can use to adjust the schedule set in the &#39;release&#39; field.  This field is specified as an integer and its value determines how the schedule is multiplied.  For example, if &#39;release&#39; is set to &#39;1&#39; (meaning &#39;daily&#39;), then a &#39;releaseFactor&#39; value of &#39;2&#39; would cause the funds to be released from this Reserve every two days. | 
**Finish** | **int?** | The date on which this Reserve resource should stop reserving funds from the Entity or Org.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

