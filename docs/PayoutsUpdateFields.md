# IO.Swagger.Model.PayoutsUpdateFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Login** | **string** | The Login that owns this resource. | 
**Entity** | **string** | The identifier of the Entity that this Payout is associated with. | 
**Account** | **string** | The identifier of the Account that this Payout is associated with.  This account will either receive the funds or be debited for the funds every time a Disbursement occurs, depending on the direction of the Disbursement. | 
**PayoutFlow** | **string** | The identifier of the PayoutFlow associated with this Payout. | [optional] 
**Name** | **string** | The name of this Payout.  This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Description** | **string** | A description of this Payout.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Schedule** | **int?** | The schedule that determines when this Payout is triggered to be paid.  Valid values are:  &#39;1&#39;: Daily - the Payout is paid every day.  &#39;2&#39;: Weekly - the Payout is paid every week.  &#39;3&#39;: Monthly - the Payout is paid every month.  &#39;4&#39;: Annually - the Payout is paid every year.  &#39;5&#39;: Single - the Payout is a one-off payment. | 
**ScheduleFactor** | **int?** | A multiplier that you can use to adjust the schedule set in the &#39;schedule&#39; field, if it is set to a duration-based trigger, such as daily, weekly, monthly, or annually.  This field is specified as an integer and its value determines how the interval is multiplied.  For example, if &#39;schedule&#39; is set to &#39;1&#39; (meaning &#39;daily&#39;), then a &#39;scheduleFactor&#39; value of &#39;2&#39; would cause the Payout to trigger every two days. | 
**Start** | **int?** | The date on which payment of the Payout should start.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016.  The value of this field must represent a date in the future, or the present date. | 
**Currency** | **string** | The currency of the amount in this Payout.  This field is only required when Um is set to ACTUAL. If this field is not set we will process disbursements for all currencies.  Currently, this field only accepts the value &#39;USD&#39;. | [optional] 
**Um** | **int?** | The unit of measure for this Payout.  Valid values are:  &#39;1&#39;: Percentage - the Payout is a percentage of the current available funds for this Entity that should be paid to their Account, specified in the &#39;amount&#39; field in basis points. &#39;2&#39;: Actual - the Payout is a fixed amount, specified in the &#39;amount&#39; field as an integer in cents.  &#39;3&#39;: Negative percentage - the Payout is a percentage of the balance, specified in the &#39;amount&#39; field as a negative integer in basis points. The direction of the Payout payment is reversed. For example, if the Entity has a negative balance of $10 and the amount is set to 10000 (100%), then $10 will be drawn from their account to fully replenish the balance to $0. | 
**Amount** | **int?** | The total amount of the Payout resource that is created.  This field is specified as an integer.  The units used in this field are determined by the value of the &#39;um&#39; field on the Payout. If the &#39;um&#39; field is set to &#39;1&#39; or &#39;3&#39;, then this field specifies the Payout percentage to levy in basis points. If the &#39;um&#39; field is set to &#39;2&#39;, then this field specifies the Payout in cents. | 
**Minimum** | **int?** |  | [optional] 
**_Float** | **int?** | An optional field indicating the minimum balance you want to maintain, despite any Payouts occurring. If the Payout would reduce the balance to below this value, then it is not processed.  This field is specified as an integer in cents.  For example, a float value of 1000 would ensure that a balance of 10 USD is maintained at all times. | 
**SkipOffDays** | **int?** | Whether to skip the creation of disbursements on holidays and weekends. A value of &#39;1&#39; means skip and a value of &#39;0&#39; means do not skip. | 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

