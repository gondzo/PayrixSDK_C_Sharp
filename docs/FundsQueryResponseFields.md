# IO.Swagger.Model.FundsQueryResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Entity** | **string** | The identifier of the Entity that owns this Fund. | [optional] 
**Currency** | **string** | The currency of the Fund.  Currently, this field only accepts the value &#39;USD&#39;. | [optional] 
**Reserved** | **string** | The amount held in this Fund that is marked as reserved.  This field is specified as an integer in cents. | [optional] 
**Pending** | **string** |  | [optional] 
**Available** | **string** | The amount held in this Fund that is currently available for disbursement.  This field is specified as an integer in cents. | [optional] 
**Total** | **string** | The total amount held in this Fund.  This field is specified as an integer in cents. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

