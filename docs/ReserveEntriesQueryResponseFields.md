# IO.Swagger.Model.ReserveEntriesQueryResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Login** | **string** | The Login that owns this resource. | [optional] 
**Fund** | **string** | The identifier of the Fund that this reserveEntries resource relates to. | [optional] 
**Txn** | **string** | This field indicates that this reserveEntry was triggered from a Transaction.  This field stores the identifier of the Transaction. | [optional] 
**TxnHold** | **string** |  | [optional] 
**Reserve** | **string** | This field indicates that this reserveEntry was triggered from an automatic reserve.  This field stores the identifier of the Reserve resource. | [optional] 
**EntityReserve** | **string** | This field indicates that this reserveEntry was triggered from a manual change to an entityReserve.  This field stores the identifier of the entityReserve resource. | [optional] 
**ReserveEntry** | **string** | This field indicates that this reserveEntry shows funds moving out of reserve.  This field stores the identifier of the reserveEntry resource that moved the funds into the reserve. | [optional] 
**Description** | **string** | A description of this reserveEntries resource.   This field is stored as a text string and must be between 0 and 100 characters long. | [optional] 
**Release** | **int?** | The date on which the funds in reserve should be released.  The date is specified as an eight digit string in YYYYMMDD format, for example, &#39;20160120&#39; for January 20, 2016. | [optional] 
**Amount** | **int?** | The amount held in reserve in this reserveEntries resource.  This field is specified as an integer in cents. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

