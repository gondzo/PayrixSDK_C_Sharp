# IO.Swagger.Model.InlineResponse20047ResponseData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Entity** | **string** | The identifier of the Entity that this PendingEntry refers to. | [optional] 
**Fromentity** | **string** | If the activity that this PendingEntry refers to involves two parties in the system with one paying a charge of any kind, then this field stores the identifier of the Entity that the charge or other activity is for. | [optional] 
**Fund** | **string** | The identifier of the Fund that this PendingEntry refers to. | [optional] 
**OpposingPendingEntry** | **string** |  | [optional] 
**Entry** | **string** |  | [optional] 
**Adjustment** | **string** |  | [optional] 
**Chargeback** | **string** | If the activity that this PendingEntry refers to is a Chargeback, then this field stores the identifier of the corresponding Chargeback resource. | [optional] 
**Disbursement** | **string** | If the activity that this PendingEntry refers to is the charging of a Disbursement, then this field stores the identifier of the corresponding Disbursement resource. | [optional] 
**Fee** | **string** | If the activity that this PendingEntry refers to is the charging of a Fee, then this field stores the identifier of the corresponding Fee resource. | [optional] 
**Refund** | **string** | If the activity that this PendingEntry refers to is the paying of a Refund, then this field stores the identifier of the corresponding Refund resource. | [optional] 
**Txn** | **string** | If the activity that this PendingEntry refers to is a Transaction, then this field stores the identifier of the corresponding Transaction resource. | [optional] 
**_Event** | **int?** | The type of event that triggered this PendingEntry resource.  Valid values are:  &#39;1&#39;: Daily - the PendingEntry triggers every day.  &#39;2&#39;: Weekly - the PendingEntry triggers every week.  &#39;3&#39;: Monthly - the PendingEntry triggers every month.  &#39;4&#39;: Annually - the PendingEntry triggers every year.  &#39;5&#39;: Single - the PendingEntry is a one-off event.  &#39;6&#39;: Auth - the PendingEntry triggers at the time of authorization of a transaction.  &#39;7&#39;: Capture - the PendingEntry triggers at the capture time of a Transaction.  &#39;8&#39;: Refund - the PendingEntry triggers when a refund transaction is processed.  &#39;9&#39;: Board - the PendingEntry triggers when the Merchant is boarded.  &#39;10&#39;: Payout - the PendingEntry triggers when a payout is processed.  &#39;11&#39;: Chargeback - the PendingEntry triggers when a card chargeback occurs.  &#39;12&#39;: Overdraft - the PendingEntry triggers when an overdraft usage charge from a bank is levied.  &#39;13&#39;: Interchange - the PendingEntry triggers when interchange Fees are assessed for the Transactions of this Merchant.  &#39;14&#39;: Processor - the PendingEntry triggers when the Transactions of this Merchant are processed by a payment processor.  &#39;15&#39;: ACH failure - the PendingEntry triggers when an automated clearing house failure occurs.  &#39;16&#39;: Account - the PendingEntry triggers when a bank account is verified. | [optional] 
**EventId** | **string** | The identifier of the record that is associated with this PendingEntry. | [optional] 
**Description** | **string** | A description of this PendingEntry. | [optional] 
**Amount** | **string** | The amount involved in this PendingEntry. It refers to the amount charged, transferred, or disbursed.  This field is specified as an integer in cents. | [optional] 
**Pending** | **int?** | Whether this resource is marked as pending. A value of &#39;1&#39; means pending and a value of &#39;0&#39; means not pending. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

