# IO.Swagger.Model.InlineResponse200ResponseDetails
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RequestId** | **string** | A unique identifier set by the API client, that is echoed by the response.  The request ID can be useful in troubleshooting and monitoring. | [optional] 
**Page** | [**InlineResponse200ResponseDetailsPage**](InlineResponse200ResponseDetailsPage.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

