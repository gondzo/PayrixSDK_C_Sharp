# IO.Swagger.Model.InlineResponse200ResponseDetailsPage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Current** | **int?** | The current page in the paginated resource list. | [optional] 
**Last** | **int?** | The last available page in the paginated resource list. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

