# IO.Swagger.Model.FeeModifiersUpdateResponseFields
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of this resource. | [optional] 
**Created** | **DateTime?** | The date and time at which this resource was created. | [optional] 
**Modified** | **DateTime?** | The date and time at which this resource was modified. | [optional] 
**Creator** | **string** | The identifier of the Login that created this resource. | [optional] 
**Modifier** | **string** | The identifier of the Login that last modified this resource. | [optional] 
**Fee** | **string** | The identifier of the Fee that this Fee Modifier applies. | [optional] 
**Entity** | **string** | The identifier of the Entity that this Fee Modifier applies for. | [optional] 
**Org** | **string** | The identifier of the Org this Fee Modifiers should apply for on behalf of the Entity identified in the value of the &#39;entity&#39; field.  This field is optional. If it is set, then the Fee Modifier is applied to this Org instead. | [optional] 
**Fromentity** | **string** | The identifier of the Entity who should pay this Fee on behalf of the Entity identified in the value of the &#39;entity&#39; or &#39;org&#39; field.  This field is optional. If it is set, then the Fee is charged to this Entity instead. | [optional] 
**MarkupUm** | **int?** | The unit of measure for the markup amount for the Fee.  Valid values are:  &#39;2&#39;: The markup is a fixed amount, specified in the &#39;markupAmount&#39; field as an integer in cents.  &#39;1&#39;: The markup is a percentage of the fee amount, specified in the &#39;markupAmount&#39; field in basis points. | [optional] 
**MarkupAmount** | **string** | The total amount of the markup value for this Fee.  This field is specified as an integer.  The units used in this field are determined by the value of the &#39;markupUm&#39; field on the Fee. If the &#39;markupUm&#39; field is set to &#39;percentage&#39;, then this field specifies the Fee percentage to levy in basis points. If the &#39;markupUm&#39; field is set to &#39;actual&#39;, then this field specifies the markup amount in cents. | [optional] 
**Inactive** | **int?** | Whether this resource is marked as inactive. A value of &#39;1&#39; means inactive and a value of &#39;0&#39; means active. | [optional] 
**Frozen** | **int?** | Whether this resource is marked as frozen. A value of &#39;1&#39; means frozen and a value of &#39;0&#39; means not frozen. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

